<?php

class LanguagesServices extends Service {

	private $model;

	public function __construct() {

        $model = Languages::Instance();

        if($model instanceof Languages) {
            $this->model = $model;
        }
	}
    

    /************************************ LOAD ************************************/


	public function loadOne($data) {

		$result = $this->model->getOne($data);

		return $result;
	}


	public function loadAll() {
		
		$results = $this->model->getAll();
      
		return $results;
	}


	public function loadByAlias($data) {

        $result = $this->model->getByAlias($data);

        return $result;
    }
}
?>