<?php

class CategoriesServices extends Service {

    private $model;

    public function __construct() {

        $model = Categories::Instance();

        if($model instanceof Categories) {
            $this->model = $model;
        }
    }


    /************************************ LOAD ************************************/


    public function loadOne($data) {

        if(Languages::languagesExist() && !isset($data["admin_view"])) {

            $data = $this->setLangGroupIdFromId($data);

            $result = $this->model->getByLanguageGroupIdAndLanguageId($data);
        }
        else {
            $result = $this->model->getOne($data);
        }

        $this->setItemProperties($result);

        return $result;
    }


    public function loadAll($data = null) {

        if(Languages::languagesExist() && !isset($data["admin_view"])) {

            $results = $this->model->getByLanguageId($data);
        }
        else{
            $results = $this->model->getAll($data);
        }

        foreach($results as $result) {

            $this->setItemProperties($result);
        }

        return $results;
    }


    public function loadByAlias($data) {

        $adminLogged = $this->adminLoggedIn();

        $result = $this->model->getByAlias($data, $adminLogged);

        $this->setItemProperties($result);

        return $result;
    }


    public function loadByParentId($data) {

        if(Languages::languagesExist() && !isset($data["admin_view"])) {

            $data = $this->setLangGroupIdFromParentId($data);

            $results = $this->model->getByParentIdAndLanguageGroupIdAndLanguageId($data);
        }
        else{
            $results = $this->model->getByParentId($data);
        }

        foreach($results as $result) {

            $this->setItemProperties($result);
        }

        return $results;
    }


    public function loadChildren($data) {

        $results = Categories::getCategories();

        $children = array();
        foreach($results as $result) {

            if($this->isChildOf($results, $result, $data['parent_id'])) {
                array_push($children, $result);
            }
        }

        return $children;
    }


    public function loadBySearch($data) {

        if(Languages::languagesExist() && !isset($data["admin_view"])) {

            $data["lang_id"] = Trans::getLanguageId();
        }

        $results = $this->model->getBySearch($data);

        foreach($results as $result) {

            $this->setItemProperties($result);
        }

        return $results;
    }


    public function loadByIds($data) {

        $results = $this->model->getByIds($data);

        return $results;
    }


    public function loadForAdminTables() {

       $results = $this->model->getForAdminTables();
       return $results;
    }


    /*** LOAD BY LANGUAGES ***/


    public function loadByLanguageId($data) {

        $results = $this->model->getByLanguageId($data);

        return $results;
    }


    public function loadByLanguageGroupId($data) {

        $results = $this->model->getByLanguageGroupId($data);

        return $results;
    }


    public function loadByLanguageGroupIdAndLanguageId($data) {

        $result = $this->model->getByLanguageGroupIdAndLanguageId($data);

        $this->setItemProperties($result, $data);

        return $result;
    }


    public function loadByParentIdAndLanguageGroupIdAndLanguageId($data) {

        $results = $this->model->getByParentIdAndLanguageGroupIdAndLanguageId($data);

        foreach($results as $result) {

            $this->setItemProperties($result);
        }

        return $results;
    }


    public function loadOneWithLanguageGroups($data) {

        $results = null;
        $langGroupId = null;

        if(isset($data['id']) && (int)$data['id'] !== 0) {

            $item = $this->model->getOne($data);

            if(isset($item) && $item) {

                $langGroupId = $this->setLanguageGroupId($item);

                $langGroupIdParams = $this->setLanguageGroupIdParams($langGroupId, $data);

                $results = $this->model->getByLanguageGroupId($langGroupIdParams);

                foreach($results as $result) {

                    $this->setItemProperties($result);
                }
            }
        }

        return $this->setItemWithLanguageGroupsResponse($results, $langGroupId);
    }


    /************************************ ACTIONS ************************************/


    public function insert($data) {

       if((string)$data['lang_group_id'] === "" || (int)$data['lang_group_id'] === 0) {
          unset($data['lang_group_id']);
       }

        $alias = filterUrl($data['name']);

        $aliases = $this->model->getAliasesByParentId($data);

        $data['alias'] = $this->setAlias($alias, $aliases);

        $data["created_by"] = $this->getLoggedInUserId();

        if((string)$data['lang_group_id'] === '' || (int)$data['lang_group_id'] === 0) unset($data['lang_group_id']);

        $this->model->insert($data);

        return $this->model->lastInsertId();
    }


    public function update($data) {

       if((string)$data['lang_group_id'] === "" || (int)$data['lang_group_id'] === 0) {
          unset($data['lang_group_id']);
       }

        $alias = filterUrl($data['name']);
        $id    = $data['id'];

        $aliases = $this->model->getAliasesByParentId($data);

        $data['alias'] = $this->setAlias($alias, $aliases, $id);

        $data["updated_by"] = $this->getLoggedInUserId();

        return $this->model->update($data);
    }


    public function updatePosition($data) {

        $this->model->updateParentId($data['id'], $data['$parent_id']);
        $this->model->updatePositions($data['id'], $data['position']);

        return $this->model->getOne(array("id" => $data['id'], "admin_view" => true));
    }

    /************************************ OTHER ************************************/


    public static function buildUrl($category, $langId = null) {

        $categories = Categories::getCategories($langId);

        if(isset($category)) {

            $alias = $category->alias;
            $parentId = $category->parent_id;

            $maxCount = 20;
            while($parentId != 0 && $maxCount > 0) {

                $category = null;
                foreach($categories as $cat) {
                    if($cat->id == $parentId) {
                        $category = $cat;
                    }
                }

                if(isset($category)) {
                    $parentId = $category->parent_id;
                    $alias = $category->alias . "/" . $alias;
                }
                $maxCount--;
            }

            return Conf::get('url') . "/" . $alias;
        }

        return Conf::get('url');
    }


    private function setItemProperties($item, $data = null) {

        $langId = null;
        if(@exists($data) && @exists($data['lang_id'])) {
            $langId = $data['lang_id'];
        }

        if(isset($item) && $item != false) {

            $item->url = self::buildUrl($item, $langId);
        }

        return $item;
    }


    private function isChildOf($categories, $childCat, $parentId) {

        if((int)$parentId === 0) return true;
        else if((int)$parentId === (int)$childCat->id) return true;
        else if((int)$childCat->id === 0) return false;
        else {
            $newChildCat = new stdClass();
            $newChildCat->id = 0;
            foreach($categories as $category) {
                if((int)$category->id === (int)$childCat->parent_id) {
                    $newChildCat = $category;
                }
            }
            return $this->isChildOf($categories, $newChildCat, $parentId);
        }
    }
}
?>