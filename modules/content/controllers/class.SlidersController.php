<?php


class SlidersController extends Controller {
    
    private $service;

    public function __construct() {  
        
        parent::__construct();
        $this->model->setTable("sliders");

        $service = SlidersServices::Instance();

        if($service instanceof SlidersServices) {
            $this->service = $service;
        }

        Trans::initTranslations();
    }
    

    /************************************ FETCH ************************************/


    /*
     * @param   int  id
     *
     * @return  object
    */
    public function fetchOne() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadOne($params);

        $this->view->respond($data, null);  
        return $data; 
    }


    /*
     * @param   NULL
     *
     * @return  array of objects
    */
    public function fetchAll() {

        $data = $this->service->loadAll();

        $this->view->respond($data, null);
        return $data; 
    }


    /*
     * @param   int  id
     *
     * @return  object
    */
    public function fetchOneWithItems() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));
        $data = $this->service->loadOneWithItems($params);

        $this->view->respond($data, null);
        return $data;
    }

    /*** FETCH BY LANGUAGES ***/


    /*
     * @param   int  lang_id    (optional)
     *
     * @return  array of objects
    */
    public function fetchByLanguageId() {
        
        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByLanguageId($params);

        $this->view->respond($data, null);  
        return $data;  
    }


    /*
     * @param   int  lang_group_id
     *
     * @return  array of objects
    */
    public function fetchByLanguageGroupId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByLanguageGroupId($params);

        $this->view->respond($data, null);  
        return $data; 
    }


    /*
     * @param   int  lang_group_id
     * @param   int  lang_id        (optional)
     *
     * @return  object
    */
    public function fetchByLanguageGroupIdAndLanguageId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByLanguageGroupIdAndLanguageId($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * Fetch by parent's parent, language group and current language (groups items with same language group)
     *
     * @param   int  id
     * @param   bool admin_view     (optional)
     *
     * @return  object with two properties:
     * 1) array items - all items with same language group with lang_id as key
     * 2) int   langGroupId
    */
    public function fetchOneWithLanguageGroups() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadOneWithLanguageGroups($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*************************************************************************
    *                              SLIDER ITEMS                              *
    *************************************************************************/


    /*
     * @param   int  id
     * @param   bool admin_view (optional)
     *
     * @return  object
    */
    public function fetchOneItem() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadOneItem($params);

        $this->view->respond($data, null);  
        return $data; 
    }


    /*
     * @param   bool admin_view (optional)
     *
     * @return  array of objects
    */
    public function fetchAllItems() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));
 
        $data = $this->service->loadAllItems($params);

        $this->view->respond($data, null);  
        return $data; 
    }


    /*
     * @param   int  parent_id
     * @param   bool admin_view (optional)
     *
     * @return  array of objects
    */
    public function fetchItemsByParentId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadItemsByParentId($params);

        return $data;
    }


    /*
     * @param   int  id
     *
     * @return  array
    */
    public function fetchTree() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadTee($params);

        $this->view->respond($data, null, Request::JSON_REQUEST);
        return $data;
    }


    /************************************ ACTIONS ************************************/


    /*
     * @param   array of form params
     *
     * @return  object:
     * 1) bool   success
     * 2) string message
    */
    public function insertSlider() {
        
        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = new stdClass();

        if(!@exists($params["name"])) {
            $data->success = false;
            $data->message = Trans::get("Name required");
        }
        else{

            $this->service->insert($params);
            $data->success = true;
            $data->message = Trans::get("Slider created");
        }

        $this->view->respond($data, null, Request::JSON_REQUEST);
        return $data;
    }


    /*
     * @param   array of form params
     *
     * @return  object:
     * 1) bool   success
     * 2) string message
    */
    public function updateSlider() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = new stdClass();

        if(!@exists($params["name"])) {
            $data->success = false;
            $data->message = Trans::get("Name required");
        }
        else{

            $this->service->update($params);

            $data->success = true;
            $data->message = Trans::get("Slider updated");
        }

        $this->view->respond($data, null, Request::JSON_REQUEST);
        return $data;
    }


    /*************************************************************************
    *                              SLIDER ITEMS                              *
    *************************************************************************/


    /*
     * @param   array of form params
     *
     * @return  object:
     * 1) bool   success
     * 2) string message
     * 3) int    id - last inserted
    */
    public function insertSliderItem() {
        
        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = new stdClass();

        if(!@exists($params["image"])) {
            $data->success = false;
            $data->message = Trans::get("Image required");
        }
        else{

            $data->id      = $this->service->insertItem($params);
            $data->success = true;
            $data->message = Trans::get("Slider item created");
        }

        $this->view->respond($data, null, Request::JSON_REQUEST);
        return $data;
    }


    /*
     * @param   array of form params
     *
     * @return  object:
     * 1) bool   success
     * 2) string message
    */
    public function updateSliderItem() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = new stdClass();

        if(!@exists($params["image"])) {
            $data->success = false;
            $data->message = Trans::get("Image required");
        }
        else{

            $this->service->updateItem($params);

            $data->success = true;
            $data->message = Trans::get("Slider item updated");
        }               

        $this->view->respond($data, null, Request::JSON_REQUEST);
        return $data;
    }


    /*
     * @param   int  id
     *
     * @return  NULL
    */
    public function deleteSliderItem() {

        $id = trim(Security::Instance()->purifier()->purify($this->params("id")));

        $this->service->deleteItem($id);

        $this->view->respond(null, null, Request::JSON_REQUEST);
        return null;
    }


    /*
     * @param   int  id
     * @param   int  position
     *
     * @return  object
    */
    public function updateItemsPosition() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));
        
        $data = $this->service->updateItemsPosition($params);
        
        $this->view->respond($data, null, Request::JSON_REQUEST);
        return $data; 
    }
}
?>