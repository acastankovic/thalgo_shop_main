<?php


class ArticlesController extends Controller {

    private $service;

    public function __construct() {
        parent::__construct();
        $this->model->setTable("articles");

        $service = ArticlesServices::Instance();

        if($service instanceof ArticlesServices) {
            $this->service = $service;
        }

        Trans::initTranslations();
    }


    /************************************ FETCH ************************************/


    /*
     * @param   int  id
     * @param   bool admin_view (optional)
     * @param   int  lang_id    (optional)
     *
     * @return  object
    */
    public function fetchOne() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadOne($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * @param   bool admin_view (optional)
     * @param   int  lang_id    (optional)
     *
     * @return  array of objects
    */
    public function fetchAll() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadAll($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * @param   int    parent_id
     * @param   string alias
     *
     * @return  object
    */
    public function fetchByAlias() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByAlias($params);

        return $data;
    }


    /*
     * @param   int  parent_id
     * @param   bool admin_view (optional)
     * @param   int  lang_id    (optional)
     *
     * @return  array of objects
    */
    public function fetchByParentId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByParentId($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * @param   int  parent_id
     * @param   bool admin_view (optional)
     * @param   int  lang_id    (optional)
     *
     * @return  array of objects
    */
    public function fetchByCategoryParentId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByCategoryParentId($params);

        return $data;
    }


    /*
     * @param   string  search_term
     * @param   bool    admin_view (optional)
     * @param   int     lang_id    (optional)
     *
     * @return  array of objects
    */
    public function fetchBySearch() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = null;
        if(@exists($params["search"])) {

            $data = $this->service->loadBySearch($params);
        }

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * @param   bool    admin_view (optional)
     *
     * @return  ids string
    */
    public function fetchParentIds() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadParentIds($params);

        $this->view->respond($data, null);
        return $data;
    }


   /*
    * @return  array of objects
   */
   public function fetchForAdminTables() {

      $data = $this->service->loadForAdminTables();

      $this->view->respond($data, null);
      return $data;
   }


    /*** FETCH BY LANGUAGES ***/


    /*
     * @param   int  lang_id    (optional)
     * @param   bool admin_view (optional)
     *
     * @return  array of objects
    */
    public function fetchByLanguageId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByLanguageId($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * @param   int  lang_group_id
     *
     * @return  array of objects
    */
    public function fetchByLanguageGroupId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByLanguageGroupId($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * @param   int  lang_group_id
     * @param   int  lang_id        (optional)
     *
     * @return  object
    */
    public function fetchByLanguageGroupIdAndLanguageId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByLanguageGroupIdAndLanguageId($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * Fetch by parent, language group and current language
     *
     * @param   int  lang_group_id
     * @param   int  lang_id        (optional)
     * @param   bool admin_view     (optional)
     *
     * @return  array of objects
    */
    public function fetchByParentIdAndLanguageGroupIdAndLanguageId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByParentIdAndLanguageGroupIdAndLanguageId($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * Fetch by parent's parent, language group and current language
     *
     * @param   int  lang_group_id
     * @param   int  lang_id        (optional)
     * @param   bool admin_view     (optional)
     *
     * @return  array of objects
    */
    public function fetchByParentsParentIdAndLanguageGroupIdAndLanguageId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByParentsParentIdAndLanguageGroupIdAndLanguageId($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * Fetch by parent's parent, language group and current language (groups items with same language group)
     *
     * @param   int  id
     * @param   bool admin_view     (optional)
     *
     * @return  object with two properties:
     * 1) array items - all items with same language group with lang_id as key
     * 2) int   langGroupId
    */
    public function fetchOneWithLanguageGroups() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadOneWithLanguageGroups($params);

        $this->view->respond($data, null);
        return $data;
    }


    /************************************ ACTIONS ************************************/


    /*
     * @param   array of form params
     *
     * @return  object:
     * 1) bool   success
     * 2) string message
     * 3) int    id - last inserted
    */
    public function insertArticle() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = new stdClass();

        if(!@exists($params["title"])) {
            $data->success = false;
            $data->message = Trans::get('Title required');
        }
        else{

            $data->id      = $this->service->insert($params);
            $data->success = true;
            $data->message = Trans::get("Article created");
        }

        $this->view->respond($data, null, Request::JSON_REQUEST);
        return $data;
    }


    /*
     * @param   array of form params
     *
     * @return  object:
     * 1) bool   success
     * 2) string message
    */
    public function updateArticle() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = new stdClass();

        if(!@exists($params["title"])) {
            $data->success = false;
            $data->message = Trans::get("Title required");
        }
        else{

            $this->service->update($params);

            $data->success = true;
            $data->message = Trans::get("Article updated");
        }

        $this->view->respond($data, null, Request::JSON_REQUEST);
        return $data;
    }


    /*
     * Changes item position (rang)
     *
     * @param   int  id
     * @param   int  position
     *
     * @return  object
    */
    public function updatePosition() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->updatePosition($params);

        $this->view->respond($data, null, Request::JSON_REQUEST);
        return $data;
    }
}
?>