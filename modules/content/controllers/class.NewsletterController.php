<?php


class NewsletterController extends Controller {

   private $service;

   public function __construct() {

      parent::__construct();

      $this->model->setTable("newsletter");

      $service = NewsletterServices::Instance();

      if ($service instanceof NewsletterServices) {
         $this->service = $service;
      }

      Trans::initTranslations();
   }


   /************************************ FETCH ************************************/

   /*
    * @param   int  id
    * @param   bool admin_view (optional)
    * @param   int  lang_id    (optional)
    *
    * @return  object
   */
   public function fetchOne() {

      $params = trimFields(Security::Instance()->purifyAll($this->params()));

      $data = $this->service->loadOne($params);

      $this->view->respond($data, null);
      return $data;
   }


   /*
    * @param   bool admin_view (optional)
    * @param   int  lang_id    (optional)
    *
    * @return  array of objects
   */
   public function fetchAll() {

      $params = trimFields(Security::Instance()->purifyAll($this->params()));

      $data = $this->service->loadAll($params);

      $this->view->respond($data, null);
      return $data;
   }

   /************************************ ACTIONS ************************************/


   // signup from site
   public function signup() {

      $params = trimFields(Security::Instance()->purifyAll($this->params()));

      $data = $this->service->validateFields($params);

      if(!$data->success) {
         $this->view->respond($data);
         return $data;
      }

      $this->model->insert($params);

      $response = new stdClass();
      $response->success = true;
      $response->message = Trans::get("Newsletter created");
      $response->data    = $this->model->loadLastInsert();

      $this->view->respond($response);
      return $response;
   }


   public function download() {

      $data = Dispatcher::instance()->dispatch("content", "newsletter", "fetchAll", null);

      $downloadData = $this->service->setDownloadData($data);

      $filename = Conf::get('download_filename')['newsletter'] . ".xls";
      header("Content-Encoding: UTF-8");
      header("Content-Type: application/vnd.ms-excel; charset=utf-8");
      header("Content-Disposition: attachment; filename=\"$filename\"");
      exportFile($downloadData->body, $downloadData->heading);
      exit;
   }

}
?>