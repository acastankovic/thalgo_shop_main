<?php


class GalleriesController extends Controller {
    
    private $service;
    
    public function __construct() {
        parent::__construct();
        $this->model->setTable("galleries");

        $service = GalleriesServices::Instance();

        if($service instanceof GalleriesServices) {
            $this->service = $service;
        }

        Trans::initTranslations();
    }
    

    /************************************ FETCH ************************************/


    /*
     * @param   int  id
     * @param   int  type_id
     *
     * @return  object
    */
    public function fetchOne() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadOne($params);

        $this->view->respond($data, null);
        return $data; 
    }


    /*
     * @param   int  id
     * @param   int  type_id
     *
     * @return  object
    */
    public function fetchOneForAdmin() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadOneForAdmin($params);

        $this->view->respond($data, null);
        return $data;
    }
    

    public function fetchAll() {
        
        $data = $this->service->loadAll();
        
        $this->view->respond($data, null);  
        return $data; 
    }


    /************************************ ACTIONS ************************************/


    /*
     * @param   array of form params
     *
     * @return  object:
     * 1) bool   success
     * 2) string message
     * 3) int    id - last inserted
    */
    public function insertGallery() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = new stdClass();

        if(!@exists($params["name"])) {
            $data->success = false;
            $data->message = Trans::get('Title required');
        }
        else{

            $data->id      = $this->service->insert($params);
            $data->success = true;
            $data->message = Trans::get("Gallery created");
        }

        $this->view->respond($data, null, Request::JSON_REQUEST);
        return $data;
    }


    /*
     * @param   array of form params
     *
     * @return  object:
     * 1) bool   success
     * 2) string message
    */
    public function updateGallery() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = new stdClass();

        if(!@exists($params["name"])) {
            $data->success = false;
            $data->message = Trans::get("Title required");
        }
        else{

            $this->service->update($params);

            $data->success = true;
            $data->message = Trans::get("Gallery updated");
        }

        $this->view->respond($data, null, Request::JSON_REQUEST);
        return $data;
    }
}
?>