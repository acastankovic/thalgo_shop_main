<?php


class LanguagesController extends Controller {
    
    private $service;

    public function __construct() {
        parent::__construct();
        $this->model->setTable("languages");

        $service = LanguagesServices::Instance();

        if($service instanceof LanguagesServices) {
            $this->service = $service;
        }

        Trans::initTranslations();
    }
    

    /************************************ FETCH ************************************/


    /*
     * @param   int  id
     *
     * @return  object
    */
    public function fetchOne() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadOne($params);

        $this->view->respond($data, null);  
        return $data; 
    }


    /*
     * @param   NULL
     *
     * @return  array of objects
    */
    public function fetchAll() {

        $data = $this->service->loadAll();

        $this->view->respond($data, null);  
        return $data; 
    }


    /*
     * @param   string  alias
     *
     * @return  object
    */
    public function fetchByAlias() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByAlias($params);

        $this->view->respond($data, null);
        return $data;
    }


    /************************************ ACTIONS ************************************/


    /*
     * @param   int  id
     *
     * @return  NULL
    */
    public function setLanguage() {
                
        $language = Dispatcher::instance()->dispatch("content", "languages", "fetchOne", array("id" => $this->params("id")));
        Trans::setLanguage($language);

        $this->view->respond(null, null, Request::JSON_REQUEST);
    }
}
?>