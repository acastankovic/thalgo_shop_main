<?php

class Comments extends Model {

   private $selectQueryString;
   private $selectTotalQueryString;

   public function __construct() {
      parent::__construct();
      $this->setTable("comments");
      //$this->setQueryStrings();
   }


   /************************************ FETCH ************************************/

   public function getOne($data) {

      $sql = "select * from `comments` where `id` = :id";

      return $this->exafe($sql, array("id" => $data["id"]));
   }


   public function getAll() {

      $sql = "select * from `comments`";

      return $this->exafeAll($sql);
   }


   public function getByTypeIdAndTargetId($data) {

      $sql = "select * from `comments` where `type_id` = :type_id and `target_id` = :target_id";

      if(!$this->adminView($data)) {
         $sql .= " and `published` = 1";
      }

      return $this->exafeAll($sql, array("type_id" => $data["type_id"], "target_id" => $data["target_id"]));
   }


   public function publish($data) {

      $sql = "update `comments` set `published` = :published where `id` = :id";
      return $this->execute($sql, array("published" => $data["published"], "id" => $data["id"]));
   }
}
?>