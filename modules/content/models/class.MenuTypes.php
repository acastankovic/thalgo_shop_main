<?php


Class MenuTypes extends Model {

    public function __construct() {
        parent::__construct();
        $this->setTable("menu_types");
    }


    /************************************ FETCH ************************************/


    public function getOne($data) {

        $sql = "select * from `menu_types` where `id` = :id ";

        return $this->exafe($sql, array("id" => $data["id"]));
    }


    public function getAll() {

        $sql = "select * from `menu_types`";

        return $this->exafeAll($sql);
    }
}
?>