<?php


Class Menus extends Model {

    private $selectQueryString;

    public function __construct() {
        parent::__construct();
        $this->setTable("menus");
        $this->setQueryStrings();
    }


    /************************************ FETCH ************************************/


    public function getOne($data) {

        if(!isset($data) || !isset($data["id"])) return null;

        $sql  = $this->selectQueryString;
        $sql .= " where `m`.`id` = :id ";

        return $this->exafe($sql, array("id" => $data["id"]));
    }
    
    
    public function getAll() {
        
        $sql = $this->selectQueryString;

        return $this->exafeAll($sql);
    }


    public function getByLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where `m`.`lang_id` = :lang_id";

        return $this->exafeAll($sql, array("lang_id" => $langId));
    }


    public function getByLanguageGroupId($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `m`.`id` = :lang_group_id || `m`.`lang_group_id` = :lang_group_id";

        return $this->exafeAll($sql, array("lang_group_id" => $data["lang_group_id"]));
    }


    public function getByLanguageGroupIdAndLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where (`m`.`id` = :lang_group_id || `m`.`lang_group_id` = :lang_group_id) and `m`.`lang_id` = :lang_id";

        return $this->exafe($sql, array("lang_group_id" => $data["lang_group_id"], "lang_id" => $langId));
    }


    /************************************ OTHER ************************************/


    private function setQueryStrings() {

        $this->selectQueryString = "select `m`.*, 
                                    `l`.`name` as `language_name`, `l`.`alias` as `language_alias`, 
                                    `uc`.`username` as created_by_username, 
                                    `uu`.`username` as update_by_username 
                                    from `menus` as `m` 
                                    left join  `languages` as `l` on `l`.`id` = `m`.`lang_id`
                                    left join `users` as `uc` on `uc`.`id` = `m`.`created_by`
                                    left join `users` as `uu` on `uu`.`id` = `m`.`updated_by`";
    }
}
?>