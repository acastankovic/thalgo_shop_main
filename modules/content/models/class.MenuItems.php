<?php


Class MenuItems extends Model {

    private $selectQueryString;
    private $orderByString;

    public function __construct() {
        parent::__construct();
        $this->setTable("menu_items");
        $this->setQueryStrings();
    }


    /************************************ FETCH ************************************/


    public function getOne($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `mi`.`id` = :id";
        return $this->exafe($sql, array("id" => $data["id"]));
    }


    public function getAll() {

        $sql  = $this->selectQueryString;
        $sql .= $this->orderByString;
        return $this->exafeAll($sql);
    }


    public function getByMenuId($data) {

        $sql = "select `mi1`.*, `mi2`.`name` as `parent_name` from `menu_items` `mi1` 
                left join `menu_items` `mi2` on `mi1`.`parent_id` = `mi2`.`id` 
                where `mi1`.`menu_id` = :menu_id 
                order by `mi1`.`rang`";

        return $this->exafeAll($sql, array("menu_id" => $data["menu_id"]));
    }


    /************************************ ACTIONS ************************************/


    public function updateParentId($data) {

        return $this->update(array("id" => $data["id"], "parent_id" => $data["parent_id"]));
    }


    private function updatePositionByMenuId($data) {

        $sql = "update `menu_items` set `rang` = :rang where `id` = :id and `menu_id` = :menu_id";
        return $this->execute($sql, array("rang" => $data["rang"], "id" => $data["id"], "menu_id" => $data["menu_id"]));
    }


    public function updatePositions($data) {

        $menu_item = $this->getOne($data);

        $sql  = $this->selectQueryString;
        $sql .= " where `mi`.`parent_id` = :parent_id and `mi`.`id` != :id and `m`.`id` = :menu_id";
        $sql .= $this->orderByString;

        $menuItems = $this->exafeAll($sql, array("parent_id" => $menu_item->parent_id, "id" => $data["id"], "menu_id" => $menu_item->menu_id));

        $this->updatePositionByMenuId(array("id" => $data["id"], "rang" => $data["position"], "menu_id" => $menu_item->menu_id));

        $counter = 0;
        foreach($menuItems as $item) {

            if((int)$counter === (int)$data["position"]) {
                $counter++;
            }

            if((int)$counter !== (int)$item->rang) {

                $this->updatePositionByMenuId(array("id" => $item->id, "rang" => $counter, "menu_id" => $item->menu_id));
            }

            $counter++;
        }
    }

    /************************************ OTHER ************************************/


    private function setQueryStrings() {

        $this->selectQueryString = "select `mi`.*, 
                                    `mi2`.`name` as `parent_name`,
                                    `m`.`name` as `menu_name`, `m`.`lang_group_id` as `menu_lang_group_id`, `m`.`lang_id` as `menu_lang_id`, 
                                    `uc`.`username` as created_by_username, 
                                    `uu`.`username` as update_by_username   
                                    from `menu_items` as `mi`
                                    left join `menus` as `m` on `m`.`id` =  `mi`.`menu_id`
                                    left join `menu_items` as `mi2` on `mi2`.`id` = `mi`.`parent_id`
                                    left join `users` as `uc` on `uc`.`id` = `mi`.`created_by`
                                    left join `users` as `uu` on `uu`.`id` = `mi`.`updated_by`";

        $this->orderByString = " order by `mi`.`rang`, `mi`.`id` desc";
    }
}
?>