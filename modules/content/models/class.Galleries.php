<?php


Class Galleries extends Model {

    public function __construct() {  
        
        parent::__construct();
        $this->setTable("galleries");
    }
    

    /************************************ FETCH ************************************/
    
    
    public function getOne($data) {

        $sql = "select * from `galleries` where `id` = :id";

        return $this->exafe($sql, array("id" => $data["id"]));
    }

    public function getOneForAdmin($data) {

        $sql = "select `g`.*,
                `uc`.`username` as `created_by_username`,
                `uu`.`username` as `update_by_username`
                from `galleries` as `g`
                left join `users` as `uc` on `uc`.`id` = `g`.`created_by`
                left join `users` as `uu` on `uu`.`id` = `g`.`updated_by`
                where `g`.`id` = :id";

        return $this->exafe($sql, array("id" => $data["id"]));
    }

    public function getAll() {

        $sql = "select * from `galleries`";
        
        return $this->exafeAll($sql, null);
    }
}
?>