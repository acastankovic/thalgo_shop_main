<?php


class Categories extends Model {

    static public $categories;
    static public $db;

    private $selectQueryString;
    private $orderByString;
    private $publishedConditionWithWhereClause;
    private $publishedConditionWithAndOperator;

    public function __construct () {
        parent::__construct();
        $this->setTable("categories");
        $this->setQueryStrings();
    }


    /************************************ FETCH ************************************/


    private static function setDBConn() {

        if (!isset(self::$db)) self::$db = DB::Connect();

        return self::$db;
    }


    public static function getCategories($langId = null) {

        $getCategories = false;

        if(isset($langId)) {
            $getCategories = true;
        }

        if(!isset(self::$categories)) {
            $getCategories = true;
        }

        if($getCategories) {

            self::setDBConn();

            try {

                $sql = "select `c1`.*, 
                        `c2`.`id` as `parent_category_id`,  `c2`.`name` as `parent_category_name`, `c2`.`alias` as `parent_category_alias`, `c2`.`lang_group_id` as `parent_lang_group_id`,
                        `l`.`name` as `language_name`, `l`.`alias` as `language_alias`,
                        `uc`.`username` as created_by_username,
                        `uu`.`username` as update_by_username
                        from `categories` `c1` 
                        left join `categories` `c2` on `c1`.`parent_id` = `c2`.`id` 
                        left join `languages` as `l` on `l`.`id` = `c1`.`lang_id`
                        left join `users` as `uc` on `uc`.`id` = `c1`.`created_by`
                        left join `users` as `uu` on `uu`.`id` = `c1`.`updated_by`";

                if(isset($langId)) $sql .= " where `c1`.`lang_id` = :lang_id";

                $sql .= " order by `c1`.`rang`";

                $stm = self::$db->prepare($sql);

                if(isset($langId)) $stm->bindValue(':lang_id', (int) $langId, PDO::PARAM_INT);

                $stm->execute();

                $categories = self::fetchAll($stm);

                foreach ($categories as $category) {
                    $category->url = self::buildUrl($category, $categories);
                }

                self::$categories = $categories;

                return self::$categories;
            }
            catch (PDOException $e) {
                self::HandleDBError($e);
                return false;
            }
        }
        else return self::$categories;
    }


    public function getOne($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `c1`.`id` = :id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafe($sql, array("id" => $data["id"]));
    }


    public function getAll($data = null) {

        $sql  = $this->selectQueryString;

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithWhereClause;
        }

        $sql .= $this->orderByString;

        return $this->exafeAll($sql);
    }


    public function getByAlias($data, $adminLogged) {

        $sql  = $this->selectQueryString;
        $sql .= " where `c1`.`parent_id` = :parent_id and `c1`.`alias` = :alias";
        if(!$adminLogged) $sql .= " and `c1`.`published` = 1";

        return $this->exafe($sql, array("parent_id" => $data["parent_id"], "alias" => $data["alias"]));
    }


    public function getByParentId($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `c1`.`parent_id` = :parent_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        $sql .= $this->orderByString;

        return $this->exafeAll($sql, array("parent_id" => $data["parent_id"]));
    }


    public function getBySearch($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where (`c1`.`name` like :search or `c1`.`subtitle` like :search or `c1`.`content` like :search)";

        if(@exists($data["lang_id"])) {
            $sql .= " and `c1`.`lang_id` = :lang_id";
        }

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        $stm = $this->dbh->prepare($sql);

        $stm->bindValue(':search', (string) '%' . $data["search"] . '%', PDO::PARAM_STR);

        if(@exists($data["lang_id"])) {
            $stm->bindValue(':lang_id', (int) $data["lang_id"], PDO::PARAM_INT);
        }

        $stm->execute();

        return $stm->fetchAll(PDO::FETCH_OBJ);
    }


    public function getByIds($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `c1`.`id` in (" . $data['ids'] . ")";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafeAll($sql);
    }


    public function getForAdminTables() {

       $sql = "select `c1`.`id`, `c1`.`name`, `c1`.`published`, `c2`.`name` as `parent_name`, `l`.`name` as `language_name`  from `categories` as `c1`  left join `categories` as `c2` on `c2`.`id` = `c1`.`parent_id` left join `languages` as `l` on `l`.`id` = `c1`.`lang_id`";
       return $this->exafeAll($sql);
    }


    public function getAliasesByParentId($data) {

        $sql = "select `id`, `alias` from `categories` where `parent_id` = :parent_id";
        return $this->exafeAll($sql, array("parent_id" => $data["parent_id"]));
    }


    public function getByLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where `c1`.`lang_id` = :lang_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        $sql .= $this->orderByString;

        return $this->exafeAll($sql, array("lang_id" => $langId));
    }


    public function getByLanguageGroupId($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `c1`.`id` = :lang_group_id || `c1`.`lang_group_id` = :lang_group_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafeAll($sql, array("lang_group_id" => $data["lang_group_id"]));
    }


    public function getByLanguageGroupIdAndLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where (`c1`.`id` = :lang_group_id || `c1`.`lang_group_id` = :lang_group_id) and `c1`.`lang_id` = :lang_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafe($sql, array("lang_group_id" => $data["lang_group_id"], "lang_id" => $langId));
    }


    public function getByParentIdAndLanguageGroupIdAndLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where (`c2`.`id` = :lang_group_id || `c2`.`lang_group_id` = :lang_group_id) and `c2`.`lang_id` = :lang_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }
        else {
            $sql .= " order by `c1`.`rang`";
        }

        return $this->exafeAll($sql, array("lang_group_id" => $data["lang_group_id"], "lang_id" => $langId));
    }


    /************************************ ACTIONS ************************************/


    public function updateParentId($id, $parent_id) {

        return $this->update(array("id" => $id, "parent_id" => $parent_id));
    }


    public function updatePositions($id, $position) {

        $category = $this->getOne(array("id" => $id, "admin_view" => true));
        $sql = "select * from `categories` where `parent_id` = :parent_id and `id` <> :id order by `rang`";
        $categories = $this->exafeAll($sql, array("parent_id" => $category->parent_id, "id" => $id));

        $this->update(array("id" => $id, "rang" => $position));

        $counter = 0;
        foreach ($categories as $category) {

            if ($counter == $position) {
                $counter++;
            }

            if ($counter != $category->rang) {

                $this->update(array("id" => $category->id, "rang" => $counter));
            }

            $counter++;
        }
    }


    /************************************ OTHER ************************************/


    private function setQueryStrings() {

        $this->selectQueryString = "select `c1`.*, 
                                    `c2`.`id` as `parent_category_id`,  `c2`.`name` as `parent_category_name`, `c2`.`alias` as `parent_category_alias`, `c2`.`lang_group_id` as `parent_lang_group_id`,
                                    `l`.`name` as `language_name`, `l`.`alias` as `language_alias`,
                                    `uc`.`username` as created_by_username,
                                    `uu`.`username` as update_by_username
                                    from `categories` `c1` 
                                    left join `categories` `c2` on `c2`.`id` = `c1`.`parent_id`  
                                    left join `languages` as `l` on `l`.`id` = `c1`.`lang_id`
                                    left join `users` as `uc` on `uc`.`id` = `c1`.`created_by`
                                    left join `users` as `uu` on `uu`.`id` = `c1`.`updated_by`";

        $this->publishedConditionWithWhereClause = " where `c1`.`published` = 1";

        $this->publishedConditionWithAndOperator = " and `c1`.`published` = 1";

        $this->orderByString = " order by `c1`.`rang`, `c1`.`name`";
    }


    public static function buildUrl($category, $categories) {

        $alias = $category->alias;
        $parentId = $category->parent_id;

        $maxCount = 20;
        while ($parentId != 0 && $maxCount>0) {

            $category = null;
            foreach ($categories as $cat) {
                if ($cat->id == $parentId) {
                    $category = $cat;
                }
            }

            if (isset($category)) {
                $parentId = $category->parent_id;
                $alias = $category->alias . "/" . $alias;
            }
            $maxCount--;
        }

        return Conf::get('url') . "/" . $alias;
    }
}
?>