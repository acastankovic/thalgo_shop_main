<?php

class Newsletter extends Model {

   public function __construct() {
      parent::__construct();

      $this->setTable("newsletter");
   }


   /************************************ FETCH ************************************/


   public function getOne($data) {

      $sql = "select * from `newsletter` where `email` = :id";

      return $this->exafe($sql, array("id" => $data["id"]));
   }


   public function getAll($data = null) {

      $sql = "select * from `newsletter`";

      return $this->exafeAll($sql);
   }


   public function getByEmail($params) {

      $sql = "select * from `newsletter` where `email` = :email";

      return $this->exafe($sql, array("email" => $params["email"]));
   }
}
?>