<?php

class Media extends Model {  

    private $selectQueryString;
    private $selectTotalQueryString;
    
    public function __construct () {
        parent::__construct();
        $this->setTable("media");
        $this->setQueryStrings();
    }  


    /************************************ FETCH ************************************/


    public function getOne($data) {
        
        $sql  = $this->selectQueryString;
        $sql .= " where `id` = :id";
        return $this->exafe($sql, array("id" => $data["id"]));
    }


    public function getAll() {
        
        $sql = $this->selectQueryString;
        return $this->exafeAll($sql);
    }


    public function getByFileName($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `file_name` = :file_name";
        return $this->exafe($sql, array("file_name" => $data["file_name"]));
    }


    public function getByIds($ids) {

        $sql  = $this->selectQueryString;
        $sql .= " where `id` in (" . $ids . ")";

        return $this->exafeAll($sql);       
    }


    public function getBySearch($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where (`url` like :search or `name` like :search) order by `cdate` desc";
        return $this->exafeAll($sql, array('search' => '%' . $data["search_term"] . '%'));
    }


    public function getWithPagination($data) {

        $limit  = $data["items_per_page"];
        $page   = isset($data["page"]) ? $data["page"] : 1;
        $offset = isset($page) ? ($page - 1) * $limit : 0;

        $sql = $this->selectQueryString;

        if(isset($data)) {

            if(isset($data["mime_type"]) || isset($data["search"])) {
                $sql .= " where";
            }

            if(isset($data["mime_type"])) {
                $sql .= " `mime_type` = :mime_type";
            }

            if(isset($data["search"])) {
                $sql .= " `title` like :search or `file_name` like :search or `mime` like :search";
            }

            if(isset($data["order_by"])) {
                $sql .= " order by " . $data["order_by"];
            }

            if(isset($data["order_direction"])) {
                $sql .= " " . $data["order_direction"];
            }
        }

        $sql .= " limit :offset, :limit";

        $stm = $this->dbh->prepare($sql);

        if(isset($data)) {

            if(isset($data["mime_type"])) {
                $stm->bindValue(':mime_type', (string)$data["mime_type"], PDO::PARAM_STR);
            }

            if(isset($data["search"])) {
                $stm->bindValue(':search', (string)'%' . $data["search"] . '%', PDO::PARAM_STR);
            }
        }

        $stm->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
        $stm->bindValue(':offset', (int) $offset, PDO::PARAM_INT); 

        $stm->execute();

        return $stm->fetchAll(PDO::FETCH_OBJ);
    }


    public function getTotal($data) {

        $sql = $this->selectTotalQueryString;

        if(isset($data) && isset($data["mime_type"])) {
            $sql .= " where `mime_type` = :mime_type";
        }

        $stm = $this->dbh->prepare($sql);

        if(isset($data) && isset($data["mime_type"])) {
            $stm->bindValue(':mime_type', (string) $data["mime_type"], PDO::PARAM_STR);
        }

        $stm->execute();

        $result = $stm->fetch(PDO::FETCH_OBJ);

        return $result->total;
    }


    /************************************ OTHER ************************************/


    private function setQueryStrings() {

        $this->selectQueryString = "select * from `media`";

        $this->selectTotalQueryString = "select count(`id`) as `total` from `media`";
    }
}
?>