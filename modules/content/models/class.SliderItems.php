<?php


class SliderItems extends Model {  
    
    private $selectQueryString;
    private $orderByString;
    private $publishedConditionWithWhereClause;
    private $publishedConditionWithAndOperator;

    public function __construct () {
        parent::__construct();
        $this->setTable("slider_items");
        $this->setQueryStrings();
    }


    /************************************ FETCH ************************************/
    

    public function getOne($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `si`.`id` = :id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafe($sql, array("id" => $data["id"]));
    }


    public function getAll($data) {

        $sql  = $this->selectQueryString;

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithWhereClause;
        }

        $sql .= $this->orderByString;

        return $this->exafeAll($sql);
    }


    public function getByParentId($data) {
        
        $sql  = $this->selectQueryString;
        $sql .= " where `si`.`slider_id` = :slider_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        $sql .= $this->orderByString;

        return $this->exafeAll($sql, array("slider_id" => $data["parent_id"]));
    }


    public function getByParentIdAndLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where `si`.`slider_id` = :slider_id and `s`.`lang_id` = :lang_id ";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        $sql .= $this->orderByString;

        return $this->exafeAll($sql, array("slider_id" => $data["parent_id"], "lang_id" => $langId));
    }


    public function getByParentLangGroupIdAndLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where (`s`.`id` = :lang_group_id || `s`.`lang_group_id` = :lang_group_id) and `s`.`lang_id` = :lang_id ";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        $sql .= $this->orderByString;

        return $this->exafeAll($sql, array("lang_group_id" => $data["lang_group_id"], "lang_id" => $langId));
    }


    public function updatePositions($data) {

        $slider_item = $this->getOne($data);

        $sql = "select * from `slider_items` where `slider_id` = :slider_id and `id` != :id order by rang";

        $stm = $this->execute($sql, array("slider_id" => $slider_item->slider_id, "id" => $data["id"]));

        $items = $stm->fetchAll(PDO::FETCH_OBJ);

        $this->update(array("id" => $data["id"], "rang" => $data["position"]));

        $counter = 0; 
        foreach ($items as $item) {

            if ((int)$counter === (int)$data["position"]) {
                $counter++; 
            }

            if ($counter != $item->rang) {
                //update
                $this->update(array("id" => $item->id, "rang" => $counter));
            }

            $counter++; 
        }           
    }  


    /************************************ OTHER ************************************/


    private function setQueryStrings() {

        $this->selectQueryString = "select `si`.*, 
                                    `s`.`name` as `slider_name`, `s`.`lang_id` as `slider_lang_id`, `s`.`lang_group_id` as `slider_lang_group_id`,
                                    `uc`.`username` as created_by_username, 
                                    `uu`.`username` as update_by_username   
                                    from `slider_items` `si` 
                                    left join `sliders` as `s` on  `s`.`id` = `si`.`slider_id`
                                    left join `users` as `uc` on `uc`.`id` = `si`.`created_by`
                                    left join `users` as `uu` on `uu`.`id` = `si`.`updated_by`";

        $this->publishedConditionWithWhereClause = " where `si`.`published` = 1";

        $this->publishedConditionWithAndOperator = " and `si`.`published` = 1";

        $this->orderByString = " order by `si`.`rang`, `si`.`id`";
	}
}
?>