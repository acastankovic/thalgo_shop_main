<?php


class Articles extends Model {


    private $selectQueryString;
    private $orderByString;
    private $publishedConditionWithWhereClause;
    private $publishedConditionWithAndOperator;

    public function __construct () {
        parent::__construct();
        $this->setTable("articles");
        $this->setQueryStrings();
    }


    /************************************ FETCH ************************************/


    public function getOne($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `a`.`id` = :id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafe($sql, array("id" => $data["id"]));
    }


    public function getAll($data = null) {

        $sql  = $this->selectQueryString;

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithWhereClause;
        }

        $sql .= $this->orderByString;

        return $this->exafeAll($sql);
    }


    public function getByAlias($data, $adminLogged) {

        $sql  = $this->selectQueryString;
        $sql .= " where `a`.`category_id` = :category_id and `a`.`alias` = :alias";
        if(!$adminLogged) $sql .= " and `a`.`published` = 1";

        return $this->exafe($sql, array("category_id" => $data["parent_id"], "alias" => $data["alias"]));
    }


    public function getByParentId($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `a`.`category_id` = :category_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        if(@exists($data['order_by'])) {
          $sql .= $this->setOrderByString($data['order_by'], 'a');
        }

        $sql .= $this->orderByString;

        return $this->exafeAll($sql, array("category_id" => $data["parent_id"]));
    }


    public function getByCategoryParentId($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `c1`.`parent_id` = :category_parent_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        $sql .= $this->orderByString;

        return $this->exafeAll($sql, array("category_parent_id" => $data["parent_id"]));
    }


    public function getBySearch($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where (`a`.`title` like :search or `a`.`subtitle` like :search or `a`.`content` like :search)";

        if(@exists($data["parent_id"])) {
            $sql .= " and `a`.`category_id` = :parent_id";
        }

        if(@exists($data["lang_id"])) {
            $sql .= " and `a`.`lang_id` = :lang_id";
        }

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        $sql .= $this->orderByString;

        $stm = $this->dbh->prepare($sql);

        $stm->bindValue(':search', (string) '%' . $data["search"] . '%', PDO::PARAM_STR);

        if(@exists($data["parent_id"])) {
            $stm->bindValue(':parent_id', (int) $data["parent_id"], PDO::PARAM_INT);
        }

        if(@exists($data["lang_id"])) {
            $stm->bindValue(':lang_id', (int) $data["lang_id"], PDO::PARAM_INT);
        }

        $stm->execute();

        return $stm->fetchAll(PDO::FETCH_OBJ);
    }


    public function getAliasesByParentId($data) {

        $sql = "select `id`, `alias` from `articles` where `category_id` = :category_id;";
        return $this->exafeAll($sql, array("category_id" => $data["parent_id"]));
    }


    public function getByLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where `a`.`lang_id` = :lang_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafeAll($sql, array("lang_id" => $langId));
    }


    public function getByLanguageGroupId($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `a`.`id` = :lang_group_id || `a`.`lang_group_id` = :lang_group_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafeAll($sql, array("lang_group_id" => $data["lang_group_id"]));
    }


    public function getByLanguageGroupIdAndLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where (`a`.`id` = :lang_group_id || `a`.`lang_group_id` = :lang_group_id) and `a`.`lang_id` = :lang_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafe($sql, array("lang_group_id" => $data["lang_group_id"], "lang_id" => $langId));
    }


    public function getByParentIdAndLanguageGroupIdAndLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where (`c1`.`id` = :lang_group_id || `c1`.`lang_group_id` = :lang_group_id) and `c1`.`lang_id` = :lang_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        if(@exists($data['order_by'])) {
            $sql .= $this->setOrderByString($data['order_by'], 'a');
        }

        if(@exists($data['limit'])) {
          $sql .= " limit " . $data['limit'];
        }

        return $this->exafeAll($sql, array("lang_group_id" => $data["lang_group_id"], "lang_id" => $langId));
    }


    public function getByParentsParentIdAndLanguageGroupIdAndLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where (`c2`.`id` = :lang_group_id || `c2`.`lang_group_id` = :lang_group_id) and `c2`.`lang_id` = :lang_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafeAll($sql, array("lang_group_id" => $data["lang_group_id"], "lang_id" => $langId));
    }


    public function getParentIds($data) {

        $langId = $this->setLangId($data);

        $sql = "select distinct `category_id` from `articles` where `category_id` != 0 && `category_id` is not null";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafeAll($sql, array("lang_id" => $langId));
    }


    public function getForAdminTables() {

       $sql = "select `a`.`id`, `a`.`title`, `a`.`published`, `c`.`name` as `parent_name`, `l`.`name` as `language_name` from `articles` as `a` left join `categories` as `c` on `c`.`id` = `a`.`category_id` left join `languages` as `l` on `l`.`id` = `a`.`lang_id`";
       return $this->exafeAll($sql);
    }


    public function updatePositions($id, $position) {

        $article = $this->getOne(array("id" => $id, "admin_view" => true));

        $sql = "select * from `articles` where `category_id` = :parent_id and `id` <> :id order by `rang`";
        $articles = $this->exafeAll($sql, array("parent_id" => $article->category_id, "id" => $id));

        $this->update(array("id" => $id, "rang" => $position));

        $counter = 0;
        foreach ($articles as $article) {

            if ($counter == $position) {
                $counter++;
            }

            if ($counter != $article->rang) {

                $this->update(array("id" => $article->id, "rang" => $counter));
            }

            $counter++;
        }

        return $this->getOne(array("id" => $id, "admin_view" => true));
    }

    /************************************ OTHER ************************************/


    private function setQueryStrings() {

        $this->selectQueryString = "select `a`.*, 
                                    `c1`.`id` as `parent_id`, `c1`.`name` as `parent_name`, `c1`.`lang_group_id` as `parent_lang_group_id`, `c1`.`lang_id` as `parent_lang_id`,
                                    `c2`.`id` as `categories_parent_id`, `c2`.`name` as `categories_parent_name`, `c2`.`parent_id` as `categories_parent_parent_id`, `c2`.`lang_group_id` as `categories_parent_lang_group_id`, `c2`.`lang_id` as `categories_parent_lang_id`,
                                    `l`.`name` as `language_name`, `l`.`alias` as `language_alias`,
                                    `uc`.`username` as `created_by_username`,
                                    `uu`.`username` as `update_by_username`
                                    from `articles` as `a` 
                                    left join `categories` as `c1` on `a`.`category_id` = `c1`.`id` 
                                    left join `categories` as `c2` on `c1`.`parent_id` = `c2`.`id` 
                                    left join `languages` as `l` on `l`.`id` = `a`.`lang_id`
                                    left join `users` as `uc` on `uc`.`id` = `a`.`created_by`
                                    left join `users` as `uu` on `uu`.`id` = `a`.`updated_by`";


        $this->publishedConditionWithWhereClause = " where `a`.`published` = 1";

        $this->publishedConditionWithAndOperator = " and `a`.`published` = 1";

        $this->orderByString = " order by `a`.`rang`, `a`.`id` desc";
    }
}
?>