<?php


class Sliders extends Model {  

    private $selectQueryString;

    public function __construct () {
        parent::__construct();
        $this->setTable("sliders");
        $this->setQueryStrings();
    }


    /************************************ FETCH ************************************/
    

    public function getOne($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `s`.`id` = :id ";

        return $this->exafe($sql, array("id" => $data["id"]));
    }


    public function getAll() {

        $sql = $this->selectQueryString;

        return $this->exafeAll($sql);
    }


    public function getByLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where `s`.`lang_id` = :lang_id";

        return $this->exafeAll($sql, array("lang_id" => $langId));
    }


    public function getByLanguageGroupId($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `s`.`id` = :lang_group_id || `s`.`lang_group_id` = :lang_group_id";

        return $this->exafeAll($sql, array("lang_group_id" => $data["lang_group_id"]));
    }


    public function getByLanguageGroupIdAndLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where (`s`.`id` = :lang_group_id || `s`.`lang_group_id` = :lang_group_id) and `s`.`lang_id` = :lang_id";

        return $this->exafe($sql, array("lang_group_id" => $data["lang_group_id"], "lang_id" => $langId));
    }


    /************************************ OTHER ************************************/


    private function setQueryStrings() {

        $this->selectQueryString = "select `s`.*, 
                                    `l`.`name` as `language_name`, `l`.`alias` as `language_alias` 
                                    from `sliders` as `s` 
                                    left join  `languages` as `l` on `l`.`id` = `s`.`lang_id`";
    }
}
?>