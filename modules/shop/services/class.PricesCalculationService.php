<?php 

class PricesCalculationService extends Service {

    private static $totalPrice;
    private static $productPriceByQty;
    private static $discountPrice;
    private static $vatPrice;
    private static $priceWithoutDiscount;


	public function __construct() {

	}


	public static function getProductPriceByQty($product, $quantity) {

        self::calculatePrices($product);

        self::$productPriceByQty = self::$totalPrice * $quantity;

        return self::$productPriceByQty;
    }


	public static function getProductPrice($product) {

        self::calculatePrices($product);

        return self::$totalPrice;
    }


	public static function getDiscountPrice($product) {

        self::calculatePrices($product);

        return self::$discountPrice;
    }


	public static function getVatPrice($product) {

        self::calculatePrices($product);

        return self::$vatPrice;
    }


	public static function getPriceWithoutDiscount($product) {

        self::calculatePrices($product);

        return self::$priceWithoutDiscount;
    }


	public static function calculatePrices($product) {

        $vatRate     = self::setVatRate($product->vat);
        $vatRateBase = self::setVatRateBase($product->vat);

        $totalPrice = 0;
        $discountPrice = 0;
        $vatPrice = 0;
        $priceWithoutDiscount = 0;

        if(self::discountExists($product)) {

            if(self::discountAbsolute($product)) {

                $totalPrice           = $product->price * $vatRate - $product->discount_value;
                $discountPrice        = $product->discount_value;
                $vatPrice             = ($totalPrice * $vatRateBase) / $vatRate;
                $priceWithoutDiscount = $totalPrice + $product->discount_value;

            }
            else if(self::discountPercent($product))  {

                $totalPrice           = (1 - $product->discount_value / 100) * $product->price * $vatRate;
                $discountPrice        = $product->price * ($product->discount_value / 100) * $vatRate;
                $vatPrice             = ($product->price * (1 - $product->discount_value / 100)) * $vatRateBase;
                $priceWithoutDiscount = ($totalPrice * 100) / (100 - $product->discount_value);
            }

        }else{
            
            $totalPrice           = $product->price * $vatRate;
            $discountPrice        = 0;
            $vatPrice             = $totalPrice - $product->price;
            $priceWithoutDiscount = $totalPrice;

        }

        self::$totalPrice           = $totalPrice;
        self::$discountPrice        = $discountPrice;
        self::$vatPrice             = $vatPrice;
        self::$priceWithoutDiscount = $priceWithoutDiscount;
    }


	public static function setVatRate($vat) {

        return self::vatExists($vat) ? (1 + $vat / 100) : 1;
    }


	public static function setVatRateBase($vat) {

        return self::vatExists($vat) ? ($vat / 100) : 0;
    }


	public static function discountPercent($product) {

        return (int)$product->discount_type === (int)Conf::get('discount_type')['percent'];
    }


	public static function discountAbsolute($product) {

        return (int)$product->discount_type === (int)Conf::get('discount_type')['absolute'];
    }


	public static function discountExists($product) {
        if(!@exists($product->discount_type)) return false;
        if(!isset($product->discount_value) || (int)$product->discount_value === 0) return false;
        return true;
    }


	public static function vatExists($vat) {

        return isset($vat) && (int)$vat !== 0 && (string)$vat !== '';
    }


    public static function renderDiscountValue($product) {

        if(self::discountExists($product)) {

            if(self::discountAbsolute($product)) {
                return  formatPrice($product->discount_value);
            }
            else if(self::discountPercent($product))  {
                return  $product->discount_value . '%';
            }
        }
        return "";
    }
}
?>