<?php

class VendorsServices extends Service {

	private $model;

	public function __construct() {

        $model = Vendors::Instance();

        if($model instanceof Vendors) {
            $this->model = $model;
        }
	}
    

    /************************************ LOAD ************************************/


    public function loadOne($data) {

        if(Languages::languagesExist() && !isset($data["admin_view"])) {

            $data = $this->setLangGroupIdFromId($data);

            $result = $this->model->getByLanguageGroupIdAndLanguageId($data);
        }
        else {
            $result = $this->model->getOne($data);
        }

        $this->setItemProperties($result);

        return $result;
    }


    public function loadAll($data = null) {

        if(Languages::languagesExist() && !isset($data["admin_view"])) {

            $results = $this->model->getByLanguageId($data);
        }
        else{
            $results = $this->model->getAll($data);
        }

        foreach($results as $result) {

            $this->setItemProperties($result);
        }

        return $results;
    }


    public function loadByAlias($data, $adminLogged) {

        $result = $this->model->getByAlias($data, $adminLogged);

        $this->setItemProperties($result);

        return $result;
    }


    /*** LOAD BY LANGUAGES ***/


    public function loadByLanguageId($data) {

        $results = $this->model->getByLanguageId($data);

        return $results;
    }


    public function loadByLanguageGroupId($data) {

        $results = $this->model->getByLanguageGroupId($data);

        return $results;
    }


    public function loadByLanguageGroupIdAndLanguageId($data) {

        $result = $this->model->getByLanguageGroupIdAndLanguageId($data);

        $this->setItemProperties($result);

        return $result;
    }


    public function loadOneWithLanguageGroups($data) {

        $results = null;
        $langGroupId = null;

        if(isset($data['id']) && (int)$data['id'] !== 0) {

            $item = $this->model->getOne($data);

            if(isset($item) && $item) {

                $langGroupId = $this->setLanguageGroupId($item);

                $langGroupIdParams = $this->setLanguageGroupIdParams($langGroupId, $data);

                $results = $this->model->getByLanguageGroupId($langGroupIdParams);

                foreach($results as $result) {

                    $this->setItemProperties($result);
                }
            }
        }

        return $this->setItemWithLanguageGroupsResponse($results, $langGroupId);
    }


    /************************************ ACTIONS ************************************/


    public function insert($data) {

       if((string)$data['lang_group_id'] === "" || (int)$data['lang_group_id'] === 0) {
          unset($data['lang_group_id']);
       }

       $alias = filterUrl($data['name']);

        $aliases = $this->model->getAliases();

        $data['alias'] = $this->setAlias($alias, $aliases);

        $data["created_by"] = $this->getLoggedInUserId();

        $this->model->insert($data);

        return $this->model->lastInsertId();
    }


    public function update($data) {

       if((string)$data['lang_group_id'] === "" || (int)$data['lang_group_id'] === 0) {
          unset($data['lang_group_id']);
       }

       $alias = filterUrl($data['name']);
        $id    = $data['id'];
        
        $aliases = $this->model->getAliases();

        $data['alias'] = $this->setAlias($alias, $aliases, $id);

        $data["updated_by"] = $this->getLoggedInUserId();

        return $this->model->update($data);
    }


    public function publish($data) {
		
		return $this->model->update($data);
    }


    /************************************ OTHER ************************************/


    private function setItemProperties($item) {

        if(isset($item) && $item != false) {

            if(!isset($item->published)) $item->published = 1;
        }

        return $item;
    }
}
?>