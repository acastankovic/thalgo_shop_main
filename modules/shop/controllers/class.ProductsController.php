<?php


class ProductsController extends Controller {
    
    private $service;

    public function __construct() {  
        
        parent::__construct();
        $this->model->setTable("products");

        $service = ProductsServices::Instance();

        if($service instanceof ProductsServices) {
            $this->service = $service;
        }

        Trans::initTranslations();
    }
    

    /************************************ FETCH ************************************/


    /*
     * @param   int  id
     * @param   bool admin_view (optional)
     * @param   int  lang_id    (optional)
     *
     * @return  object
    */
    public function fetchOne() {
        
        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadOne($params);

        $this->view->respond($data, null);
        return $data; 
    }


    /*
     * @param   bool admin_view (optional)
     * @param   int  lang_id    (optional)
     *
     * @return  array of objects
    */
    public function fetchAll() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadAll($params);

        $this->view->respond($data, null);
        return $data; 
    }


    /*
     * @param   int    parent_id
     * @param   string alias
     *
     * @return  object
    */
    public function fetchByAlias() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByAlias($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * @param   int  parent_id
     * @param   bool admin_view (optional)
     * @param   int  lang_id    (optional)
     *
     * @return  array of objects
    */
    public function fetchByParentId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByParentId($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * @return  array of objects
    */
    public function fetchForAdminTables() {

       $data = $this->service->loadForAdminTables();

       $this->view->respond($data, null);
       return $data;
    }


    public function fetchWithFilters() {

       $params = trimFields(Security::Instance()->purifyAll($this->params()));

       $data = $this->service->loadWithFilters($params);

       $this->view->respond($data, null);
       return $data;
    }

   /*** FETCH BY LANGUAGES ***/


    /*
     * @param   int  lang_id    (optional)
     * @param   bool admin_view (optional)
     *
     * @return  array of objects
    */
    public function fetchByLanguageId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByLanguageId($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * @param   int  lang_group_id
     *
     * @return  array of objects
    */
    public function fetchByLanguageGroupId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByLanguageGroupId($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * @param   int  lang_group_id
     * @param   int  lang_id        (optional)
     *
     * @return  object
    */
    public function fetchByLanguageGroupIdAndLanguageId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByLanguageGroupIdAndLanguageId($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * Fetch by parent, language group and current language
     *
     * @param   int  lang_group_id
     * @param   int  lang_id        (optional)
     * @param   bool admin_view     (optional)
     *
     * @return  array of objects
    */
    public function fetchByParentIdAndLanguageGroupIdAndLanguageId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByParentIdAndLanguageGroupIdAndLanguageId($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * Fetch by parent's parent, language group and current language
     *
     * @param   int  lang_group_id
     * @param   int  lang_id        (optional)
     * @param   bool admin_view     (optional)
     *
     * @return  array of objects
    */
    public function fetchByParentsParentIdAndLanguageGroupIdAndLanguageId() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadByParentsParentIdAndLanguageGroupIdAndLanguageId($params);

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * Fetch by parent's parent, language group and current language (groups items with same language group)
     *
     * @param   int  id
     * @param   bool admin_view     (optional)
     *
     * @return  object with two properties:
     * 1) array items - all items with same language group with lang_id as key
     * 2) int   langGroupId
    */
    public function fetchOneWithLanguageGroups() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = $this->service->loadOneWithLanguageGroups($params);

        $this->view->respond($data, null);
        return $data;
    }

    
    /************************************ ACTIONS ************************************/


    public function insertProduct() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = new stdClass();

        if(!@exists($params["title"])) {
            $data->success = false;
            $data->message = Trans::get('Title required');
        }
        else{

            $data->id      = $this->service->insert($params);
            $data->success = true;
            $data->message = Trans::get("Product created");
        }

        $this->view->respond($data, null, Request::JSON_REQUEST);
        return $data;
    }


    public function updateProduct() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = new stdClass();

        if(!@exists($params["title"])) {
            $data->success = false;
            $data->message = Trans::get("Title required");
        }
        else{

            $this->service->update($params);

            $data->success = true;
            $data->message = Trans::get("Product updated");
        }

        $this->view->respond($data, null, Request::JSON_REQUEST);
        return $data;
    }
}
?>