<?php


class Vendors extends Model {
    
    private $selectQueryString;
    // private $orderByString;
    private $publishedConditionWithWhereClause;
    private $publishedConditionWithAndOperator;

    public function __construct () {
        parent::__construct();
        $this->setTable("vendors");
        $this->setQueryStrings();
    }  
    

    /************************************ FETCH ************************************/


    public function getOne($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `v`.`id` = :id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafe($sql, array("id" => $data["id"]));
    }


    public function getAll($data = null) {

        $sql = $this->selectQueryString;

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithWhereClause;
        }

        return $this->exafeAll($sql);
    }


    public function getByAlias($data, $adminLogged) {

        $sql  = $this->selectQueryString;
        $sql .= " where `v`.`alias` = :alias";
        if(!$adminLogged) $sql .= " and `v`.`published` = 1";

        return $this->exafe($sql, array("alias" => $data["alias"]));
    }


    public function getAliases() {

        $sql = "select `id`, `alias` from `vendors`";
        return $this->exafeAll($sql);
    }


    public function getByLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where `v`.`lang_id` = :lang_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafeAll($sql, array("lang_id" => $langId));
    }


    public function getByLanguageGroupId($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `v`.`id` = :lang_group_id || `v`.`lang_group_id` = :lang_group_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafeAll($sql, array("lang_group_id" => $data["lang_group_id"]));
    }


    public function getByLanguageGroupIdAndLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where (`v`.`id` = :lang_group_id || `v`.`lang_group_id` = :lang_group_id) and `v`.`lang_id` = :lang_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafe($sql, array("lang_group_id" => $data["lang_group_id"], "lang_id" => $langId));
    }


    /************************************ OTHER ************************************/


    private function setQueryStrings() {

        $this->selectQueryString = "select `v`.*,
                                    `l`.`name` as `language_name`, `l`.`alias` as `language_alias`,
                                    `uc`.`username` as `created_by_username`,
                                    `uu`.`username` as `update_by_username`
                                    from `vendors` as `v`
                                    left join `languages` as `l` on `l`.`id` = `v`.`lang_id`
                                    left join `users` as `uc` on `uc`.`id` = `v`.`created_by`
                                    left join `users` as `uu` on `uu`.`id` = `v`.`updated_by` ";


        $this->publishedConditionWithWhereClause = " where `v`.`published` = 1";

        $this->publishedConditionWithAndOperator = " and `v`.`published` = 1";
    }
}
?>