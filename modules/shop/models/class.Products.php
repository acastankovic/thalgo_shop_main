<?php


class Products extends Model {
    
    private $selectQueryString;
    // private $orderByString;
    private $publishedConditionWithWhereClause;
    private $publishedConditionWithAndOperator;

    public function __construct () {
        parent::__construct(); 
        $this->setTable("products");
        $this->setQueryStrings();
    }  
    
    
    /************************************ FETCH ************************************/    


    public function getOne($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `p`.`id` = :id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafe($sql, array("id" => $data["id"]));
    }


    public function getAll($data = null) {

        $sql = $this->selectQueryString;

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithWhereClause;
        }

        return $this->exafeAll($sql);
    }
    
    
    public function getByAlias($data, $adminLogged) {
       
        $sql  = $this->selectQueryString;
        $sql .= " where `p`.`category_id` = :category_id and `p`.`alias` = :alias";
        if(!$adminLogged) $sql .= " and `p`.`published` = 1";

        return $this->exafe($sql, array("category_id" => $data["parent_id"], "alias" => $data["alias"]));
    }
    
    
    public function getByParentId($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `p`.`category_id` = :category_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafeAll($sql, array("category_id" => $data["parent_id"]));
    }


    public function getAliasesByParentId($data) {

       $sql = "select `id`, `alias` from `products` where `category_id` = :category_id;";
       return $this->exafeAll($sql, array("category_id" => $data["parent_id"]));
    }


    public function getByLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where `p`.`lang_id` = :lang_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        if(@exists($data["limit"])) {
           $sql .= " limit " . $data["limit"];
        }

        return $this->exafeAll($sql, array("lang_id" => $langId));
    }


    public function getByLanguageGroupId($data) {

        $sql  = $this->selectQueryString;
        $sql .= " where `p`.`id` = :lang_group_id || `p`.`lang_group_id` = :lang_group_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafeAll($sql, array("lang_group_id" => $data["lang_group_id"]));
    }


    public function getByLanguageGroupIdAndLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where (`p`.`id` = :lang_group_id || `p`.`lang_group_id` = :lang_group_id) and `p`.`lang_id` = :lang_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafe($sql, array("lang_group_id" => $data["lang_group_id"], "lang_id" => $langId));
    }


    public function getByParentIdAndLanguageGroupIdAndLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where (`c1`.`id` = :lang_group_id || `c1`.`lang_group_id` = :lang_group_id) and `p`.`lang_id` = :lang_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafeAll($sql, array("lang_group_id" => $data["lang_group_id"], "lang_id" => $langId));
    }


    public function getByParentsParentIdAndLanguageGroupIdAndLanguageId($data) {

        $langId = $this->setLangId($data);

        $sql  = $this->selectQueryString;
        $sql .= " where (`c2`.`id` = :lang_group_id || `c2`.`lang_group_id` = :lang_group_id) and `c2`.`lang_id` = :lang_id";

        if(!$this->adminView($data)) {
            $sql .= $this->publishedConditionWithAndOperator;
        }

        return $this->exafeAll($sql, array("lang_group_id" => $data["lang_group_id"], "lang_id" => $langId));
    }


    public function getForAdminTables() {

       $sql = "select `p`.`id`, `p`.`title`, `p`.`published`, `p`.`code`, `p`.`price`, `p`.`discount_type`, `p`.`discount_value`, `p`.`vendor_id`, `c`.`name` as `parent_name`, `v`.`name` as `vendor_name`, `l`.`name` as `language_name` from `products` as `p` left join `categories` as `c` on `c`.`id` = `p`.`category_id` left join `languages` as `l` on `l`.`id` = `p`.`lang_id` left join `vendors` as `v` on `v`.`id` = `p`.`vendor_id`";
       return $this->exafeAll($sql);
    }


    public function getTotal($data) {

       $whereString = " WHERE `p`.`lang_id` = :lang_id";

       if(@exists($data)) {

          if(@exists($data["search"])) {
            $whereString .= " AND (`p`.`title` like :search or `p`.`subtitle` like :search or `p`.`content` like :search or `p`.`code` like :search)";
          }

          if(@exists($data["parent_id"])) {
             $whereString .= " AND `p`.`category_id` = :category_id";
          }

          if(@exists($data["vendor_id"])) {
             $whereString .= " AND `p`.`vendor_id` = :vendor_id";
          }
       }

       $sql  = "select count(`p`.`id`) as `total`
                from `products` as `p` 
                left join `categories` as `c1` on `c1`.`id` = `p`.`category_id`  
                left join `categories` as `c2` on `c1`.`parent_id` = `c2`.`id` 
                left join `vendors` as `v` on `v`.`id` = `p`.`vendor_id`
                left join `languages` as `l` on `l`.`id` = `p`.`lang_id`";
       $sql .= $whereString;

       $stm = $this->dbh->prepare($sql);

       $stm->bindValue(':lang_id', (int) Trans::getLanguageId(), PDO::PARAM_INT);

       if(@exists($data)) {

           if(@exists($data["search"])) {
             $stm->bindValue(':search', (string) '%' . $data["search"] . '%', PDO::PARAM_STR);
           }

          if(@exists($data["parent_id"])) {
             $stm->bindValue(':category_id', (int) $data["parent_id"], PDO::PARAM_INT);
          }

          if(@exists($data["vendor_id"])) {
             $stm->bindValue(':vendor_id', (int) $data["vendor_id"], PDO::PARAM_INT);
          }
       }

       $stm->execute();

       $result = $stm->fetch(PDO::FETCH_OBJ);


       return $result->total;
    }


    public function getWithFilters($data) {

       $limit  = @exists($data["items_per_page"]) ? $data["items_per_page"] : Conf::get("items_per_page")["site_products"];
       $page   = @exists($data["page"]) ? $data["page"] : 1;
       $offset = @exists($page) ? ($page - 1) * $limit : 0;

       $whereString = " WHERE `p`.`lang_id` = :lang_id";

       if(@exists($data)) {

          if(@exists($data["search"])) {
            $whereString .= " AND (`p`.`title` like :search OR `p`.`subtitle` like :search OR `p`.`content` like :search OR `p`.`code` like :search)";
          }

          if(@exists($data["parent_id"])) {
             $whereString .= " AND `p`.`category_id` = :category_id";
          }

          if(@exists($data["vendor_id"])) {
             $whereString .= " AND `p`.`vendor_id` = :vendor_id";
          }
       }

       $sql  = $this->selectQueryString;
       $sql .= $whereString;

       if(!$this->adminView($data)) {
          $sql .= $this->publishedConditionWithAndOperator;
       }

       $sql .= " limit :offset, :limit";

       $stm = $this->dbh->prepare($sql);

       $stm->bindValue(':lang_id', (int) Trans::getLanguageId(), PDO::PARAM_INT);

       if(@exists($data)) {


          if(@exists($data["search"])) {
            $stm->bindValue(':search', (string) '%' . $data["search"] . '%', PDO::PARAM_STR);
          }

          if(@exists($data["parent_id"])) {
             $stm->bindValue(':category_id', (int) $data["parent_id"], PDO::PARAM_INT);
          }

          if(@exists($data["vendor_id"])) {
             $stm->bindValue(':vendor_id', (int) $data["vendor_id"], PDO::PARAM_INT);
          }
       }

       $stm->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
       $stm->bindValue(':offset', (int) $offset, PDO::PARAM_INT);

       $stm->execute();

       return $stm->fetchAll(PDO::FETCH_OBJ);
    }


    /************************************ OTHER ************************************/


    private function setQueryStrings() {

        $this->selectQueryString = "select `p`.*, 
                                    `c1`.`id` as `parent_id`, `c1`.`name` as `parent_name`, `c1`.`lang_group_id` as `parent_lang_group_id`, `c1`.`lang_id` as `parent_lang_id`,
                                    `c2`.`id` as `categories_parent_id`, `c2`.`name` as `categories_parent_name`, `c2`.`parent_id` as `categories_parent_parent_id`, `c2`.`lang_group_id` as `categories_parent_lang_group_id`, `c2`.`lang_id` as `categories_parent_lang_id`,
                                    `v`.`name` as `vendor_name`, `v`.`alias` as `vendor_alias`, `v`.`code` as `vendor_code`, `v`.`content` as `vendor_content`, `v`.`link` as `vendor_link`, `v`.`image` as `vendor_image`, 
                                    `l`.`name` as `language_name`, `l`.`alias` as `language_alias`,
                                    `uc`.`username` as `created_by_username`,
                                    `uu`.`username` as `update_by_username`
                                    from `products` as `p` 
                                    left join `categories` as `c1` on `c1`.`id` = `p`.`category_id`  
                                    left join `categories` as `c2` on `c1`.`parent_id` = `c2`.`id` 
                                    left join `vendors` as `v` on `v`.`id` = `p`.`vendor_id`
                                    left join `languages` as `l` on `l`.`id` = `p`.`lang_id`
                                    left join `users` as `uc` on `uc`.`id` = `p`.`created_by`
                                    left join `users` as `uu` on `uu`.`id` = `p`.`updated_by`";

        $this->publishedConditionWithWhereClause = " where `p`.`published` = 1";

        $this->publishedConditionWithAndOperator = " and `p`.`published` = 1";
    }
}
?>