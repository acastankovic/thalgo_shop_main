<?php

class OrderStatus {
  const NOT_ORDERED = 0;
  const PENDING = 1;
  const IN_DELIVERY = 2;
  const DELIVERED = 3;
}

class Orders extends Model {

    public function __construct () {
        parent::__construct(); 
        $this->setTable("orders");
    }  
    
    
    /************************************ FETCH ************************************/


    public function getOne($data) {


      $sql = "select * from `orders` where `id` = :id";


      return $this->exafe($sql, array("id" => $data["id"]));
    }


    public function getAll($data = null) {

      $sql = "select * from `orders`";

      return $this->exafeAll($sql);
    }


    public function getBySession($session) {
        
        $sql = "select * from `orders` where `session` = :session and `session` != '' ";
        return $this->exafe($sql, array("session" => $session));
    }


    /*********************************** ACTIONS ***********************************/


    public function updateBySession($data) {
    	
    	$sql = "update `orders` set `items` = :items, `quantity` = :quantity, `order_discount`= :order_discount, `order_vat`= :order_vat, `order_price`= :order_price where `session`= :session;";

        return $this->execute($sql, 
        	array(
        		"items" 		 => $data->items, 
        		"quantity" 		 => $data->quantity, 
        		"order_discount" => $data->discount, 
        		"order_vat" 	 => $data->vat, 
        		"order_price" 	 => $data->price, 
        		"session" 		 => $data->session
        	)
        );
    }

    public function insertCustomerInfo($session, $data) {

      $customerJson = json_encode($data);
      $sql = "update `orders` set `customer_json` = :customer_json where `session` = :orderSession";
      return $this->execute($sql, array("customer_json" => $customerJson, "orderSession" => $session));
    }


    public function updateOrderStatus($status, $id) {
      $sql = "update `orders` set `status` = :status where `id` = :id";
      return $this->execute($sql, array("status" => $status, "id" => $id));
    }


    public function changeOrder() {

    }
}
?>