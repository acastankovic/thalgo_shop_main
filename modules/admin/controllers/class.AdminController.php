<?php


class AdminController extends Controller {
    
    protected $user;
    
    public function __construct() {  

        parent::__construct();

        Trans::initTranslations();
    }
    
    public function loginPage() {
        
        $tableView = new AdminView($this);
        $tableView->respond(null, "pages/login/login.php");
    }
    
    public function logout() {
        Dispatcher::instance()->dispatch("users", "authentication", "logout", null);  
        header("Location:" . Conf::get('url') . "/admin");
    }


    /*************************************************************************
    *                             DASHBOARD                                  *
    *************************************************************************/


    public function dashboardPage() {
        $this->view->respond(null, "pages/dashboard/dashboard.php");
    }


    /*************************************************************************
    *                               MEDIA                                    *
    *************************************************************************/

    public function mediaPage() {

//        $params = Security::Instance()->purifyAll($this->params());
//
//        $itemsPerPage = isset($params["items_per_page"]) ? $params["items_per_page"] : Conf::get("items_per_page")["admin_media"];
//
//        $data = $this->setTableData($params, $itemsPerPage);
//        if(isset($params["mime_type"])) $data["mime_type"] = $params["mime_type"];
//
//        $items = Dispatcher::instance()->dispatch("content", "media", "fetchWithPagination", $data);
//
//        $paginationParams = $this->setPaginationParams($params,  $items->total, $itemsPerPage, "media");

        $tableView = new MediaAdminView($this);
        $tableView->init(
            array(
                "items" => Dispatcher::instance()->dispatch("content", "media", "fetchAll", null)
                //"items"            => $items->items,
                //"paginationParams" => $paginationParams
            )
        );          
        $tableView->respond(null, "pages/media/media.php");
    }

    
    /*************************************************************************
    *                            CATEGORIES                                  *
    *************************************************************************/   


    public function categoriesPage() {

        $tableView = new CategoriesAdminView($this);
        $tableView->init(
            array(
               "items" => Dispatcher::instance()->dispatch("content", "categories", "fetchForAdminTables", null)
            )
        );          
        $tableView->respond(null, "pages/categories/categories.php");
    }


    public function categoryInsertPage() {
        
        $id = (int)$this->params("id");

        $fetchResults = Dispatcher::instance()->dispatch("content", "categories", "fetchOneWithLanguageGroups", array("id" => $id, "admin_view" => true));

        $tableView = new CategoriesAdminView($this);
        $tableView->init(
            array(
                "items"       => $fetchResults->items,
                "langGroupId" => $fetchResults->langGroupId,
                "languages"   => Languages::getActive()
            )
        );        
        $tableView->respond(null, "pages/categories/category_insert.php");
    }


    public function categoriesSortPage() {

    }


    /*************************************************************************
    *                            ARTICLES                                    *
    *************************************************************************/    


    public function articlesPage() {

        $tableView = new ArticlesAdminView($this);
        $tableView->init(
            array(
               "items" => Dispatcher::instance()->dispatch("content", "articles", "fetchForAdminTables", null)
            )
        );        
        $tableView->respond(null, "pages/articles/articles.php");
    }
    

    public function articleInsertPage() {

        $id = (int)$this->params("id");

        $fetchResults = Dispatcher::instance()->dispatch("content", "articles", "fetchOneWithLanguageGroups", array("id" => $id, "admin_view" => true));

        $data = new stdClass();
        $data->galleries = Dispatcher::instance()->dispatch("content", "galleries", "fetchAll", null);

        $tableView = new ArticlesAdminView($this);
        $tableView->init(
            array(
                "items"       => $fetchResults->items,
                "langGroupId" => $fetchResults->langGroupId,
                "languages"   => Languages::getActive()
            )
        );
        $tableView->respond($data, "pages/articles/article_insert.php");
    }


    public function articlesCategoriesPage() {

        $articleIds = Dispatcher::instance()->dispatch("content", "articles", "fetchParentIds", array("admin_view" => true));

        $tableView = new ArticlesAdminView($this);
        $tableView->init(
            array(
                "items" => Dispatcher::instance()->dispatch("content", "categories", "fetchByIds", array("ids" => $articleIds, "admin_view" => true))
            )
        );
        $tableView->respond(null, "pages/articles/articles_categories.php");
    }


    public function articlesSortPage() {

        $category = Dispatcher::instance()->dispatch("content", "categories", "fetchOne", array("id" => $this->params("id"), "admin_view" => true));

        $tableView = new ArticlesAdminView($this);
        $tableView->init(
            array(
                "parentId" => $this->params("id"),
                "pageTitle" => Trans::get("Sort articles") . ': "' . $category->name . '"'
            )
        );
        $tableView->respond(null, "pages/articles/articles_sort.php");
    }


    /*************************************************************************
    *                               MENUS                                    *
    *************************************************************************/        


    public function menusPage() {

        $tableView = new MenusAdminView($this); 
        $tableView->init(
            array(
                "items"     => Dispatcher::instance()->dispatch("content", "menus", "fetchAll", null),
                "languages" => Languages::getActive()
            )
        );
        $tableView->respond(null, "pages/menus/menus.php");
    }


    public function menuItemsPage() {

        $menu = Dispatcher::instance()->dispatch("content", "menus", "fetchOne", array("id" => $this->params("id")));

        $categories = Categories::getCategories($menu->lang_id);
        $articles   = Dispatcher::instance()->dispatch("content", "articles", "fetchByLanguageId", array("lang_id" => $menu->lang_id, "admin_view" => true));

        $menuWithItems = Dispatcher::instance()->dispatch("content", "menus", "fetchOneWithItems",
            array(
                "id"         => $this->params("id"),
                "categories" => $categories,
                "articles"   => $articles,
                "lang_id"    => $menu->lang_id,
                "admin_view" => true
            )
        );

        $tableView = new MenusAdminView($this);
        $tableView->init(
            array(
                "menu"       => $menuWithItems,
                "languages"  => Languages::getActive(),
                "categories" => $categories,
                "articles"   => $articles,
                "menuTypes"  => Dispatcher::instance()->dispatch("content", "menus", "fetchTypes", null)
            )
        );
        $tableView->respond(null, "pages/menus/menu_items.php");
    }
    

    /*************************************************************************
    *                               SLIDERS                                  *
    *************************************************************************/
    

    public function slidersPage() {

        $tableView = new SlidersAdminView($this);
        $tableView->init(
            array(
                "items"     => Dispatcher::instance()->dispatch("content", "sliders", "fetchAll", null),
                "languages" => Languages::getActive()
            )
        );    
        $tableView->respond(null, "pages/sliders/sliders.php");
    }


    public function sliderItemsPage() {

        $tableView = new SlidersAdminView($this);
        $tableView->init(
            array(
                "parentId" => $this->params('slider_id'),
                "items"    => Dispatcher::instance()->dispatch("content", "sliders", "fetchItemsByParentId", array("parent_id" => $this->params('slider_id'), "admin_view" => true))
            )
        );
        $tableView->respond(null, "pages/sliders/slider_items.php");
    }


    public function sliderItemInsertPage() {

        $id = (int)$this->params("id");

        if($id != 0) {
            $item = Dispatcher::instance()->dispatch("content", "sliders", "fetchOneItem", array("id" => $this->params("id"), "admin_view" => true));
        }
        else {
            $item = new stdClass();
            $item->id = 0;
        }

        if(!isset($item->slider_id)) {
            $item->slider_id = $this->params("slider_id");
        }

        $tableView = new SlidersAdminView($this);
        $tableView->init(
            array(
                "item"     => $item,
                "parentId" => $this->params('slider_id'),
            )
        );
        $tableView->respond(null, "pages/sliders/slider_item_insert.php");
    }


    /*************************************************************************
    *                             GALLERIES                                  *
    *************************************************************************/

    public function galleriesPage() {

        $tableView = new GalleriesAdminView($this);
        $tableView->init(
            array(
                "items" => Dispatcher::instance()->dispatch("content", "galleries", "fetchAll", null)
            )
        );
        $tableView->respond(null, "pages/galleries/galleries.php");
    }


    public function galleryInsertPage() {

        $id = (int)$this->params("id");

        if($id != 0) {
            $item = Dispatcher::instance()->dispatch("content", "galleries", "fetchOneForAdmin", array("id" => $this->params("id")));
        }
        else {
            $item = new stdClass();
            $item->id = 0;
        }

        $tableView = new GalleriesAdminView($this);
        $tableView->init(
            array(
                "item" => $item
            )
        );
        $tableView->respond(null, "pages/galleries/gallery_insert.php");
    }


    /*************************************************************************
    *                               USERS                                    *
    *************************************************************************/ 


    public function usersPage() {
                
        if($this->adminLoggedIn()) {

            $tableView = new UsersAdminView($this);
            $tableView->init(
                array(
                    "items" => Dispatcher::instance()->dispatch("users", "users", "fetchAll", null)
                )
            );
            $tableView->respond(null, "pages/users/users.php");
        }
        else $this->dashboardPage();
    }


    public function userInsertPage() {

        $id = $this->params("id");

        if(!$this->adminLoggedIn() && !$this->editorLoggedIn($id)) {
            $this->dashboardPage();
            return;
        }

        if($id != 0) {
            $item = Dispatcher::instance()->dispatch("users", "users", "fetchOne", array("id" => $id));
        }
        else {
            $item = new stdClass();
            $item->id = 0;
        }

        $tableView = new UsersAdminView($this);
        $tableView->init(
            array(
                "item"  => $item,
                "roles" => Dispatcher::instance()->dispatch("users", "roles", "fetchAll", null),
                "loggedInUser" => $this->user
            )
        );
        $tableView->respond(null, "pages/users/user_insert.php");
    }


    //override 
    public function run($action) {
        
        //user needed
        $this->user = Dispatcher::instance()->dispatch("users", "authentication", "getActiveSession", null);

        if(!$this->user) $this->loginPage();
        else {
            $this->view = new AdminView($this);
            parent::run($action);
        }
    }
    

    public function getUser() {
        return $this->user; 
    }


    public function getUserId() {
        return $this->user->id; 
    }


    public function adminLoggedIn() {
        return (int)$this->user->role_id === (int)Conf::get('user_role_id')['admin'];
    }


    public function editorLoggedIn($userId) {
        return (int)$this->user->role_id === (int)Conf::get('user_role_id')['editor'] && (int)$this->user->id === (int)$userId;
    }


    /*************************************************************************
    *                             NEWSLETTER                                 *
    *************************************************************************/

    public function newsletterPage() {

        $tableView = new NewsletterAdminView($this);
        $tableView->init(
           array(
              "items"     => Dispatcher::instance()->dispatch("content", "newsletter", "fetchAll", null),
              "languages" => Languages::getActive()
           )
        );
        $tableView->respond(null, "pages/newsletter/newsletter.php");
    }
     

    /*************************************************************************
    *                            TABLE DATA                                  *
    *************************************************************************/


   public function tablePage() {

       $tableName = $this->params('name');
       $model = new Model();
       $model->setTable($tableName);
       $model->InitTable();

       $data = new stdClass();
       $data->tableContent = $model->loadFull();
       $data->tableName = $tableName;
       $data->tableDescription = $model->getTableDescription();

       $tableView = new GenericTablesAdminView($this);
       $tableView->setTableName($tableName);
       $tableView->init(
          array(
             "tableName"        => $data->tableName,
             "tableDescription" => $data->tableDescription,
             "tableContent"     => $data->tableContent,
          )
       );
       $tableView->respond($data, "pages/table/table.php");
    }


    public function saveData() {
        
        $params = $this->params();
        $tableName = $params['name'];

        $model = new Model();
        $model->setTable($tableName);               
        
        if(!isset($params['id'])) $params['id'] = "";
                
        if($params['id'] === "") {
            $postData = $this->post();
            $postData['created_by'] = $this->user->user_id;
            unset($postData['id']);
            $data = $model->insert($postData);
        }
        else {
            $postData = $this->post();
            $postData['updated_by'] = $this->user->user_id;
            $data = $model->update($postData);
        }
        
        $this->view->respond($data, null);
    }
    

    public function deleteData() {
        
        $params = $this->params();
        $tableName = $params['name'];
        $id = $params['id'];
        
        $model = new Model();
        $model->setTable($tableName);          
        
        $model->delete($id);
        
        $this->view->respond(null, null);       
    }


    /*************************************************************************
    *                                 OTHER                                  *
    *************************************************************************/


    private function setPaginationParams($params, $total, $itemsPerPage, $route) {

        $paginationParams = $params;
        $paginationParams["total"] = $total;
        $paginationParams["itemsPerPage"] = $itemsPerPage;
        $paginationParams["route"] = $route;
        unset($paginationParams["items_per_page"]);

        return $paginationParams;
    }


    private function setTableData($params, $itemsPerPage) {

        $page = isset($params["page"]) ? $params["page"] : 1;

        if(isset($params["items_per_page"])) unset($params["items_per_page"]);

        return array_merge(array("page" => $page, "items_per_page" => $itemsPerPage), $params);
    }


    /*************************************************************************
    *                            TRANSLATIONS                                *
    *************************************************************************/
    

    // load translation javaScript
    public function translationsJs() {

        echo 'var NC_TRANSLATIONS =' . json_encode(Trans::$translations) . ';';
        echo 'var NC_LANGUAGE ="' . Trans::getLanguageAlias() . '";';

        echo 'function NC_TRANSLATE(key, group)';
        echo '{';
            echo 'var result = (typeof group != "undefined") ? NC_TRANSLATIONS["group"][key] : NC_TRANSLATIONS["default"][key];';
            echo 'return (result) ? result : key;';
        echo '}';
    }   
}
?>
