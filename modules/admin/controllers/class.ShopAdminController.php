<?php


class ShopAdminController extends AdminController {
        
    public function __construct() {  
        
        parent::__construct();

        Trans::initTranslations();
    }

    
    /*************************************************************************
    *                            CATEGORIES                                  *
    *************************************************************************/   


    public function categoriesPage() {
    
        $pageType = $this->params('type');

        if(isset($pageType) && (string)$pageType === "shop") {

            $tableView = new CategoriesAdminView($this);
            $tableView->init(
                array(
                    "items" => Dispatcher::instance()->dispatch("content", "categories", "fetchForAdminTables", null),
                    "shopPage" => true
                )
            );
            $tableView->respond(null, "pages/categories/categories.php");
        }
        else $this->dashboardPage();
    }


    public function categoryInsertPage() {

        $pageType = $this->params('type');

        if(isset($pageType) && (string)$pageType === "shop") {

            $id = (int)$this->params("id");

            $fetchResults = Dispatcher::instance()->dispatch("content", "categories", "fetchOneWithLanguageGroups", array("id" => $id, "admin_view" => true));

            $tableView = new CategoriesAdminView($this);
            $tableView->init(
                array(
                    "items"       => $fetchResults->items,
                    "langGroupId" => $fetchResults->langGroupId,
                    "languages"   => Languages::getActive(),
                    "shopPage"    => true,
                )
            );
            $tableView->respond(null, "pages/categories/category_insert.php");
        }
        else $this->dashboardPage();
    }


//    private function getShopRootCategoryIds() {
//
//        $shopRootCategories = Dispatcher::instance()->dispatch("content", "categories", "fetchOneWithLanguageGroups", array("id" => Conf::get('shop_category_id'), "admin_view" => true));
//
//        $items = $shopRootCategories->items;
//
//        $ids = array();
//        foreach ($items as $key => $item) {
//
//            $newItem = new stdClass();
//            $newItem->id = $item->id;
//            $newItem->lang_id = $item->lang_id;
//
//            array_push($ids, $newItem);
//        }
//
//        return $ids;
//    }

    /*************************************************************************
    *                              PRODUCTS                                  *
    *************************************************************************/


    public function productsPage() {

        $tableView = new ProductsAdminView($this);
        $tableView->init(
            array(
                "items" => Dispatcher::instance()->dispatch("shop", "products", "fetchForAdminTables", null)
            )
        );
        $tableView->respond(null, "pages/products/products.php");
    }
    

    public function productInsertPage() {

        $id = (int)$this->params("id");

        $fetchResults = Dispatcher::instance()->dispatch("content", "products", "fetchOneWithLanguageGroups", array("id" => $id, "admin_view" => true));

        $tableView = new ProductsAdminView($this);
        $tableView->init(
            array(
                "items"       => $fetchResults->items,
                "langGroupId" => $fetchResults->langGroupId,
                "vendors"     => Dispatcher::instance()->dispatch("shop", "vendors", "fetchAll", array("admin_view" => true)),
                "languages"    => Languages::getActive()
            )
        );
        $tableView->respond(null, "pages/products/product_insert.php");
    }


    /*************************************************************************
    *                               VENDORS                                  *
    *************************************************************************/


    public function vendorsPage() {

        $tableView = new VendorsAdminView($this);
        $tableView->init(
            array(
                "items" => Dispatcher::instance()->dispatch("shop", "vendors", "fetchAll", array("admin_view" => true))
            )
        );
        $tableView->respond(null, "pages/vendors/vendors.php");
    }


    public function vendorInsertPage() {

        $id = (int)$this->params("id");

        $data = Dispatcher::instance()->dispatch("shop", "vendors", "fetchOneWithLanguageGroups", array("id" => $id, "admin_view" => true));

        $tableView = new VendorsAdminView($this);
        $tableView->init(
            array(
                "langGroupId"        => $data->langGroupId,
                "items"              => $data->items,
                "languages"          => Languages::getActive()
            )
        );
        $tableView->respond($data, "pages/vendors/vendor_insert.php");
    }


    /*************************************************************************
    *                                ORDERS                                  *
    *************************************************************************/


    public function ordersPage() {

      $tableView = new OrdersAdminView($this);

      $tableView->init(
        array(
          "items" => Dispatcher::instance()->dispatch("shop", "orders", "fetchAll", array("admin_view" => true))
        )
      );
      $tableView->respond(null, "pages/orders/orders.php");
    }


    public function orderInsertPage() {

      $id = (int)$this->params("id");

      $tableView = new OrdersAdminView($this);
      $tableView->init(
        array(
          "item" => Dispatcher::instance()->dispatch("shop", "orders", "fetchOne", array("id" => $id, "admin_view" => true))
        )
      );
      $tableView->respond(null, "pages/orders/order_insert.php");
    }

}
?>