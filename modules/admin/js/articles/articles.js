// delete
$(document).on('click', '.delete',
   function () {
      var route = '/content/articles/';
      var message  = NC_TRANSLATION[LANG.ALIAS].sDeleteArticleWarning;
      asyncDeleteAction(this, route, message);
   }
);