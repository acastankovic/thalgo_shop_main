// delete
$(document).on('click', '.delete',
   function () {
      var route = '/users/users/';
      var message  = NC_TRANSLATION[LANG.ALIAS].sDeleteUserWarning;
      asyncDeleteAction(this, route, message);
   }
);