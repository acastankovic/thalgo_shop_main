$(document).on("ready",
   function () {
      initTreeGrid();
   }
);


$(document).on('click', '.delete',
   function () {        
      var route = '/content/categories/';
      var message  = NC_TRANSLATION[LANG.ALIAS].sDeleteCategoryWarning;
      asyncDeleteAction(this, route, message);
   }
);


//drag and drop handler
function handleDnD(e, data) {
    
    var nodeId = data.node.id;
    var newParentId = data.parent;
    var newPosition = data.position; 
    
    if(newParentId === "#") newParentId = 0; 
    
    $.ajax({
        url: BASE_URL + '/content/categories/' + nodeId + '/position',
        type: 'put',
        data:  { parent_id: newParentId, position: newPosition },
        beforeSend: function(xhr) { 
            xhr.setRequestHeader("Accept","application/json");
        },
        success: function(response) {                    

            if(response.success) {
                $("tr[data-id='"+nodeId+"']").find("td[data-name='parent_category']").text(response.data.parent_category);
            }  
            else alert("Server error");
        }, 
        error: function() {

            alert("Server error");
        }
    });
};