$(document).on("ready",
    function () {
        initSliderItemsTree();
    }
);


// delete
$(document).on("click", ".delete",
   function () {
      var route = '/content/slider_items/';
      var message  = NC_TRANSLATION[LANG.ALIAS].sDeleteSliderItemWarning;
      asyncDeleteAction(this, route, message);
   }
);


function initSliderItemsTree() {

    var sliderId = $("#sliderId").val();

    ajaxCall(
        '/content/slider_items/tree/' + sliderId,
        'GET',
        {admin_view: true},
        function(response) {

            if(ajaxSuccess(response)) {

                $('#tree3').on('move_node.jstree', function (e, data) {
                    handleDnDSliderItemsTree (e, data);

                }).jstree({
                    "core": {
                        "check_callback": true,
                        'data': response.data
                    },
                    "plugins": ["dnd"]
                });
                $('#tree3').jstree(true).settings.core.data = response.data;
                $('#tree3').jstree(true).refresh();
            }
            else defaultErrorHandler(response);
        }
    );
};


//drag and drop handler
function handleDnDSliderItemsTree (e, data) {
    
    var nodeId = data.node.id;
    var newPosition = data.position;

    ajaxCall(
        '/content/slider_items/' + nodeId + '/position',
        'PUT',
        {position: newPosition, admin_view: true},
        function(response) {

            if(ajaxSuccess(response)) {

                $("tr[data-id='"+nodeId+"']").find("td[data-name='parent_category']").text(response.data.slider_id);
            }
            else defaultErrorHandler(response);
        }
    );
};