$(document).ready(
    function () {
        $('[data-toggle="tooltip"]').tooltip();
    }
);


function setUrlVariables() {

    var variables = getUrlVariables();

    if(!variables) {
        variables = [];
    }

    return variables;
};


/************************************************************
*                                                           *
*                   SHOW ITEMS BY AMOUNT                    *
*                                                           *
************************************************************/


// select box
$('#tableShowItems').on('change',
    function (event) {
        event.preventDefault();

        var value = $(this).val();

        var variables = setUrlVariables();

        if(typeof variables.items_per_page == "undefined" || variables.items_per_page == null || variables.items_per_page == "") {
            variables.items_per_page = value;
        }

        if(typeof variables.page == "undefined" || variables.page == null || variables.page == "") {
            variables.page = 1;
        }

        var pageBaseUrl = getUrlWithoutVariables();

        var urlString = pageBaseUrl + "?";

        var counter = 0;
        for(var key in variables) {

            if(key == "page") {
                variables[key] = 1;
            }

            if(key == "items_per_page") {
                variables[key] = value;
            }

            if(counter != 0) {

                urlString += "&";
            }

            urlString += key + "=" + variables[key];

            counter++;
        }

        window.location = urlString;
    }
);


/************************************************************
*                                                           *
*                        SEARCH FORM                        *
*                                                           *
************************************************************/

// search
$('#tableSearchForm').on('submit',
    function (event) {
        event.preventDefault();

        var value = $('#tableSearch').val();

        if(value == '') {
            openWarningPopup(NC_TRANSLATION[LANG.ALIAS].sSearchFieldEmpty);
            return;
        }

        var variables = setUrlVariables();

        if(typeof variables.search == "undefined" || variables.search == null || variables.search == "") {
            variables.search = value;
        }

        var pageBaseUrl = getUrlWithoutVariables();

        var urlString = pageBaseUrl + "?";

        var counter = 0;
        for(var key in variables) {

            if(key == "page") {
                variables[key] = 1;
            }

            if(key == "search") {
                variables[key] = value;
            }

            if(counter != 0) {

                urlString += "&";
            }

            urlString += key + "=" + variables[key];

            counter++;
        }

        window.location = urlString;
    }
);


// clear search
$('#tableSerchTag').on('click',
    function (event) {
        event.preventDefault();

        var variables = setUrlVariables();

        delete variables.search;

        var pageBaseUrl = getUrlWithoutVariables();

        var urlString = pageBaseUrl + "?";

        var counter = 0;
        for(var key in variables) {

            if(counter != 0) {

                urlString += "&";
            }

            urlString += key + "=" + variables[key];

            counter++;
        }

        window.location = urlString;
    }
);


/************************************************************
*                                                           *
*           SORTING BY COLUMN NAME (ASC/DESC)               *
*                                                           *
************************************************************/


// sorting
$('.sort-cell').on('click',
    function (event) {
        event.preventDefault();

        var orderBy = $(this).attr("data-order_by");
        var orderDirection = $(this).attr("data-order_direction");

        if(typeof orderBy == "undefined" || orderBy == "") {
            return;
        }

        if(typeof orderDirection != "undefined" && orderDirection != "") {

            if(orderDirection == "asc") {
                orderDirection = "desc";
            }
            else if(orderDirection == "desc") {
                orderDirection = "asc";
            }
        }
        else {
            orderDirection = "asc";
        }

        var variables = setUrlVariables();

        if(typeof variables.order_by == "undefined" || variables.order_by == null || variables.order_by == "") {
            variables.order_by = orderBy;
        }

        if(typeof variables.order_direction == "undefined" || variables.order_direction == null || variables.order_direction == "") {
            variables.order_direction = orderDirection;
        }

        var pageBaseUrl = getUrlWithoutVariables();

        var urlString = pageBaseUrl + "?";

        var counter = 0;
        for(var key in variables) {

            if(key == "order_by") {
                variables[key] = orderBy;
            }

            if(key == "order_direction") {
                variables[key] = orderDirection;
            }

            if(counter != 0) {

                urlString += "&";
            }

            urlString += key + "=" + variables[key];

            counter++;
        }

        window.location = urlString;
    }
);


// clear sorting
$('.clear-table-sorting').on('click',
    function (event) {
        event.preventDefault();
        event.stopPropagation();

        var variables = setUrlVariables();

        delete variables.order_by;
        delete variables.order_direction;

        var pageBaseUrl = getUrlWithoutVariables();

        var urlString = pageBaseUrl + "?";

        var counter = 0;
        for(var key in variables) {

            if(counter != 0) {

                urlString += "&";
            }

            urlString += key + "=" + variables[key];

            counter++;
        }

        window.location = urlString;
    }
);