$('#languageTabs li a').on('click',
	function(event) {
		event.preventDefault();

		var langId = $(this).attr('data-id');
		
		setLanguage(langId);
		setLanguageTabsActiveClass(this, langId);
		setContentActiveClass(langId);
        setElements(langId);
	}
);


function setLanguage(langId) {
	$('#langId').val(langId);
};


function getLanguageId() {

	if(languageIdExists()) return $('#langId').val();
	return LANG.ID;
};


function languageIdExists() {
	return $('#langId').length > 0;
};


function setLanguageTabsActiveClass(element, langId) {

	var currentLangId = getLanguageId();

	if(langId == currentLangId) {

		removeLanguageTabsActiveClass();

		$(element).parent('li').addClass('active');
		$(element).addClass('active');
	}
};


function setModalLanguageTabsActiveClass(langId) {

    removeLanguageTabsActiveClass();

    if(typeof langId === 'undefined' || langId == null) {

    	langId = $('#languageTabs li a').eq(0).attr('data-id');
    }

    setLanguage(langId);

    $('#languageTabs li a').each(
        function() {

            var tabId = $(this).attr('data-id');

            if(tabId == langId) {
                $(this).parent('li').addClass('active');
                $(this).addClass('active');
            }
        }
    );
};


function removeLanguageTabsActiveClass() {
	$('#languageTabs li').removeClass('active');
	$('#languageTabs li a').removeClass('active');
};


function setContentActiveClass(langId) {

	var currentLangId = getLanguageId();

	if(langId == currentLangId) {

		var languageWrapper = languageWrapperElem(langId);

		$('.language-wrapper').removeClass('active');
		$(languageWrapper).addClass('active');
	}
};