$(document).ready(
   function() {
      setSortableItems();
   }
);


var $mediaModal = $('#media-modal');
var $youtubeVideoModal = $('#youtube-video-modal');
var $galleryItemDescModal = $('#gallery-item-desc-modal');
var $galleriesModal = $('#galleries-modal');


/********************************* EVENT HANDLERS ********************************/


// open media modal
$(document).on('click', '.open-media-modal',
    function(event) {
      event.preventDefault();

      var currentType = $mediaModal.attr('data-type');
      var type = $(this).attr('data-type');

      $mediaModal.attr('data-type', type);

      if(currentType != type) {
          changeModalWarningMessage(type);
          displayMediaModalItems();
      }

      showMediaModal();
    }
);


// open youtube video modal
$(document).on('click', '.open-youtube-video-modal',
    function(event) {
      event.preventDefault();

      showYoutubeVideoModal();
    }
);


// on close - youtube video modal
$youtubeVideoModal.on('hide.bs.modal', 
   function() {
      setTimeout(
         function() {
            emptyYoutubeVideoField();
         }, 800
      );
   }
);



// open gallery item description modal
var galleryItemWrapper;
$(document).on('click', '.open-gallery-item-desc-modal',
    function(event) {
        event.preventDefault();
        event.stopPropagation();

        galleryItemWrapper = $(this).parents('.gallery-image-wrapper');
        showGalleryItemDescModal();
    }
);


// on close - gallery item description modal
$galleryItemDescModal.on('hide.bs.modal',
    function() {
        $('#saveGalleryItemDesc').removeAttr('data-id');
        $('#galleryItemDesc').val('');
    }
);


$(document).on('click', '.add-image',
   function(event) {
      event.preventDefault();

      var fileName = $(this).attr('data-file_name');

      var modalType = $mediaModal.attr('data-type');

      hideMediaModal();
      displayPageImage(fileName, modalType);
   }
);


$(document).on('click', '.remove-image',
   function(event) {
      event.preventDefault();

      var type = $(this).attr('data-type');

      removePageImage(this, type);
   }
);


$(document).on('click', '.add-gallery',
   function(event) {
      event.preventDefault();

      var id = $(this).attr('data-id');

      var shortCode = '{' + NC_GALLERY_LABEL + '=' + id + '}';
      var shortCodeField = $('.cke_dialog input:first');
      $(shortCodeField).click();
      $(shortCodeField).focus();
      $(shortCodeField).val(shortCode);
      $(shortCodeField).trigger('change');
      $(shortCodeField).blur();
      hideGalleriesModal();
   }
);


$(document).on('click', '.add-document',
   function(event) {
      event.preventDefault();

      var fileName = $(this).attr('data-file_name');

      hideMediaModal();
      displayPageDocument(fileName);
   }
);


$(document).on('click', '.remove-document',
   function(event) {
      event.preventDefault();
      removePageDocument();
   }
);


$(document).on('click', '.modal-pagination a',
   function(event) {
      event.preventDefault();

      var page = $(this).attr('data-page');

      displayMediaModalItems(page);
   }
);


$(document).on('click', '#saveYoutubeVideo',
   function(event) {
      event.preventDefault();

      var url = $('#youtubeVideoUrl').val();

      if(url != '') {

         hideYoutubeVideoModal();
         renderYoutubeVideo(url);
      }
      else{
         $('#youtubeVideo').addClass('warning');
      }
   }
);


$(document).on('click', '#saveGalleryItemDesc',
    function (event) {
        event.preventDefault();

        displayGalleryItemDescription();
        hideGalleryItemDescModal();
    }
);


$(document).on('click', '.open-gallery-modal',
   function (event) {
      event.preventDefault();

      showGalleriesModal();
   }
);


/*********************************** FUNCTIONS **********************************/

var myDropzone;
function initMediaModalDropZone() {

   Dropzone.autoDiscover = false;

   myDropzone = new Dropzone('#mediaModalUpload', 
      {
         url: BASE_URL + '/content/upload',
         maxFiles: 1, // Maximum Number of Files
         maxFilesize: 200,
         dictDefaultMessage: NC_TRANSLATION[LANG.ALIAS].sUploadFiles
      }
   );

   myDropzone.on('success', function(file, response) {
      
      // console.log(response);
      
      var status = response.data.status;

      if(status != MEDIA_UPLOAD_STATUS.FAILED) {

         var type = file.type;

         var mime = type.split("/");

         var mimeType = mime[0];
         
         setTimeout(
            function() {

               var media = response.data.media;

               var modalType = $mediaModal.attr('data-type');

               hideMediaModal();

               myDropzone.removeFile(file);

               if(modalType == MODAL_TYPE.TEXT_EDITOR) {
                  displayTextEditorMediaUrl(media.file_name);
               }
               else if(modalType == MODAL_TYPE.DOCUMENT) {

                  if(mimeType != 'image') {
                     displayPageDocument(media.id, media.title);
                  }
               }
               else{
                  if(mimeType == 'image') {
                     displayPageImage(media.file_name, modalType);
                  }
               }
            }, 1200
         );
      }
         
   })
   .on('error', function(file, response) {
      var self = this;
      var file = file;
      setTimeout(
         function() {
            hideMediaModal();
            
            if(exists(file)) {
               self.removeFile(file);
            }

         }, 1200
      );
   });
};


function displayMediaModalItems(page) {
   
   showModalLoading();

   if(typeof page == "undefined" || page == null) {
       page = 1;
   }

   var fileType = setMediaFileType();

   ajaxCall(
      '/content/media/pagination/',
      'POST',
      'page=' + page + '&items_per_page=' + ITEMS_PER_PAGE.MODAL_IMAGES + '&mime_type=' + fileType + '&order_by=id&order_direction=desc',
      function(response) {

         // console.log(response);

         if(ajaxSuccess(response)) {

            removeModalImages();

            var total = response.data.total;
            var items = response.data.items;

            var html = NC_TRANSLATION[LANG.ALIAS].sNoMedia;

            if(total != 0) {

               html = renderMediaModalPagination(total, page);

               if(fileType == "image") {

                  html += renderModalImages(items);
               }
               else{
                  html += renderModalDocuments(items);
               }

               html += renderMediaModalPagination(total, page);

            }

            appendModalImages(html);
            hideModalLoading();

         }else defaultErrorHandler(response);
      }
   );
};


function displayPageImage(fileName, type) {

   if(type == MODAL_TYPE.INTRO_IMAGE) {
      renderPageIntroImage(fileName);
      $(introImageFieldElem).val(fileName);
   }
   else if(type == MODAL_TYPE.IMAGE) {
      renderPageFeaturedImage(fileName);
      $(imageFieldElem).val(fileName);
   }
   else if(type == MODAL_TYPE.GALLERY_IMAGE) {
      renderPageGalleryImage(fileName);
   }
   else if(type == MODAL_TYPE.TEXT_EDITOR) {
      displayTextEditorMediaUrl(fileName);
   }
};


function displayPageDocument(fileName) {

   removePageDocument();

   var html = renderPageDocument(fileName);

   $(documentContainerElem).append(html);
   $(documentFieldElem).val(fileName);
};


function displayTextEditorMediaUrl(fileName) {
   var url = MEDIA_URL + '/' + fileName;
   var urlInput = $('.cke_dialog input:first'); 
   var altInput = $('.cke_dialog input:eq(1)'); 
   $(urlInput).click();
   $(urlInput).focus();
   $(urlInput).val(url);
   $(urlInput).trigger('change');
   $(urlInput).blur();
   $(altInput).click();
   $(altInput).focus();
   hideMediaModal();
};


function removePageImage(element, type) {

   if(type == MODAL_TYPE.INTRO_IMAGE) {
      removePageIntroImage();
   }
   else if(type == MODAL_TYPE.IMAGE) {
      removePageFeaturedImage();
   }
   else if(type == MODAL_TYPE.GALLERY_IMAGE || type == MODAL_TYPE.YOUTUBE_VIDEO) {
      removePageGalleryImage(element);
   }
};


function removePageIntroImage() {
   $(introImageContainerElem).html(' ');
   $(introImageFieldElem).val('');
};


function removePageFeaturedImage() {
   $(imageContainerElem).html(' ');
   $(imageFieldElem).val('');
};


function removePageGalleryImage(element) {
   $(element).parents('.item-image-wrapper').remove();
};


function removePageDocument() {
   $(documentContainerElem).html(' ');
   $(documentFieldElem).val('');
};


function setMediaFileType() {

   var modalType = $mediaModal.attr('data-type');

   if(modalType != MODAL_TYPE.DOCUMENT) {
       return 'image';
   }

   return 'application';
};


function showMediaModal() {
   $mediaModal.modal('show');
};


function hideMediaModal() {
   // $mediaModal.removeAttr('data-type');
   $mediaModal.modal('hide');
};


function showGalleriesModal() {
   $galleriesModal.modal('show');
};


function hideGalleriesModal() {
   $galleriesModal.modal('hide');
};


function appendModalImages(html) {
   $mediaModal.find('.media-modal-items-wrapper').append(html);
};


function removeModalImages() {
   $mediaModal.find('.media-modal-items-wrapper').html('');
};


function showModalLoading() {
   $mediaModal.find('.spinner-overlay').show();
};


function hideModalLoading() {

   setTimeout(function() {
      $mediaModal.find('.spinner-overlay').fadeOut();
   }, 800);
};


function changeModalWarningMessage(type) {
   
   hideModalWarningMessages();

   if(type == MODAL_TYPE.DOCUMENT) {
      showModalDocumentMessage();
   }
   else{
      showModalImageMessage();
   }
};


function displayGalleryItemDescription() {

   var value = $('#galleryItemDesc').val();

   var desc = NC_TRANSLATION[LANG.ALIAS].sNoDescription;
   if(value != '') {

      desc = value;

      var galleryItemJson  = $(galleryItemWrapper).find('.gallery-item').val();

      var galleryItemValue = JSON.parse(galleryItemJson);
      galleryItemValue.description = value;

      $(galleryItemWrapper).attr('data-desc', htmlentities(value));
      $(galleryItemWrapper).find('.gallery-item').val(JSON.stringify(galleryItemValue));
   }

   $(galleryItemWrapper).find('.item-desc').text(truncateString(desc, 50));
};


function showModalImageMessage() {   
   $('.media-modal-message.image').show();
};


function showModalDocumentMessage() {   
   $('.media-modal-message.document').show();
};


function hideModalWarningMessages() {
   $('.media-modal-message.image').hide();
   $('.media-modal-message.document').hide();
};


function showYoutubeVideoModal() {
   $youtubeVideoModal.modal('show');
};


function hideYoutubeVideoModal() {
   $youtubeVideoModal.modal('hide');
};


function emptyYoutubeVideoField() {
   $('#youtubeVideo').val('');
};


function showGalleryItemDescModal() {
    populateGalleryItemDescField();
    $galleryItemDescModal.modal('show');
};



function hideGalleryItemDescModal() {
    $galleryItemDescModal.modal('hide');
    emptyGalleryItemDescField();
};


function populateGalleryItemDescField() {
    var desc = $(galleryItemWrapper).attr('data-desc');
    $('#galleryItemDesc').val(desc);
};


function emptyGalleryItemDescField() {
    galleryItemWrapper = null;
    $('#galleryItemDesc').val('');
};


/************************************* RENDER ************************************/


function renderPageIntroImage(fileName) {

   var html = '<div class="panel-image-wrapper" style="background-image: url(' + MEDIA_URL + '/' + fileName + ')">';
      html += '<button class="btn btn-sm btn-danger remove-image" data-type="' + MODAL_TYPE.INTRO_IMAGE + '"><i class="icons-office-52"></i></button>';
   html += '</div>';

   $(introImageContainerElem).html(html);
};


function renderPageFeaturedImage(fileName) {

   var html = '<div class="panel-image-wrapper" style="background-image: url(' + MEDIA_URL + '/' + fileName + ')">';
         html += '<button class="btn btn-sm btn-danger remove-image" data-type="' + MODAL_TYPE.IMAGE + '"><i class="icons-office-52"></i></button>';
      html += '</div>';

   $(imageContainerElem).html(html);
};


function renderPageGalleryImage(fileName) {

   var itemObj = {
      type: "media",
      //id: id,
      file_name: fileName,
      description: ""
   };

   var itemJson = JSON.stringify(itemObj);

   var html = '<div class="gallery-image-wrapper item-image-wrapper" data-desc="">';
       html += '<button class="btn btn-sm btn-danger btn-transparent remove-image" data-id="" data-type="' + MODAL_TYPE.GALLERY_IMAGE + '">';
           html += '<i class="icons-office-52"></i>';
       html += '</button>';
       html += '<figure><img src="' + MEDIA_URL + '/' + fileName + '" /></figure>';
       html += '<button type="button" class="btn btn-warning btn-transparent text-left open-gallery-item-desc-modal">';
            html += '<i class="icon-pencil"></i>' + NC_TRANSLATION[LANG.ALIAS].sAddDescription;
       html += '</button>';
       html += '<div class="item-image-desc">';
           html += '<p class="item-desc">' + NC_TRANSLATION[LANG.ALIAS].sNoDescription + '</p>';
       html += '</div>';
       html += "<input type='hidden' class='gallery-item' value='" + itemJson + "' />";
   html += '</div>';

   $(galleryContainerElem).append(html);
};


function renderPageDocument(fileName) {

    var html = '<div class="media-document-wrapper">';
        html += '<figure>';
            html += '<img src="' + BASE_URL + '/modules/admin/css/img/icon-document.png" />';
            html += '<figcaption>' + fileName + '</figcaption>';
        html += '</figure>';
        html += '<button class="btn btn-sm btn-danger btn-transparent remove-document">';
            html += '<i class="icons-office-52"></i>';
        html += '</button>';
    html += '</div>';
   
   return html;
};


function renderModalImages(data) {

   var html = '<div class="row clearfix">';

   for(var key in data) {

      var media = data[key];

      html += '<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">';
         html += '<figure>';
             html += '<a href="#" data-id="' + media.id + '" data-title="' + media.title + '" data-file_name="' + media.file_name + '" class="add-image">';
                 html += '<img src="' + MEDIA_THUMBS_URL + '/' + media.file_name + '" alt="' + media.title + '" />';
                 html += '<figcaption>' + media.title + '</figcaption>'; 
             html += '</a>';   
         html += '</figure>';
      html += '</div>';
   }

   html += '</div>';

   return html;
};


function renderModalDocuments(data) {

   var html = '<div class="row clearfix">';

   for(var key in data) {

       var media = data[key];

      html += '<div  class="col-lg-2 col-md-3 col-sm-4 col-xs-12">';
         html += '<figure>';
            html += '<a href="#" data-id="' + media.id + '" data-title="' + media.title + '" data-file_name="' + media.file_name + '" class="add-document">';
               html += '<img src="' + BASE_URL + '/modules/admin/css/img/icon-document.png" alt="icon-document" />';
               html += '<figcaption>' + media.title + '</figcaption>';      
            html += '</a>';
         html += '</figure>'; 
      html += '</div>'; 
   }

   html += '</div>';

   return html;
};


function renderMediaModalPagination(total, page) {

   var pagination = Math.ceil(total / ITEMS_PER_PAGE.MODAL_IMAGES);

   if(pagination == 0 || pagination == 1) return '<div class="modal-pagination"></div>';

   var html = '<div class="modal-pagination">';
       html += '<ul class="pagination">';

      for(var i = 1; i <= pagination; i++) {

          var activeClass = page == i ? ' class="active"' : '';

         html += '<li'+activeClass+'><a href="#" data-page="' + i + '">' + i + '</a></li>';
      }

      html += '</ul>';
   html += '</div>';

   return html;
};


function renderYoutubeVideo(url) {

   var code = getYouTubeVideoCode(url);

   var itemObj = {
         type: "youtube_code",
         code: code,
         description: ""
      };

   var itemJson = JSON.stringify(itemObj);

    var html = '<div class="gallery-image-wrapper item-image-wrapper" data-desc="">';
        html += '<button class="btn btn-sm btn-danger btn-transparent remove-image" data-code="' + code + '" data-type="' + MODAL_TYPE.YOUTUBE_VIDEO + '">';
            html += '<i class="icons-office-52"></i>';
        html += '</button>';
        html += '<figure>';
            html += '<img class="gallery-youtube-icon" src="' + BASE_ADMIN_URL + '/css/img/icon-youtube.png" alt="icon-youtube" />';
            html += '<img class="" src="http://img.youtube.com/vi/' + code + '/0.jpg" alt="video-youtube" />';
        html += '</figure>';
        html += '<button type="button" class="btn btn-warning btn-transparent text-left open-gallery-item-desc-modal">';
            html += '<i class="icon-pencil"></i>' + NC_TRANSLATION[LANG.ALIAS].sAddDescription;
        html += '</button>';
        html += '<div class="item-image-desc">';
            html += '<p class="item-desc">' + NC_TRANSLATION[LANG.ALIAS].sNoDescription + '</p>';
        html += '</div>';
        html += "<input type='hidden' class='gallery-item' value='" + itemJson + "' />";
    html += '</div>';

   $(galleryContainerElem).append(html);
};