function exists(data) {
   return typeof data != "undefined" && data != null && data != '';
};


function ajaxSuccess(response) {
   return typeof response != "undefined" && response.success;
};


function actionAllowed(element) {
   return !$(element).hasClass('disabled');
};


// youtube video code from url
function getYouTubeVideoCode(url) {

   var ID = '';

   url = url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);

   if(url[2] !== undefined) {
      ID = url[2].split(/[^0-9a-z_\-]/i);
      ID = ID[0];
   } else {
      ID = url;
   }

   return ID;
};


function setYouTubeVideoCode(resource, langSuffix) {

   var url = $('#' + RESOURCE + '-youtube_video_code' + langSuffix).val();
   var code = getYouTubeVideoCode(url);
   $('#' + RESOURCE + '-youtube_video_code' + langSuffix).val(code);
}

function setFormRang() {
   var rangString = $(".rang").val().trim(); 
   if(rangString == "" || isNaN(parseInt(rangString))) $(".rang").val(0);
};


var RESOURCE = $('#field-resource').val();
function setResource() {
   if(RESOURCE == '') RESOURCE = $('#field-resource').val();
};


function validateRequiredFields() {

   var proceed = true;
   $('.required').each(
      function() {
         if($(this).val() == '') {
            proceed = true;
            $(this).attr('placeholder', 'required');
         }
      }
   );

   return proceed;
};


function printContent(div) {
   var newWin= window.open('', 'win');
   newWin.document.write(div.html());
   newWin.location.reload();
   newWin.focus();
   newWin.print();
   newWin.close();
};


function disableActionButtons() {
   $('.actions-panel').find('.btn').addClass('disabled');
   $('.actions-panel').find('.action-buttons-overlay').show();
   $('.modal-footer.action-buttons').find('.btn').addClass('disabled');
   $('.modal-footer.action-buttons').find('.action-buttons-overlay').show();
};


function enableActionButtons() {
   $('.actions-panel').find('.btn').removeClass('disabled');
   $('.actions-panel').find('.action-buttons-overlay').fadeOut();
   $('.modal-footer.action-buttons').find('.btn').removeClass('disabled');
   $('.modal-footer.action-buttons').find('.action-buttons-overlay').fadeOut();
};


function setFormParentId() {

   var langId = getLanguageId();
   var tree = treeElem(langId);
   var parentIdFormField = parentIdFormFieldElem(langId);

   var parentId;
   if($(tree).jstree().get_selected(true)[0]) {
      parentId = $(tree).jstree().get_selected(true)[0].id;
   }
   else {
      parentId = 0;
      // openWarningPopup(NC_TRANSLATION[LANG.ALIAS].sRootCategory,
      //    function() {
      //       return true;
      //    }
      // );
   }

   $(parentIdFormField).val(parentId);
};


function getGalleryValues() {

   var items = [];

   if($(galleryContainerElem).find('.gallery-item').length > 0) {

      var $itemFields = $(galleryContainerElem).find('.gallery-item');

      if($itemFields.length == 1) {

         var value = $itemFields.val();
         items.push(JSON.parse(value));
      }
      else{

         $itemFields.each(
            function() {

               var value = $(this).val();
               items.push(JSON.parse(value));
            }
         );
      }
   }
   return JSON.stringify(items);
};


function setGallery() {

   var gallery = getGalleryValues();
   $(galleryJsonFieldElem).val(gallery);
};


function setSortableItems() {

   var $galleryContainer = $('.gallery-container');

   if($galleryContainer.length > 0) {

      $galleryContainer.sortable({
         containment: 'parent', 
         tolerance: 'pointer',
         opacity: 0.60
      });
   }
};

var introImageContainerElem;
var imageContainerElem;
var documentContainerElem;
var galleryContainerElem;
var introImageFieldElem;
var imageFieldElem;
var documentFieldElem;
var galleryJsonFieldElem;
function setElements(langId = null) {
   var langSuffix = getLanguageSuffix(langId);
   introImageContainerElem = '#introImageContainer' + langSuffix;
   imageContainerElem      = '#imageContainer' + langSuffix;
   documentContainerElem   = '#documentContainer' + langSuffix;
   galleryContainerElem    = '#galleryContainer' + langSuffix;
   introImageFieldElem     = '.intro-image-field' + langSuffix;
   imageFieldElem          = '.image-field' + langSuffix;
   documentFieldElem       = '.document-field' + langSuffix;
   galleryJsonFieldElem    = '.gallery-json-field' + langSuffix;
};

function parentIdFormFieldElem(langId) {
   var langSuffix = getLanguageSuffix(langId);
   return '.parentId-field' + langSuffix;
};


function treeElem(langId) {
   var langSuffix = getLanguageSuffix(langId);
   return '#tree3' + langSuffix;
};


function treeGridRootCategoryElem(langId) {
   var langSuffix = getLanguageSuffix(langId);
   return '#root-category-selector' + langSuffix;
};


function languageWrapperElem(langId) {
    var langSuffix = getLanguageSuffix(langId);
    return '#languageWrapper' + langSuffix;
};


function getLanguageSuffix(langId = null) {

   if(typeof langId == 'undefined' || langId == null) langId = getLanguageId();

   return "-langId-" + langId;
};

/****************************** POPUP MESSAGES ******************************/


function openMessagePopup(message, callback) {
   swal({
      title: NC_TRANSLATION[LANG.ALIAS].sMessage,
      text: message,
      closeOnConfirm: true
   },
   function (isConfirm) {
      if(isConfirm) if(typeof callback != "undefined") callback(); 
   });
};


function openWarningPopup(message, callback) {
   swal({
      title: NC_TRANSLATION[LANG.ALIAS].sWarning,
      text: message,
      type: "warning",
   },
   function (isConfirm) {
      if(isConfirm) if(typeof callback != "undefined") callback(); 
   });
};


/************************** /end of popup messages ***************************/



/****************************** PUBLISH ITEMS ******************************/


function renderPublishChanges(element, action) {

    $(element).addClass('hidden');

    var status;
    if(action == 'publish') {
      status = NC_TRANSLATION[LANG.ALIAS].sPublished;
      $(element).next('.publish').removeClass('hidden');
    }
    else if(action == 'unpublish') {
      status = NC_TRANSLATION[LANG.ALIAS].sUnpublished;
      $(element).prev('.publish').removeClass('hidden');
    }

    $(element).parents('.action-buttons').find('.item-status').text(status);
};


/************************** end of publish items ***************************/



/********************************* CKEDITOR *********************************/

function initCkeditor() {

   CKEDITOR.config.allowedContent = true;

   if($('#articleInsertPage').length > 0) {
      CKEDITOR.config.extraPlugins = 'gallery';
   }
   // CKEDITOR.config.extraAllowedContent = 'data-*';
   // CKEDITOR.config.extraAllowedContent = 'p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*}';

   // CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR; // <br /> instead <p> tag on every new line 

   CKEDITOR.on('dialogDefinition', function (e) {
      var dialogName = e.data.name;
      var dialog = e.data.definition.dialog;

      dialog.on('show', function () {
          if(dialogName == 'image') {
              var dialogFirstInput = this.getElement().find('input').getItem(0);
              if($('.open-media-modal-texteditor').length == 0) {
                  var btn = renderOpenBrowserButton();
                  $(btn).insertAfter('#' + dialogFirstInput.$.id);
              }
          }
      });
      dialog.on('hide', function () {});
   });
};


function renderOpenBrowserButton() {

   var html  = '<a class="cke_dialog_ui_button cke_dialog_ui_button_ok open-media-modal-texteditor open-media-modal" title="Browse" data-type="' + MODAL_TYPE.TEXT_EDITOR + '">';
         html += '<span class="cke_dialog_ui_button">Browse</span>';
      html += '</a>';
   return html;
};


/***************************** /end of ckeditor ******************************/

function truncateString(string, length = 255) {

   if(string.length > length) {
       var newString = string.substring(0, length);
       return newString + '...';
   }
   return string;

}


function stripBeginningAndEndingSlashes(string) {
    return string.replace(/^\/|\/$/g, '');
};


function getUrlRoute() {
    var currentUrl = window.location.href;
    var route = currentUrl.replace(BASE_URL + '/admin', '');
    return stripBeginningAndEndingSlashes(route);
};


function getUrlWithoutVariables() {

    var currentUrl = window.location.href;
    var currentUrlParts = currentUrl.split("?");
    var pageBaseUrl = stripBeginningAndEndingSlashes(currentUrlParts[0]);

    return pageBaseUrl;
};

function getUrlVariables() {

    var query = window.location.search.substring(1);
    if(query == "") return false;
    var variables = query.split("&");

    if (typeof variables == "undefined" || variables.length == 0) return false;

    var varsArray = [];
    for (var i = 0; i < variables.length; i++) {

       var pair = variables[i].split("=");

        varsArray[pair[0]] = pair[1];
    }

    return varsArray;
};


function initTooltips() {
   $('[data-toggle="tooltip"]').tooltip();
};


function htmlentities(str) {
   return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
};


function eventPreventDefault(event) {
    event.preventDefault ? event.preventDefault() : (event.returnValue = false);
};