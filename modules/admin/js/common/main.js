$(document).ready(
    function () {
        setElements();
    }
);

$('.new-entry').on('click',
    function(event) {
        event.preventDefault();
        $('#modal-entry input').val('');
    }
);


$('.set-language').on('click',
    function(event) {
        event.preventDefault();

        var id = $(this).attr('data-id');

        ajaxCall(
            '/content/languages/set/',
            'POST',
            'id=' + id,
            function (response) {

                if(ajaxSuccess(response)) {
                    location.reload();
                }
                else defaultErrorHandler(response);
            }
        );
    }
);


$('#form-modal').on('hide.bs.modal',
    function(event) {
        setTimeout(
            function() {
                removeLanguageTabsActiveClass();
                $('.form-modal-content').html('');
            }, 800
        );
    }
);


$('.gallery-wrapper-toggler').on('click',
    function (event) {
        event.preventDefault();

        var $wrapper  = $(this).parent('.toggler-wrapper');
        var $gallery = $wrapper.next('.gallery-wrapper');

        $wrapper.toggleClass('active');

        if($wrapper.hasClass('active')) {
            $wrapper.find('i').removeClass('fa-chevron-down');
            $wrapper.find('i').addClass('fa-chevron-up');
            $gallery.slideDown();
        }
        else{
            $wrapper.find('i').removeClass('fa-chevron-up');
            $wrapper.find('i').addClass('fa-chevron-down');
            $gallery.slideUp();
        }
    }
);


$(document).on('ifChanged', '.publish-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.published').val(value);
    }
);

/****************************FILTERS FOR FACE *********************************/

$(document).on('ifChanged', '.sourceMarine-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.sourceMarine').val(value);
    }
);

$(document).on('ifChanged', '.bbKreme-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.bbKreme').val(value);
    }
);

$(document).on('ifChanged', '.eveilAlaMer-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.eveilAlaMer').val(value);
    }
);

$(document).on('ifChanged', '.shotMaske-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.shotMaske').val(value);
    }
);

$(document).on('ifChanged', '.coldCreamMarine-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.coldCreamMarine').val(value);
    }
);

$(document).on('ifChanged', '.poklonSetovi-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.poklonSetovi').val(value);
    }
);

$(document).on('ifChanged', '.cistaciPene-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        var test =  $('#insert-form' + langSuffix).find('.cistaciPene').val(value);
        console.log('.cistaciPene-checkbox');
    }
);

$(document).on('ifChanged', '.maskeIpilinzi-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.maskeIpilinzi').val(value);
    }
);

$(document).on('ifChanged', '.pureteMarine-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.pureteMarine').val(value);
    }
);

$(document).on('ifChanged', '.mistoviIlosioni-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.mistoviIlosioni').val(value);
    }
);

$(document).on('ifChanged', '.serumiIkoncentrati-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.serumiIkoncentrati').val(value);
    }
);

$(document).on('ifChanged', '.ocnaRegija-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.ocnaRegija').val(value);
    }
);

$(document).on('ifChanged', '.dnevnaNega-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.dnevnaNega').val(value);
    }
);

$(document).on('ifChanged', '.nocnaNega-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.nocnaNega').val(value);
    }
);

$(document).on('ifChanged', '.bbKremeNamena-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.bbKremeNamena').val(value);
    }
);

/****************************END OF FILTERS FOR FACE *********************************/
/****************************FILTERS FOR ANTI AGE ***********************************/

$(document).on('ifChanged', '.spirulinaBoostAntiAge-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.spirulinaBoostAntiAge').val(value);
    }
);

$(document).on('ifChanged', '.hyaluProCollagenAntiAge-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.hyaluProCollagenAntiAge').val(value);
    }
);

$(document).on('ifChanged', '.silicijumAntiAge-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.silicijumAntiAge').val(value);
    }
);

$(document).on('ifChanged', '.exceptionAntiAge-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.exceptionAntiAge').val(value);
    }
);

$(document).on('ifChanged', '.lumiereAntiAge-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.lumiereAntiAge').val(value);
    }
);

$(document).on('ifChanged', '.prodigeDesOceansAntiAge-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.prodigeDesOceansAntiAge').val(value);
    }
);

$(document).on('ifChanged', '.shotMaskeAntiAge-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        var test =  $('#insert-form' + langSuffix).find('.shotMaskeAntiAge').val(value);
        console.log('.cistaciPene-checkbox');
    }
);

$(document).on('ifChanged', '.poklonSetoviAntiAge-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.poklonSetoviAntiAge').val(value);
    }
);

$(document).on('ifChanged', '.losioniAntiAge-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.losioniAntiAge').val(value);
    }
);

$(document).on('ifChanged', '.maskeiPilinziAntiAge-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.maskeiPilinziAntiAge').val(value);
    }
);

$(document).on('ifChanged', '.serumiAntiAge-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.serumiAntiAge').val(value);
    }
);

$(document).on('ifChanged', '.ocnaRegijaAntiAge-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.ocnaRegijaAntiAge').val(value);
    }
);

$(document).on('ifChanged', '.dnevnaNegaAntiAge-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.dnevnaNegaAntiAge').val(value);
    }
);

$(document).on('ifChanged', '.nocnaNegaAntiAge-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.nocnaNegaAntiAge').val(value);
    }
);

$(document).on('ifChanged', '.puderiAntiAge-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.puderiAntiAge').val(value);
    }
);

/**************************END OF FILTERS FOR ANTI AGE ******************************/
/****************************FILTERS FOR BODY ***********************************/

$(document).on('ifChanged', '.morskiProgramBody-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        var test =  $('#insert-form' + langSuffix).find('.morskiProgramBody').val(value);
        console.log('.cistaciPene-checkbox');
    }
);

$(document).on('ifChanged', '.coldCreamMarineBody-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.coldCreamMarineBody').val(value);
    }
);

$(document).on('ifChanged', '.mrsavljenjeBody-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.mrsavljenjeBody').val(value);
    }
);

$(document).on('ifChanged', '.spaRitualiPacifikBody-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.spaRitualiPacifikBody').val(value);
    }
);

$(document).on('ifChanged', '.spaRitualiIndoceaneBody-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.spaRitualiIndoceaneBody').val(value);
    }
);

$(document).on('ifChanged', '.spaRitualiAtlantikBody-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.spaRitualiAtlantikBody').val(value);
    }
);

$(document).on('ifChanged', '.spaRitualiArktikBody-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.spaRitualiArktikBody').val(value);
    }
);

$(document).on('ifChanged', '.promoBody-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.promoBody').val(value);
    }
);

$(document).on('ifChanged', '.poklonSetoviBody-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.poklonSetoviBody').val(value);
    }
);

/**************************END OF FILTERS FOR BODY ******************************/
/****************************FILTERS FOR MAN ***********************************/

$(document).on('ifChanged', '.brijanjeMan-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.brijanjeMan').val(value);
    }
);

$(document).on('ifChanged', '.negaMan-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.negaMan').val(value);
    }
);

$(document).on('ifChanged', '.poklonSetoviMan-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.poklonSetoviMan').val(value);
    }
);

/****************************END OF FILTERS FOR MAN *********************************/
/****************************FILTERS FOR SUN ***********************************/

$(document).on('ifChanged', '.liceSun-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.liceSun').val(value);
    }
);

$(document).on('ifChanged', '.teloSun-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.teloSun').val(value);
    }
);

$(document).on('ifChanged', '.posleSuncanjaSun-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.posleSuncanjaSun').val(value);
    }
);

$(document).on('ifChanged', '.samopotamnjivanjeSun-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.samopotamnjivanjeSun').val(value);
    }
);

/**************************END OF FILTERS FOR SUN ******************************/
/****************************FILTERS FOR SUPLEMENT ***********************************/

$(document).on('ifChanged', '.mrsavljenjeSuplement-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.mrsavljenjeSuplement').val(value);
    }
);

$(document).on('ifChanged', '.suncanjeSuplement-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.suncanjeSuplement').val(value);
    }
);

$(document).on('ifChanged', '.antiAgeSuplement-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.antiAgeSuplement').val(value);
    }
);

$(document).on('ifChanged', '.blagostanjeSuplement-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.blagostanjeSuplement').val(value);
    }
);

$(document).on('ifChanged', '.organskaInfuzijaSuplement-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.organskaInfuzijaSuplement').val(value);
    }
);

/****************************END OF FILTERS SUPLEMENT *********************************/

$(document).on('ifChanged', '.allow-comments-checkbox',
    function() {

        var value = this.checked ? 1 : 0;
        var langId = $(this).data('lang_id');

        //console.log("langId: " + langId + ", value: " + value);

        var langSuffix = getLanguageSuffix(langId);

        $('#insert-form' + langSuffix).find('.allow-comments').val(value);
    }
);


$('.remove-date').on('click',
    function (event) {
        event.preventDefault();

        $(this).parents('.date-field-wrapper').find('.date').val('');
    }
);