CKEDITOR.plugins.add( 'gallery', {
   icons: 'gallery',
   init: function( editor ) {
      editor.addCommand('gallery', new CKEDITOR.dialogCommand('galleryDialog'));
      editor.ui.addButton( 'Gallery', {
         label: 'Gallery',
         command: 'gallery',
         toolbar: 'insert'
      });

      CKEDITOR.dialog.add('galleryDialog', this.path + 'dialogs/gallery.js');
   }
});