CKEDITOR.dialog.add( 'galleryDialog', function( editor ) {
   return {
      title: 'Gallery Properties',
      minWidth: 400,
      minHeight: 200,
      contents: [
         {
            id: 'tab-gallery',
            label: 'Gallery',
            elements: [
               {
                  type: 'button',
                  id: 'galleryBtn',
                  label: 'Choose gallery',
                  className: 'open-gallery-modal',
                  // onClick: function() {
                  //    // this = CKEDITOR.ui.dialog.button
                  //    // alert( 'Clicked: ' + this.id );
                  // }
               },
               {
                  type: 'text',
                  id: 'gallery',
                  label: 'Shortcode',
                  validate: CKEDITOR.dialog.validate.notEmpty( "Gallery field cannot be empty." )
               }
            ]
         }
      ],
      onOk: function() {
         var dialog = this;

         var gallery = editor.document.createElement('gallery');

         gallery.setText(dialog.getValueOf('tab-gallery', 'gallery'));

         editor.insertElement(gallery);
      }
   };
});