<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo Trans::get("Sort articles"); ?> | <?php echo Trans::get("Administration dashboard"); ?></title>
    <?php $this->displayTemplate("common/meta.php", null) ?>
    <?php $this->displayTemplate("common/css.php", null) ?>
  </head>
  <body class="sidebar-light fixed-topbar theme-sltl bg-light-dark color-default" id="articleCategoriesPage">

    <?php $this->displayTemplate("layout/ienotice.php", null); ?>

    <?php $this->displaySortPageHiddenFields(); ?>

    <section>

      <?php $this->displayTemplate("layout/leftbar.php", null); ?>

      <div class="main-content">

        <?php $this->displayTemplate("layout/topbar.php", null); ?>

        <div class="page-content">

          <div class="panel">

            <?php $this->displayPanelHeader(array("title" => $this->pageTitle)); ?>

            <div class="panel-content">

              <div class="main-wrapper">

                <div id="tree"></div>

              </div><!-- /end of main-wrapper -->

            </div><!-- /end of panel-content -->

          </div><!-- /end of panel -->

        </div><!-- /end of page-content -->

        <?php $this->displayTemplate("layout/footer.php", null); ?>

      </div><!-- /end of main-content -->

    </section>

    <?php $this->displayModals(); ?>
    <!-- /end of modals -->

    <?php $this->displayTemplate("common/scripts.php", null); ?>
    <?php $this->displayTemplate("common/constants.php", null); ?>
    <script src="<?php echo Conf::get('url') . "/modules/admin"; ?>/assets/global/plugins/jstree/jstree.js"></script><!-- JSTree -->
    <script src="<?php echo Conf::get('url') . "/modules/admin"; ?>/js/articles/articles_sort.js"></script>
  </body>
</html>