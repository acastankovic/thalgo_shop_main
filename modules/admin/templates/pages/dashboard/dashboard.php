<!DOCTYPE html>
<html lang="en">
   <head>    
      <title><?php echo Trans::get("Administration dashboard"); ?></title> 
      <?php $this->displayTemplate("common/meta.php", null) ?>
      <?php $this->displayTemplate("common/css.php", null) ?>
   </head>

   <!-- LAYOUT: Apply "submenu-hover" class to body element to have sidebar submenu show on mouse hover -->
   <!-- LAYOUT: Apply "sidebar-collapsed" class to body element to have collapsed sidebar -->
   <!-- LAYOUT: Apply "sidebar-top" class to body element to have sidebar on top of the page -->
   <!-- LAYOUT: Apply "sidebar-hover" class to body element to show sidebar only when your mouse is on left / right corner -->
   <!-- LAYOUT: Apply "fixed-sidebar" class to body to have fixed sidebar -->
   <!-- LAYOUT: Apply "fixed-topbar" class to body to have fixed topbar -->
   <!-- LAYOUT: Apply "rtl" class to body to put the sidebar on the right side -->
   <!-- LAYOUT: Apply "boxed" class to body to have your page with 1200px max width -->

   <!-- THEME STYLE: Apply "theme-sdtl" for Sidebar Dark / Topbar Light -->
   <!-- THEME STYLE: Apply  "theme sdtd" for Sidebar Dark / Topbar Dark -->
   <!-- THEME STYLE: Apply "theme sltd" for Sidebar Light / Topbar Dark -->
   <!-- THEME STYLE: Apply "theme sltl" for Sidebar Light / Topbar Light -->

   <!-- THEME COLOR: Apply "color-default" for dark color: #2B2E33 -->
   <!-- THEME COLOR: Apply "color-primary" for primary color: #319DB5 -->
   <!-- THEME COLOR: Apply "color-red" for red color: #C9625F -->
   <!-- THEME COLOR: Apply "color-green" for green color: #18A689 -->
   <!-- THEME COLOR: Apply "color-orange" for orange color: #B66D39 -->
   <!-- THEME COLOR: Apply "color-purple" for purple color: #6E62B5 -->
   <!-- THEME COLOR: Apply "color-blue" for blue color: #4A89DC -->

   <body class="sidebar-light fixed-topbar theme-sltl bg-light-dark color-default dashboard" id="dashboardPage">

      <?php $this->displayTemplate("layout/ienotice.php", null); ?>

      <section>

         <?php $this->displayTemplate("layout/leftbar.php", null); ?>

         <div class="main-content">

            <div class="page-content">

                <?php $this->displayTemplate("layout/topbar.php", null); ?>

            </div><!-- /end of page-content -->

            <?php $this->displayTemplate("layout/footer.php", null); ?>

         </div><!-- /end of main-content -->

      </section>

      <?php $this->displayTemplate("common/scripts.php", null); ?>
      <?php $this->displayTemplate("common/constants.php", null); ?>
   </body>
</html>