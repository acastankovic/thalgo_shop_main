<!DOCTYPE html>
<html lang="en">
   <head>    
      <title><?php echo Trans::get("Media"); ?> | <?php echo Trans::get("Administration dashboard"); ?></title> 
      <?php $this->displayTemplate("common/meta.php", null) ?>
      <?php $this->displayTemplate("common/css.php", null) ?>
      <link href="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet">
   </head>

   <body class="sidebar-light fixed-topbar theme-sltl bg-light-dark color-default" id="mediaInsertPage">

      <!--[if lt IE 7]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->

      <?php $this->displayHiddenFields(); ?>

      <section>

         <!-- BEGIN LEFT SIDEBAR -->
         <?php $this->displayTemplate("layout/leftbar.php", null); ?>
         <!-- END LEFT SIDEBAR -->

         <div class="main-content">

            <?php $this->displayTemplate("layout/topbar.php", null); ?>

            <!-- BEGIN PAGE CONTENT -->
            <div class="page-content">

               <div class="row">

                  <div class="col-md-12">

                     <div class="panel">

                        <?php $this->displayPanelHeader(array("title" => $this->displayTableTitle())); ?>

                        <?php $this->renderBackButton(array('url' => Conf::get('url') . '/admin/media')); ?>

                        <div class="panel-content">

                           <div class="row">

                              <div class="col-md-12">
                                 <form id="mediaUpload" method="post" class="dropzone dz-clickable"></form>
                              </div>

                              <div class="col-md-12 main-wrapper" style="margin-top: 20px;">

                                 <?php $this->displayTable(); ?>

                              </div><!-- /end of col-md-12 -->

                           </div><!-- /end of row -->

                        </div><!-- /end of panel-content -->

                     </div><!-- /end of panel -->

                  </div><!-- /end of col-md-12 -->

               </div><!-- /end of row -->

            </div>
         <!-- END PAGE CONTENT -->

         </div>
         <!-- END MAIN CONTENT -->

      </section>

      <?php $this->displayTemplate("layout/footer.php", null); ?>

      <!-- BEGIN FORM MODAL -->
      <?php $this->displayFormModal(); ?>
      <!-- END FORM MODAL -->

      <?php $this->displayTemplate("common/scripts.php", null); ?>
      <?php $this->displayTemplate("common/constants.php", null); ?>
      <script src="<?php echo Conf::get('url') . "/modules/admin"; ?>/assets/global/plugins/dropzone/dropzone.min.js"></script>
      <script src="<?php echo Conf::get('url') . "/modules/admin"; ?>/js/media/media.js"></script>

   </body>
</html>