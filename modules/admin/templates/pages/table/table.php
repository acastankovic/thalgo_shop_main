<!DOCTYPE html>
<html lang="en">
   <head>    
      <title><?php echo Trans::get("Tables"); ?> | <?php echo Trans::get("Administration dashboard"); ?></title> 
      <?php $this->displayTemplate("common/meta.php", null) ?>
      <?php $this->displayTemplate("common/css.php", null) ?>
   </head>
   <body class="sidebar-light fixed-topbar theme-sltl bg-light-dark color-default" id="tablePage">

      <?php $this->displayTemplate("layout/ienotice.php", null); ?>

      <input type="hidden" id="table-name" value="<?php echo $this->tableName; ?>" />

      <section>

         <?php $this->displayTemplate("layout/leftbar.php", null); ?>

         <div class="main-content">

            <?php $this->displayTemplate("layout/topbar.php", null); ?>

            <div class="page-content">

               <div class="panel">

                  <?php $this->displayPanelHeader(array("title" => Trans::get("Table data for") . ": " . $this->tableName)); ?>

                  <div class="panel-content">

                    <div class="main-wrapper">

                      <?php $this->displayTable(); ?>

                    </div><!-- /end of main-wrapper -->

                  </div><!-- /end of panel-content -->

               </div><!-- /end of panel -->

            </div><!-- /end of page-content -->

            <?php $this->displayTemplate("layout/footer.php", null); ?>

         </div><!-- /end of main-content -->

      </section>

      <?php $this->displayGenericTableModal(); ?>
      <!-- /end of modal -->

      <?php $this->displayTemplate("common/scripts.php", null); ?>
      <?php $this->displayTemplate("common/constants.php", null); ?>
      <script src="<?php echo Conf::get('url') . "/modules/admin"; ?>/js/tables/tables.js"></script>
   </body>
</html>