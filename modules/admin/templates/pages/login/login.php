<!DOCTYPE html>
<html>
    <head>
        <title><?php echo Trans::get("Normacore login"); ?></title>
        <?php $this->displayTemplate("common/meta.php", null) ?>
        <?php $this->displayTemplate("common/css.php", null) ?>
        <link rel="stylesheet" href="<?php echo Conf::get('url') . "/modules/admin"; ?>/assets/global/plugins/bootstrap-loading/lada.min.css" />
    </head>
    <body class="account separate-inputs" data-page="login">

        <?php $this->displayTemplate("layout/ienotice.php", null); ?>

        <div class="container" id="login-block">
            <div class="row">
                <div class="col-sm-12 col-md-4 col-md-offset-4">
                    <div class="account-wall">
                        <i class="user-img icons-faces-users-03"></i>
                        <form id="form-signin" class="form-signin" role="form">
                            <div class="append-icon">
                                <input type="text" name="username" id="username" class="form-control form-white username" placeholder="<?php echo Trans::get("Username"); ?>" required>
                                <i class="icon-user"></i>
                            </div>
                            <div class="append-icon m-b-20">
                                <input type="password" name="password" class="form-control form-white password" placeholder="<?php echo Trans::get("Password"); ?>" required>
                                <i class="icon-lock"></i>
                            </div>
                            <button type="submit" id="submit-form" class="btn btn-lg btn-danger btn-block ladda-button" data-style="expand-left"><?php echo Trans::get("Sign In"); ?></button>
                            <!--<div class="clearfix">
                                <p class="pull-left m-t-20"><a id="password" href="#"><?php //echo Trans::get("Forgot password"); ?>?</a></p>
                                <p class="pull-right m-t-20"><a href="user-signup-v1.html"><?php //echo Trans::get("New here? Sign up"); ?></a></p>
                            </div>-->
                        </form>
                        <form class="form-password" role="form">
                            <div class="append-icon m-b-20">
                                <input type="password" name="password" class="form-control form-white password" placeholder="Password" required>
                                <i class="icon-lock"></i>
                            </div>
                            <button type="submit" id="submit-password" class="btn btn-lg btn-danger btn-block ladda-button" data-style="expand-left"><?php echo Trans::get("Send Password Reset Link"); ?></button>
                            <div class="clearfix">
                                <p class="pull-left m-t-20"><a id="login" href="#"><?php echo Trans::get("Already got an account? Sign In"); ?></a></p>
                                <p class="pull-right m-t-20"><a href="user-signup-v1.html"><?php echo Trans::get("New here? Sign up"); ?></a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <p class="account-copyright">
                <span>Copyright © <?php echo date("Y"); ?> </span><span>Normasoft</span>.<span>All rights reserved.</span>
            </p>
        </div>        
        <script src="<?php echo Conf::get('url') . "/modules/admin"; ?>/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="<?php echo Conf::get('url') . "/modules/admin"; ?>/assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?php echo Conf::get('url') . "/modules/admin"; ?>/assets/global/plugins/gsap/main-gsap.min.js"></script>
        <script src="<?php echo Conf::get('url') . "/modules/admin"; ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo Conf::get('url') . "/modules/admin"; ?>/assets/global/plugins/backstretch/backstretch.min.js"></script>
        <script src="<?php echo Conf::get('url') . "/modules/admin"; ?>/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
        <script src="<?php echo Conf::get('url') . "/modules/admin"; ?>/assets/custom/js/common.js"></script>
        <script src="<?php echo Conf::get('url') . "/modules/admin"; ?>/assets/custom/js/pages/login-v1.js"></script>
        <?php $this->displayTemplate("common/constants.php", null); ?>
    </body>
</html>