<!-- CONSTANTS -->
<script>
    
    var BASE_URL = "<?php echo Conf::get('url'); ?>";
    var BASE_ADMIN_URL = "<?php echo Conf::get('url'); ?>/modules/admin";
    
    var MEDIA_URL = "<?php echo Conf::get('media_url'); ?>";
    var MEDIA_THUMBS_URL = "<?php echo Conf::get('media_thumbs_url'); ?>";
    
    var LANG = {
        ID: "<?php echo Trans::getLanguageId(); ?>",
        NAME: "<?php echo Trans::getLanguageName(); ?>",
        ALIAS: "<?php echo Trans::getLanguageAlias(); ?>"
    };

    var MENU_ITEM_TYPE_ID = {
        ARTICLE: "<?php echo Conf::get('menu_item_type_id')['article']; ?>",
        CATEGORY: "<?php echo Conf::get('menu_item_type_id')['category']; ?>",
        EXTERNAL_LINK: "<?php echo Conf::get('menu_item_type_id')['external_link']; ?>",
        SEPARATOR: "<?php echo Conf::get('menu_item_type_id')['separator']; ?>",
        PRODUCT: "<?php echo Conf::get('menu_item_type_id')['product']; ?>"
    };


    var MODAL_TYPE = {
        INTRO_IMAGE: "<?php echo Conf::get('modal_type')['intro_image']; ?>",
        IMAGE: "<?php echo Conf::get('modal_type')['image']; ?>",
        GALLERY_IMAGE: "<?php echo Conf::get('modal_type')['gallery_image']; ?>",
        DOCUMENT: "<?php echo Conf::get('modal_type')['document']; ?>",
        TEXT_EDITOR: "<?php echo Conf::get('modal_type')['text_editor']; ?>",
        YOUTUBE_VIDEO: "<?php echo Conf::get('modal_type')['youtube_video']; ?>"
    }; 

    var MEDIA_UPLOAD_STATUS = {
        SUCCESS: "<?php echo Conf::get('media_upload_status')['success']; ?>",
        ALREADY_UPLOADED: "<?php echo Conf::get('media_upload_status')['already_uploaded']; ?>",
        FAILED: "<?php echo Conf::get('media_upload_status')['failed']; ?>"
    };

    var ITEMS_PER_PAGE = {
        MODAL_IMAGES: "<?php echo Conf::get('items_per_page')['admin_modal_images']; ?>"
    };

    var NC_GALLERY_LABEL = "<?php echo Conf::get('nc_gallery_label'); ?>";

    if(NC_LANGUAGE == undefined || NC_LANGUAGE == null) {
        var NC_LANGUAGE = "<?php echo Conf::get('language_default')['alias']; ?>";
    }

    var SHOP_CATEGORY_ID = "<?php echo Conf::get('shop_category_id'); ?>";

    Object.freeze(MENU_ITEM_TYPE_ID);
    Object.freeze(MODAL_TYPE);
    Object.freeze(MEDIA_UPLOAD_STATUS);
    Object.freeze(ITEMS_PER_PAGE);
    Object.freeze(NC_GALLERY_LABEL);

</script>