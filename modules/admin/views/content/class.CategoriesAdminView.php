<?php


class CategoriesAdminView extends AdminView {

   public $activeNavigation;
   private $resource;
   private $items;
   private $langGroupId;
   private $languages;
   private $shopPage = false;

   public function __construct($controller) {

      parent::__construct($controller);

      $this->activeNavigation = "categories";
      $this->resource = "category";
   }


   /********************************** TABLE PAGE **********************************/


   public function init($params) {

      if(!is_array($params)) $params = (array) $params;

      if(@exists($params['items'])) 	    $this->items 		  = $params['items'];
      if(@exists($params['langGroupId'])) $this->langGroupId = $params['langGroupId'];
      if(@exists($params['languages']))   $this->languages   = $params['languages'];

      if(@exists($params['shopPage']) && (bool)$params['shopPage'] === true) {
         $this->shopPage = $params['shopPage'];
      }

      $this->activeNavigation = $this->shopPage ? "shop_categories" : "categories";
   }


   public function displayHiddenFields() {
      $pageType = $this->shopPage ? Conf::get('shop_category_id') : "";
      echo '<input type="hidden" id="pageType" value="' . $pageType . '" />';
   }


   public function displayTable() {

      $url = $this->setItemLink();

      $this->renderAddNewItemLinkButton(array("url" => $url));
      $this->renderTable();
   }


   public function renderTable() {

      echo '<div class="table-wrapper">';

         echo '<table class="table table-dynamic table-tools filter-select">';

            echo '<thead>';
               echo '<tr>';
                  echo '<th>' . Trans::get("Id") . '</th>';
                  echo '<th>' . Trans::get("Title") . '</th>';
                  echo '<th>' . Trans::get("Parent category") . '</th>';
                  echo '<th>' . Trans::get("Language") . '</th>';
                  echo '<th>' . Trans::get("Visibility") . '</th>';
                  echo '<th class="text-right ">' . Trans::get("Actions") . '</th>';
               echo '</tr>';
            echo '</thead>';

            echo '<tbody>';

            if(@exists($this->items)) {

               foreach($this->items as $item) {
                  echo '<tr data-id="' . $item->id . '">';
                     $this->displayCellRaw(array("name" => "id", "type" => "int"), $item);
                     $this->displayCellRaw(array("name" => "name", "type" => "varchar"), $item);
                     $this->displayCellRaw(array("name" => "parent_name", "type" => "varchar"), $item);
                     $this->displayCellRaw(array("name" => "language_name", "type" => "varchar"), $item);
                     $this->renderPublishStatusForTable($item);
                     echo '<td class="text-right">';
                        echo '<a class="btn btn-sm btn-default edit" data-id="' . $item->id . '" href="' . Conf::get('url') . '/admin/categories/' . $item->id . '/insert' . '"><i class="icon-note"></i></a>';
                        echo '<button class="btn btn-sm btn-danger delete" data-id="' . $item->id . '"><i class="icons-office-52"></i></button>';
                     echo '</td>';
                  echo '</tr>';
               }
            }
            echo '</tbody>';

            echo '<tfoot>';
               echo '<tr>';
                  echo '<th style="visibility: hidden;">' . Trans::get("Id") . '</th>';
                  echo '<th style="visibility: hidden">' . Trans::get("Title") . '</th>';
                  echo '<th>' . Trans::get("Parent category") . '</th>';
                  echo '<th>' . Trans::get("Language") . '</th>';
                  echo '<th>' . Trans::get("Visibility") . '</th>';
                  echo '<th style="visibility: hidden;"></th>';
               echo '</tr>';
            echo '</tfoot>';

         echo '</table>';

      echo '</div>';
   }


   public function setItemLink($id = null) {

      if(!isset($id)) $id = 0;

      $url = Conf::get('url') . "/admin/categories";

      if($this->shopPage) $url .= "/shop";

      $url .= "/" . $id . "/insert";

      return $url;
   }


   /********************************* INSERT PAGE *********************************/


   public function displayInsertPageContent() {

      $rootId = @exists($this->shopPage) && $this->shopPage == true ? Conf::get('shop_category_id') : 0;

      $this->displayLanguageTabs($this->languages);

      echo '<div class="row">';

         echo '<input type="hidden" id="field-resource" value="' . $this->resource . '" />';

         foreach($this->items as $langId => $item) {

            $activeClass = (int)$langId === (int)Trans::getLanguageId() ? ' active' : '';

            $langSuffix = $this->getLanguageSuffix(array("langId" => $langId));

            echo '<div class="language-wrapper' . $activeClass . '" id="languageWrapper' . $langSuffix . '">';

               echo '<div class="col-md-9">';
                  $this->renderForm($item);
               echo '</div>';

               echo '<div class="col-md-3 side-panels">';
                  $this->displayInsertPageActions(array("item" => $item, "displayPublishCheckbox" => true, "displayPreviewButton" => true, "displayLanguageNotice" => true));
                  $this->displayStatusPanel(array("item" => $item));
                  $this->renderImagePanel(array("item" =>  $item, "langId" => $langId, "type" => Conf::get('modal_type')['intro_image']));
                  $this->renderImagePanel(array("item" =>  $item, "langId" => $langId, "type" => Conf::get('modal_type')['image']));
                  $this->displayInsertPageTree(array("langId" => $langId, "rootId" => $rootId));
               echo '</div>';

            echo '</div>';
         }

      echo '</div>';
   }


   private function renderForm($item) {

      $id  	 	         = @exists($item->id)                ? $item->id : 0;
      $parentId         = @exists($item->parent_id)         ? $item->parent_id : 0;
      $content 	      = @exists($item->content)           ? $item->content : "";
      $introText        = @exists($item->intro_text)        ? $item->intro_text : "";
      $published 	      = @exists($item->published)         ? $item->published : 0;
      $rang 		      = @exists($item->rang)              ? $item->rang : 9999;
      $introImage       = @exists($item->intro_image)       ? $item->intro_image : "";
      $image            = @exists($item->image)             ? $item->image : "";
      $galleryJson      = @exists($item->gallery_json)      ? $item->gallery_json : "";
      $name 		      = @exists($item->name)              ? htmlentities($item->name) : "";
      $subtitle 	      = @exists($item->subtitle)          ? htmlentities($item->subtitle) : "";
      $alias 		      = @exists($item->alias)             ? htmlentities($item->alias) : "";
      $metaTitle        = @exists($item->meta_title)        ? htmlentities($item->meta_title) : "";
      $metaDescription  = @exists($item->meta_description)  ? htmlentities($item->meta_description) : "";
      $metaKeywords     = @exists($item->meta_keywords)     ? htmlentities($item->meta_keywords) : "";
      $langId           = $item->lang_id;

      $langSuffix = $this->getLanguageSuffix(array("langId" => $langId));

      echo '<form id="insert-form' . $langSuffix . '">';

         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "id", "value" => $id));
         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "parent_id", "value" => $parentId, "className" => "parentId-field$langSuffix"));
         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "published", "value" => $published, "className" => "published"));
         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "rang", "value" => $rang, "className" => "rang"));
         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "intro_image", "value" => $introImage, "className" => "intro-image-field$langSuffix"));
         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "image", "value" => $image, "className" => "image-field$langSuffix"));
         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "lang_id", "value" => $langId));
         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "lang_group_id", "value" => $this->langGroupId));
         echo "<input type='hidden' name='gallery_json' id='".$this->resource."-gallery_json-" . $langId . "' value='" . $galleryJson . "' class='gallery-json-field" . $langSuffix . "' />";
         $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "name", "value" => $name, "label" => Trans::get("Name")));
         $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "subtitle", "value" => $subtitle, "label" => Trans::get("Subtitle")));
         $this->displayFormTextarea(array("name" => "intro_text", "resource" => $this->resource, "langId" => $langId, "value" => $introText, "label" => Trans::get("Intro text"), "rows" => 4, "info" => Trans::get("Short description when articles are listed, not on the article page itself")));
         $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "alias", "value" => $alias, "label" => Trans::get("Alias"), "disabled" => true));
         $this->displayFormTextEditor(array("name" => "content", "resource" => $this->resource, "langId" => $langId, "value" => $content, "label" => Trans::get("Content")));
         $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "meta_title", "value" => $metaTitle, "label" => Trans::get("Meta Title"), "info" => Trans::get('For better browser ranking (not visible on page)')));
         echo '<div class="row">';
            echo '<div class="col-md-6">';
               $this->displayFormTextarea(array("name" => "meta_description", "resource" => $this->resource, "langId" => $langId, "value" => $metaDescription, "label" => Trans::get("Meta Description"), "rows" => 4, "info" => Trans::get('For better browser ranking (not visible on page)')));
            echo '</div>';
            echo '<div class="col-md-6">';
               $this->displayFormTextarea(array("name" => "meta_keywords", "resource" => $this->resource, "langId" => $langId, "value" => $metaKeywords, "label" => Trans::get("Meta Keywords"), "rows" => 4, "info" => Trans::get('For better browser ranking (not visible on page)')));
            echo '</div>';
         echo '</div>';

         // $this->renderFormGallery($item, $langId);

      echo '</form>';
   }
}
?>