<?php


class GalleriesAdminView extends AdminView {

   public $activeNavigation;
   private $resource;
   private $items;
   private $item;

   public function __construct($controller) {

      parent::__construct($controller);

      $this->activeNavigation = "galleries";
      $this->resource = "gallery";
   }

   /********************************** TABLE PAGE **********************************/


   public function init($params) {

      if(!is_array($params)) $params = (array) $params;

      if(@exists($params['items'])) $this->items = $params['items'];
      if(@exists($params['item']))  $this->item  = $params['item'];
   }


   public function displayTable() {

      $this->renderAddNewItemLinkButton(array("url" => Conf::get('url') . "/admin/galleries/0/insert"));
      $this->renderTable();
   }


   private function renderTable() {

      echo '<div class="table-wrapper">';

         echo '<table class="table table-dynamic table-tools filter-select">';

            echo '<thead>';
               echo '<tr>';
                  echo '<th>' . Trans::get("Id") . '</th>';
                  echo '<th>' . Trans::get("Name") . '</th>';
                  echo '<th>' . Trans::get("Image") . '</th>';
                  echo '<th class="text-right">' . Trans::get("Actions") . '</th>';
               echo '</tr>';
            echo '</thead>';

            echo '<tbody>';

               if(@exists($this->items)) {

               foreach($this->items as $item) {
                  echo '<tr data-id="' . $item->id . '">';
                     $this->displayCellRaw(array("name" => "id", "type" => "int"), $item);
                     $this->displayCellRaw(array("name" => "name", "type" => "varchar"), $item);
                     $this->displayFeaturedImageFromGalleryJson($item);
                     echo '<td class="text-right">';
                        echo '<a class="btn btn-sm btn-default edit" data-id="' . $item->id . '" href="' . Conf::get('url') . '/admin/galleries/' . $item->id . '/insert"><i class="icon-note"></i></a>';
                        echo '<button type="button" class="btn btn-sm btn-danger delete" data-id="' . $item->id . '"><i class="icons-office-52"></i></button>';
                     echo '</td>';
                  echo '</tr>';
               }
            }
            echo '</tbody>';

            echo '<tfoot>';
               echo '<tr>';
                  echo '<th style="visibility: hidden;">' . Trans::get("Id") . '</th>';
                  echo '<th>' . Trans::get("Name") . '</th>';
                  echo '<th>' . Trans::get("Image") . '</th>';
                  echo '<th style="visibility: hidden;"></th>';
               echo '</tr>';
            echo '</tfoot>';

         echo '</table>';

      echo '</div>';
   }


   /*************************** GALLERY INSERT PAGE ***************************/


   public function displayInsertPageContent() {

      echo '<div class="row">';

         echo '<div class="col-md-8">';
            $this->renderForm();
         echo '</div>';

         echo '<div class="col-md-4">';
            $this->displayInsertPageActions(array("item" => $this->item));
            $this->displayStatusPanel(array("item" => $this->item));
         echo '</div>';

      echo '</div>';
   }


   private function renderForm() {

      $item = $this->item;

      $id          = @exists($item->id)           ? $item->id : 0;
      $name 		 = @exists($item->name)         ? htmlentities($item->name) : "";
      $galleryJson = @exists($item->gallery_json) ? htmlentities($item->gallery_json) : "";

      $langId = Trans::getLanguageId();

      echo '<input type="hidden" id="field-resource" value="' . $this->resource . '" />';

      $langSuffix = $this->getLanguageSuffix();

      echo '<form id="insert-form' . $langSuffix . '">';

         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "id", "value" => $id));
         echo '<input type="hidden" name="gallery_json" id="'.$this->resource.'-gallery_json' . $langSuffix . '" value="' . $galleryJson . '" class="gallery-json-field' . $langSuffix . '" />';
         $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "name", "value" => $name, "label" => Trans::get("Name")));
         $this->renderFormGallery($item, $langId);

      echo '</form>';
   }
}
?>