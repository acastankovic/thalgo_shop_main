<?php


class VendorsAdminView extends AdminView {

   public $activeNavigation;
   private $resource;
   private $items;
   private $langGroupId;
   private $languages;

   public function __construct($controller) {
      parent::__construct($controller);

      $this->activeNavigation = "vendors";
      $this->resource = "vendor";
   }


   /********************************** TABLE PAGE **********************************/


   public function init($params) {

      if(!is_array($params)) $params = (array) $params;

      if(@exists($params['items'])) 	   $this->items 	    = $params['items'];
      if(@exists($params['langGroupId'])) $this->langGroupId = $params['langGroupId'];
      if(@exists($params['languages']))   $this->languages   = $params['languages'];
   }


   public function displayTable() {

      $this->renderAddNewItemLinkButton(array("url" => Conf::get('url') . "/admin/vendors/0/insert"));
      $this->renderTable();
   }


   public function renderTable() {

      echo '<div class="table-wrapper">';

         echo '<table class="table table-dynamic table-tools filter-select">';

            echo '<thead>';
               echo '<tr>';
                  echo '<th>' . Trans::get("Id") . '</th>';
                  echo '<th>' . Trans::get("Name") . '</th>';
                  echo '<th>' . Trans::get("Code") . '</th>';
                  echo '<th>' . Trans::get("Link") . '</th>';
                  echo '<th>' . Trans::get("Language") . '</th>';
                  echo '<th>' . Trans::get("Visibility") . '</th>';
                  echo '<th class="text-right">' . Trans::get("Actions") . '</th>';
               echo '</tr>';
            echo '</thead>';

            echo '<tbody>';

               if(@exists($this->items)) {

               foreach ($this->items as $item) {
                     echo '<tr data-id="' . $item->id . '">';
                        $this->displayCellRaw(array("name" => "id", "type" => "int"), $item);
                        $this->displayCellRaw(array("name" => "name", "type" => "varchar"), $item);
                        $this->displayCellRaw(array("name" => "code", "type" => "varchar"), $item);
                        $this->displayCellRaw(array("name" => "link", "type" => "varchar"), $item);
                        $this->displayCellRaw(array("name" => "language_name", "type" => "varchar"), $item);
                        $this->renderPublishStatusForTable($item);
                        echo '<td class="text-right">';
                           echo '<a class="btn btn-sm btn-default edit" data-id="' . $item->id . '" href="' . Conf::get('url') . '/admin/vendors/' . $item->id . '/insert"><i class="icon-note"></i></a>';
                           echo '<button type="button" class="btn btn-sm btn-danger delete" data-id="' . $item->id . '"><i class="icons-office-52"></i></button>';
                        echo '</td>';
                     echo '</tr>';
                  }
               }
            echo '</tbody>';

            echo '<tfoot>';
               echo '<tr>';
                  echo '<th style="visibility: hidden;">' . Trans::get("Id") . '</th>';
                  echo '<th>' . Trans::get("Name") . '</th>';
                  echo '<th>' . Trans::get("Code") . '</th>';
                  echo '<th style="visibility: hidden;">' . Trans::get("Link") . '</th>';
                  echo '<th>' . Trans::get("Language") . '</th>';
                  echo '<th style="visibility: hidden;">' . Trans::get("Visibility") . '</th>';
                  echo '<th style="visibility: hidden;"></th>';
               echo '</tr>';
            echo '</tfoot>';

         echo '</table>';

      echo '</div>';
   }


   /********************************* INSERT PAGE *********************************/


   public function displayInsertPageContent() {

      $this->displayLanguageTabs($this->languages);

      echo '<div class="row">';

         echo '<input type="hidden" id="field-resource" value="' . $this->resource . '" />';

         foreach($this->items as $langId => $item) {

            $activeClass = (int)$langId === (int)Trans::getLanguageId() ? ' active' : '';

            $langSuffix = $this->getLanguageSuffix(array("langId" => $langId));

            echo '<div class="language-wrapper' . $activeClass . '" id="languageWrapper' . $langSuffix . '">';

               echo '<div class="col-md-9">';
                  $this->renderForm($item);
               echo '</div>';

            echo '<div class="col-md-3 side-panels">';
               $this->displayInsertPageActions(array("item" => $item, "displayPublishCheckbox" => true, "displayPreviewButton" => true, "displayLanguageNotice" => true));
               $this->displayStatusPanel(array("item" => $item));
               $this->renderImagePanel(array("item" =>  $item, "langId" => $langId, "type" => Conf::get('modal_type')['intro_image']));
               $this->renderImagePanel(array("item" =>  $item, "langId" => $langId, "type" => Conf::get('modal_type')['image']));
            echo '</div>';

            echo '</div>';
         }

      echo '</div>';
   }


   public function renderForm($item) {

      $id  	            = @exists($item->id) 		         ? $item->id : 0;
      $published        = @exists($item->published)         ? $item->published : 1;
      $rang 	         = @exists($item->rang) 	            ? $item->rang : 9999;
      $content          = @exists($item->content) 	         ? $item->content : "";
      $introText        = @exists($item->intro_text)        ? $item->intro_text : "";
      $introImage       = @exists($item->intro_image)       ? $item->intro_image : "";
      $image            = @exists($item->image)             ? $item->image : "";
      $name 	         = @exists($item->name)              ? htmlentities($item->name) : "";
      $code 	         = @exists($item->code)              ? htmlentities($item->code) : "";
      $alias 	         = @exists($item->alias)             ? htmlentities($item->alias) : "";
      $link             = @exists($item->link)              ? htmlentities($item->link) : "";
      $metaTitle        = @exists($item->meta_title)        ? htmlentities($item->meta_title) : "";
      $metaDescription  = @exists($item->meta_description)  ? htmlentities($item->meta_description) : "";
      $metaKeywords     = @exists($item->meta_keywords)     ? htmlentities($item->meta_keywords) : "";
      $langId     = $item->lang_id;

      $langSuffix = $this->getLanguageSuffix(array("langId" => $langId));

      echo '<form id="insert-form' . $langSuffix . '">';

         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "id", "value" => $id));
         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "published", "value" => $published, "className" => "published"));
         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "rang", "value" => $rang, "className" => "rang"));
         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "intro_image", "value" => $introImage, "className" => "intro-image-field$langSuffix"));
         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "image", "value" => $image, "className" => "image-field$langSuffix"));
         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "lang_id", "value" => $langId));
         $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "lang_group_id", "value" => $this->langGroupId));
         $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "name", "value" => $name, "label" => Trans::get("Name")));
         $this->displayFormTextarea(array("name" => "intro_text", "resource" => $this->resource, "langId" => $langId, "value" => $introText, "label" => Trans::get("Intro text"), "rows" => 4, "info" => Trans::get("Short description when articles are listed, not on the article page itself")));
         $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "alias", "value" => $alias, "label" => Trans::get("Alias"), "disabled" => true));
         $this->displayFormTextEditor(array("name" => "content", "resource" => $this->resource, "langId" => $langId, "value" => $content, "label" => Trans::get("Content")));
         echo '<div class="row">';
            echo '<div class="col-md-6">';
               $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "code", "value" => $code, "label" => Trans::get("Code")));
            echo '</div>';
            echo '<div class="col-md-6">';
               $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "link", "value" => $link, "label" => Trans::get("Link")));
            echo '</div>';
         echo '</div>';
         $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "meta_title", "value" => $metaTitle, "label" => Trans::get("Meta Title"), "info" => Trans::get('For better browser ranking (not visible on page)')));
         echo '<div class="row">';
            echo '<div class="col-md-6">';
               $this->displayFormTextarea(array("name" => "meta_description", "resource" => $this->resource, "langId" => $langId, "value" => $metaDescription, "label" => Trans::get("Meta Description"), "rows" => 4, "info" => Trans::get('For better browser ranking (not visible on page)')));
            echo '</div>';
            echo '<div class="col-md-6">';
               $this->displayFormTextarea(array("name" => "meta_keywords", "resource" => $this->resource, "langId" => $langId, "value" => $metaKeywords, "label" => Trans::get("Meta Keywords"), "rows" => 4, "info" => Trans::get('For better browser ranking (not visible on page)')));
            echo '</div>';
         echo '</div>';

      echo '</form>';
   }
}
?>