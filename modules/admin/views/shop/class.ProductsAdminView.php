<?php


class ProductsAdminView extends AdminView {

    public $activeNavigation;
    public $pageTitle;
    private $parentId;
    private $resource;
    private $mediaCategoryId;
    private $items;
    private $langGroupId;
    private $languages;
    private $vendors;

    private $shopRootCategories;

    public function __construct($controller) {

        parent::__construct($controller);

        $this->activeNavigation = "products";
        $this->resource = "product";
        $this->mediaCategoryId = Conf::get('media_category_id')['products'];
    }


    /********************************** TABLE PAGE **********************************/


    public function init($params) {

        if(!is_array($params)) $params = (array) $params;

        if(@exists($params['items'])) 	   $this->items 	    = $params['items'];
        if(@exists($params['langGroupId'])) $this->langGroupId = $params['langGroupId'];
        if(@exists($params['languages']))   $this->languages   = $params['languages'];
        if(@exists($params['parentId']))    $this->parentId    = $params['parentId'];
        if(@exists($params['pageTitle']))   $this->pageTitle   = $params['pageTitle'];
        if(@exists($params['vendors'])) 	   $this->vendors 	 = $params['vendors'];

        if(@exists($params['shopRootCategories']))  $this->shopRootCategories  = $params['shopRootCategories'];
    }


    public function displayTable() {

        $this->renderAddNewItemLinkButton(array("url" => Conf::get('url') . "/admin/products/0/insert"));
        $this->renderTable();
    }


    public function renderTable() {

        echo '<div class="table-wrapper">';
        echo '<table class="table table-dynamic table-tools filter-select">';
        echo '<thead>';
        echo '<tr>';
        echo '<th>' . Trans::get("Id") . '</th>';
        echo '<th>' . Trans::get("Title") . '</th>';
        echo '<th>' . Trans::get("Parent category") . '</th>';
        echo '<th>' . Trans::get("Code") . '</th>';
        echo '<th>' . Trans::get("Price") . '</th>';
        echo '<th>' . Trans::get("Discount") . '</th>';
        echo '<th>' . Trans::get("Language") . '</th>';
        echo '<th>' . Trans::get("Visibility") . '</th>';
        echo '<th class="text-right">' . Trans::get("Actions") . '</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        if(@exists($this->items)) {

            foreach ($this->items as $item) {

                echo '<tr data-id="' . $item->id . '">';
                $this->displayCellRaw(array("name" => "id", "type" => "int"), $item);
                $this->displayCellRaw(array("name" => "title", "type" => "varchar"), $item);
                $this->displayCellRaw(array("name" => "parent_name", "type" => "varchar"), $item);
                $this->displayCellRaw(array("name" => "code", "type" => "varchar"), $item);
                $this->displayCellRaw(array("name" => "price", "type" => "int"), $item);
                $this->renderDiscountValueForTable($item);
                $this->displayCellRaw(array("name" => "language_name", "type" => "varchar"), $item);
                $this->renderPublishStatusForTable($item);
                echo '<td class="text-right">';
                echo '<a class="btn btn-sm btn-default edit" data-id="' . $item->id . '" href="' . Conf::get('url') . '/admin/products/' . $item->id . '/insert"><i class="icon-note"></i></a>';
                echo '<button type="button" class="btn btn-sm btn-danger delete" data-id="' . $item->id . '"><i class="icons-office-52"></i></button>';
                echo '</td>';
                echo '</tr>';
            }
        }
        echo '</tbody>';
        echo '<tfoot>';
        echo '<tr>';
        echo '<th style="visibility: hidden;">' . Trans::get("Id") . '</th>';
        echo '<th>' . Trans::get("Title") . '</th>';
        echo '<th>' . Trans::get("Parent category") . '</th>';
        echo '<th>' . Trans::get("Code") . '</th>';
        echo '<th style="visibility: hidden;">' . Trans::get("Price") . '</th>';
        echo '<th>' . Trans::get("Discount") . '</th>';
        echo '<th>' . Trans::get("Language") . '</th>';
        echo '<th style="visibility: hidden;">' . Trans::get("Visibility") . '</th>';
        echo '<th style="visibility: hidden;"></th>';
        echo '</tr>';
        echo '</tfoot>';
        echo '</table>';
        echo '</div>';
    }


    public function renderDiscountValueForTable($item) {

        $value = $item->discount_value;
        if((int)$item->discount_type === (int)Conf::get('discount_type')['percent']) {
            $value .= '%';
        }

        echo '<td data-name="discount_value" data-type="varchar" data-value="' . $value . '">' . $value . '</td>';
    }

    /********************************* INSERT PAGE *********************************/


    public function displayInsertPageContent() {

        $this->displayLanguageTabs($this->languages);

        echo '<div class="row">';

        echo '<input type="hidden" id="field-resource" value="' . $this->resource . '" />';

        foreach($this->items as $langId => $item) {
            Kint::dump($item);
            $activeClass = (int)$langId === (int)Trans::getLanguageId() ? ' active' : '';

            $langSuffix = $this->getLanguageSuffix(array("langId" => $langId));

            echo '<div class="language-wrapper' . $activeClass . '" id="languageWrapper' . $langSuffix . '">';

            echo '<div class="col-md-3 side-panels">';
            $this->displayInsertPageActions(array("item" => $item, "displayPublishCheckbox" => true, "displayPreviewButton" => true, "displayLanguageNotice" => true, "displayAllowCommentsCheckbox" => true));
            $this->displayInsertPageTree(array("langId" => $langId, "rootId" => Conf::get('shop_category_id')));


            if (($item->category_id) === (string)Conf::get('lice_category_id')) {
                echo '<div class="filter-section">';
                echo '<h3>Filteri</h3>';
                echo '<p>LICE</p>';
                echo '<p>By Range</p>';
                echo '<div class="filter-wrapper">';
                $this->renderEveilAlaMerCheckbox($item);
                $this->renderSourceMarineCheckbox($item);
                $this->renderBbKremeCheckbox($item);
                $this->renderColdCreamMarineCheckbox($item);
                $this->renderPureteMarineCheckbox($item);
                $this->renderShotMaskeCheckbox($item);
                $this->renderPoklonSetoviCheckbox($item);
                echo '</div>';
                echo '<p>By action</p>';
                echo '<div class="filter-wrapper">';
                $this->renderCistaciPeneCheckbox($item);
                $this->renderMaskeIpilinziCheckbox($item);
                $this->renderMistoviIlosioniCheckbox($item);
                $this->renderSerumiIkoncentratiCheckbox($item);
                $this->renderOcnaRegijaCheckbox($item);
                $this->renderDnevnaNegaCheckbox($item);
                $this->renderNocnaNegaCheckbox($item);
                $this->renderBbKremeNamenaCheckbox($item);
                echo '</div>';
                echo '</div>';
            }else if (($item->category_id) === (string)Conf::get('anti_age_category_id')){
                echo '<div class="filter-section">';
                echo '<h3>Filteri</h3>';
                echo '<p>LICE</p>';
                echo '<p>By Range</p>';
                echo '<div class="filter-wrapper">';
                $this->renderSpirulinaBoostAntiAgeCheckbox($item);
                $this->renderHyaluProCollagenAntiAgeCheckbox($item);
                $this->renderSilicijumAntiAgeCheckbox($item);
                $this->renderExceptionAntiAgeCheckbox($item);
                $this->renderLumiereAntiAgeCheckbox($item);
                $this->renderProdigeDesOceansAntiAgeCheckbox($item);
                $this->renderShotMaskeAntiAgeCheckbox($item);
                $this->renderPoklonSetoviAntiAgeCheckbox($item);
                echo '</div>';
                echo '<p>By action</p>';
                echo '<div class="filter-wrapper">';
                $this->renderLosioniAntiAgeCheckbox($item);
                $this->renderMaskeiPilinziAntiAgeCheckbox($item);
                $this->renderSerumiAntiAgeCheckbox($item);
                $this->renderOcnaRegijaAntiAgeCheckbox($item);
                $this->renderDnevnaNegaAntiAgeCheckbox($item);
                $this->renderNocnaNegaAntiAgeCheckbox($item);
                $this->renderPuderiAntiAgeCheckbox($item);
                echo '</div>';
                echo '</div>';
            }else if (($item->category_id) === (string)Conf::get('body_category_id')){
                echo '<div class="filter-section">';
                echo '<h3>Filteri</h3>';
                echo '<p>TELO</p>';
                echo '<p>By Range</p>';
                echo '<div class="filter-wrapper">';
                $this->renderMorskiProgramBodyCheckbox($item);
                $this->renderColdCreamMarineBodyCheckbox($item);
                $this->renderMrsavljenjeBodyCheckbox($item);
                $this->renderSpaRitualiPacifikBodyCheckbox($item);
                $this->renderSpaRitualiIndoceaneBodyCheckbox($item);
                $this->renderSpaRitualiAtlantikBodyCheckbox($item);
                $this->renderSpaRitualiArktikBodyCheckbox($item);
                $this->renderPromoBodyCheckbox($item);
                $this->renderPoklonSetoviBodyCheckbox($item);
                echo '</div>';

                echo '</div>';
            }else if (($item->category_id) === (string)Conf::get('for_man_category_id')){
                echo '<div class="filter-section">';
                echo '<h3>Filteri</h3>';
                echo '<p>ZA MUSKARCE</p>';
                echo '<p>By action</p>';
                echo '<div class="filter-wrapper">';
                $this->renderBrijanjeManCheckbox($item);
                $this->renderNegaManCheckbox($item);
                $this->renderPoklonSetoviManCheckbox($item);
                echo '</div>';

                echo '</div>';
            }else if (($item->category_id) === (string)Conf::get('sun_protect_category_id')){
                echo '<div class="filter-section">';
                echo '<h3>Filteri</h3>';
                echo '<p>ZA MUSKARCE</p>';
                echo '<p>By action</p>';
                echo '<div class="filter-wrapper">';
                $this->renderLiceSunCheckbox($item);
                $this->renderTeloSunCheckbox($item);
                $this->renderPosleSuncanjaSunCheckbox($item);
                $this->renderSamopotamnjivanjeSunCheckbox($item);
                echo '</div>';

                echo '</div>';
            }else if (($item->category_id) === (string)Conf::get('suplement_category_id')){
                echo '<div class="filter-section">';
                echo '<h3>Filteri</h3>';
                echo '<p>SUPLEMENTI</p>';
                echo '<p>By action</p>';
                echo '<div class="filter-wrapper">';
                $this->renderMrsavljenjeSuplementCheckbox($item);
                $this->renderSuncanjeSuplementCheckbox($item);
                $this->renderAntiAgeSuplementCheckbox($item);
                $this->renderBlagostanjeSuplementCheckbox($item);
                $this->renderOrganskaInfuzijaSuplementCheckbox($item);
                echo '</div>';

                echo '</div>';
            }else {
                null;
            }

//                  $this->displayFilterPageActions(array("item" => $item, "displayEveilAlaMerCheckbox" => true,  "displayPreviewButton" => true, "displayLanguageNotice" => true, "displayAllowCommentsCheckbox" => true));
            $this->displayStatusPanel(array("item" => $item));
            $this->renderImagePanel(array("item" =>  $item, "langId" => $langId, "type" => Conf::get('modal_type')['intro_image']));
            $this->renderImagePanel(array("item" =>  $item, "langId" => $langId, "type" => Conf::get('modal_type')['image']));

            echo '</div>';

            echo '<div class="col-md-9">';
            $this->renderForm($item);
            if(@exists($item->comments)) {
                $this->renderCommentsSection($item->comments);
            }
            echo '</div>';



            echo '</div>';
        }

        echo '</div>';
    }


    public function renderForm($item) {
//    Kint::dump($item);
        $id  	 	         = @exists($item->id) 		          ? $item->id : 0;
        //LICE
        //by range

        $eveil_a_la_mer 	      = @exists($item->eveil_a_la_mer)          ? $item->eveil_a_la_mer  : 0;
        $source_marine         = @exists($item->source_marine)           ? $item->source_marine  : 0;
        $bb_kreme              = @exists($item->bb_kreme)                ? $item->bb_kreme : 0;
        $cold_cream_marine     = @exists($item->cold_cream_marine)       ? $item->cold_cream_marine : 0;
        $purete_marine         = @exists($item->purete_marine)           ? $item->purete_marine : 0;
        $shot_maske            = @exists($item->shot_maske)              ? $item->shot_maske : 0;
        $poklon_setovi         = @exists($item->poklon_setovi)           ? $item->poklon_setovi : 0;
        // by action

        $cistaci_pene          = @exists($item->cistaci_pene)            ? $item->cistaci_pene : 0;
        $mistovi_I_losioni = @exists($item->mistovi_I_losioni) ? $item->mistovi_I_losioni  : 0;
        $maske_i_pilinzi       = @exists($item->maske_i_pilinzi)         ? $item->maske_i_pilinzi : 0;
        $serumi_i_koncentrati = @exists($item->serumi_i_koncentrati) ? $item->serumi_i_koncentrati : 0;
        $ocna_regija = @exists($item->ocna_regija) ? $item->ocna_regija : 0;
        $dnevna_nega = @exists($item->dnevna_nega) ? $item->dnevna_nega : 0;
        $nocna_nega = @exists($item->nocna_nega) ? $item->nocna_nega : 0;
        $bb_kreme_namena = @exists($item->bb_kreme_namena) ? $item->bb_kreme_namena : 0;

        //ANTI AGE
        //by range
        $spirulina_boost_anti_age 	      = @exists($item->spirulina_boost_anti_age)          ? $item->spirulina_boost_anti_age  : 0;
        $hyalu_pro_collagen_anti_age         = @exists($item->hyalu_pro_collagen_anti_age)           ? $item->hyalu_pro_collagen_anti_age  : 0;
        $silicijum_anti_age              = @exists($item->silicijum_anti_age)                ? $item->silicijum_anti_age : 0;
        $exception_anti_age     = @exists($item->exception_anti_age)       ? $item->exception_anti_age : 0;
        $lumiere_anti_age         = @exists($item->lumiere_anti_age)           ? $item->lumiere_anti_age : 0;
        $prodige_des_oceans_anti_age            = @exists($item->prodige_des_oceans_anti_age)              ? $item->prodige_des_oceans_anti_age : 0;
        $shot_maske_anti_age         = @exists($item->shot_maske_anti_age)           ? $item->shot_maske_anti_age : 0;
        $poklon_setovi_anti_age          = @exists($item->poklon_setovi_anti_age)            ? $item->poklon_setovi_anti_age : 0;
        // by action
        $losioni_anti_age = @exists($item->losioni_anti_age) ? $item->losioni_anti_age  : 0;
        $maske_i_pilinzi_anti_age       = @exists($item->maske_i_pilinzi_anti_age)         ? $item->maske_i_pilinzi_anti_age : 0;
        $serumi_anti_age = @exists($item->serumi_anti_age) ? $item->serumi_anti_age : 0;
        $ocna_regija_anti_age = @exists($item->ocna_regija_anti_age) ? $item->ocna_regija_anti_age : 0;
        $dnevna_nega_anti_age = @exists($item->dnevna_nega_anti_age) ? $item->dnevna_nega_anti_age : 0;
        $nocna_nega_anti_age = @exists($item->nocna_nega_anti_age) ? $item->nocna_nega_anti_age : 0;
        $puderi_anti_age = @exists($item->puderi_anti_age) ? $item->puderi_anti_age : 0;

        //telo
        //by range

        $morski_program_body 	      = @exists($item->morski_program_body)          ? $item->morski_program_body  : 0;
        $cold_cream_marine_body         = @exists($item->cold_cream_marine_body)           ? $item->cold_cream_marine_body  : 0;
        $mrsavljenje_body              = @exists($item->mrsavljenje_body)                ? $item->mrsavljenje_body : 0;
        $spa_rituali_pacifik_body     = @exists($item->spa_rituali_pacifik_body)       ? $item->spa_rituali_pacifik_body : 0;
        $spa_rituali_indoceane_body         = @exists($item->spa_rituali_indoceane_body)           ? $item->spa_rituali_indoceane_body : 0;
        $spa_rituali_atlantik_body            = @exists($item->spa_rituali_atlantik_body)              ? $item->spa_rituali_atlantik_body : 0;
        $spa_rituali_arktik_body         = @exists($item->spa_rituali_arktik_body)           ? $item->spa_rituali_arktik_body : 0;
        $promo_body          = @exists($item->promo_body)            ? $item->promo_body : 0;
        $poklon_setovi_body          = @exists($item->poklon_setovi_body)            ? $item->poklon_setovi_body : 0;

        //MAN
        //by action

        $brijanje_man         = @exists($item->brijanje_man)           ? $item->brijanje_man : 0;
        $nega_man          = @exists($item->nega_man)            ? $item->nega_man : 0;
        $poklon_setovi_man          = @exists($item->poklon_setovi_man)            ? $item->poklon_setovi_man : 0;

        //SUN
        //by action

        $lice_sun         = @exists($item->lice_sun)           ? $item->lice_sun : 0;
        $telo_sun          = @exists($item->telo_sun)            ? $item->telo_sun : 0;
        $posle_suncanja_sun          = @exists($item->posle_suncanja_sun)            ? $item->posle_suncanja_sun : 0;
        $samopotamnjivanje_sun          = @exists($item->samopotamnjivanje_sun)            ? $item->samopotamnjivanje_sun : 0;

        //SUPLEMENTI
        //by action

        $mrsavljenje_suplement         = @exists($item->mrsavljenje_suplement)           ? $item->mrsavljenje_suplement : 0;
        $suncanje_suplement          = @exists($item->suncanje_suplement)            ? $item->suncanje_suplement : 0;
        $anti_age_suplement          = @exists($item->anti_age_suplement)            ? $item->anti_age_suplement : 0;
        $blagostanje_suplement          = @exists($item->blagostanje_suplement)            ? $item->blagostanje_suplement : 0;
        $organska_infuzija_suplement          = @exists($item->organska_infuzija_suplement)            ? $item->organska_infuzija_suplement : 0;

        $opis_1 = @exists($item->opis_1)        ?           $item->opis_1 : '';
        $opis_2 = @exists($item->opis_2)        ?           $item->opis_2 : '';
        $opis_3 = @exists($item->opis_3)        ?           $item->opis_3 : '';
        $opis_4 = @exists($item->opis_4)        ?           $item->opis_4 : '';

        $categoryId       = @exists($item->category_id)        ? $item->category_id : 0;
        $published 	      = @exists($item->published)          ? $item->published : 0;
        $allowComments    = @exists($item->allow_comments)     ? $item->allow_comments : 0;
        $rang 		      = @exists($item->rang) 		          ? $item->rang : 9999;
        $content 	      = @exists($item->content) 	          ? $item->content : "";
        $introText        = @exists($item->intro_text)         ? $item->intro_text : "";
        $introImage       = @exists($item->intro_image)        ? $item->intro_image : "";
        $image            = @exists($item->image)              ? $item->image : "";
        $galleryJson      = @exists($item->gallery_json)       ? htmlentities($item->gallery_json) : "";
        $price    	      = @exists($item->price) 	          ? $item->price : 0;
        $products_weight    	      = @exists($item->products_weight) 	          ? $item->products_weight : 0;
        $discountValue    = @exists($item->discount_value)     ? $item->discount_value : 0;
        $title 		      = @exists($item->title)              ? htmlentities($item->title) : "";
        $subtitle 	      = @exists($item->subtitle)           ? htmlentities($item->subtitle) : "";
        $alias 		      = @exists($item->alias)              ? htmlentities($item->alias) : "";
        $metaTitle        = @exists($item->meta_title)         ? htmlentities($item->meta_title) : "";
        $metaDescription  = @exists($item->meta_description)   ? htmlentities($item->meta_description) : "";
        $metaKeywords     = @exists($item->meta_keywords)      ? htmlentities($item->meta_keywords) : "";
        $code 		      = @exists($item->code)               ? htmlentities($item->code) : "";
        $youtubeVideoLink = @exists($item->youtube_video_code) ? 'https://www.youtube.com/embed/' . $item->youtube_video_code : "";
        $langId 	         = $item->lang_id;

        $langSuffix = $this->getLanguageSuffix(array("langId" => $langId));

        echo '<form id="insert-form' . $langSuffix . '">';

        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "id", "value" => $id));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "category_id", "value" => $categoryId, "className" => "parentId-field$langSuffix"));
        //lice
        //by range
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "eveil_a_la_mer", "value" => $eveil_a_la_mer, "className" => "eveilAlaMer"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "source_marine", "value" => $source_marine, "className" => "sourceMarine"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "bb_kreme", "value" => $bb_kreme, "className" => "bbKreme"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "cold_cream_marine", "value" => $cold_cream_marine, "className" => "coldCreamMarine"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "shot_maske", "value" => $shot_maske, "className" => "shotMaske"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "poklon_setovi", "value" => $poklon_setovi, "className" => "poklonSetovi"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "purete_marine", "value" => $purete_marine, "className" => "pureteMarine"));

        // by action
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "mistovi_I_losioni", "value" => $mistovi_I_losioni, "className" => "mistoviIlosioni"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "maske_i_pilinzi", "value" => $maske_i_pilinzi, "className" => "maskeIpilinzi"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "cistaci_pene", "value" => $cistaci_pene, "className" => "cistaciPene"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "serumi_i_koncentrati", "value" => $serumi_i_koncentrati, "className" => "serumiIkoncentrati"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "ocna_regija", "value" => $ocna_regija, "className" => "ocnaRegija"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "dnevna_nega", "value" => $dnevna_nega, "className" => "	dnevnaNega"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "nocna_nega", "value" => $nocna_nega, "className" => "nocnaNega"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "bb_kreme_namena", "value" => $bb_kreme_namena, "className" => "	bbKremeNamena"));
        //anti age
        //by range
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "spirulina_boost_anti_age", "value" => $spirulina_boost_anti_age, "className" => "spirulinaBoostAntiAge"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "hyalu_pro_collagen_anti_age", "value" => $hyalu_pro_collagen_anti_age, "className" => "hyaluProCollagenAntiAge"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "silicijum_anti_age", "value" => $silicijum_anti_age, "className" => "silicijumAntiAge"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "exception_anti_age", "value" => $exception_anti_age, "className" => "exceptionAntiAge"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "lumiere_anti_age", "value" => $lumiere_anti_age, "className" => "lumiereAntiAge"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "prodige_des_oceans_anti_age", "value" => $prodige_des_oceans_anti_age, "className" => "prodigeDesOceansAntiAge"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "shot_maske_anti_age", "value" => $shot_maske_anti_age, "className" => "shotMaskeAntiAge"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "poklon_setovi_anti_age", "value" => $poklon_setovi_anti_age, "className" => "poklonSetoviAntiAge"));
        // by action

        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "losioni_anti_age", "value" => $losioni_anti_age, "className" => "losioniAntiAge"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "maske_i_pilinzi_anti_age", "value" => $maske_i_pilinzi_anti_age, "className" => "maskeiPilinziAntiAge"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "serumi_anti_age", "value" => $serumi_anti_age, "className" => "serumiAntiAge"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "ocna_regija_anti_age", "value" => $ocna_regija_anti_age, "className" => "ocnaRegijaAntiAge"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "dnevna_nega_anti_age", "value" => $dnevna_nega_anti_age, "className" => "dnevnaNegaAntiAge"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "nocna_nega_anti_age", "value" => $nocna_nega_anti_age, "className" => "nocnaNegaAntiAge"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "puderi_anti_age", "value" => $puderi_anti_age, "className" => "puderiAntiAge"));
        //telo
        //by range

        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "morski_program_body", "value" => $morski_program_body, "className" => "morskiProgramBody"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "cold_cream_marine_body", "value" => $cold_cream_marine_body, "className" => "coldCreamMarineBody"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "mrsavljenje_body", "value" => $mrsavljenje_body, "className" => "mrsavljenjeBody"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "spa_rituali_pacifik_body", "value" => $spa_rituali_pacifik_body, "className" => "spaRitualiPacifikBody"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "spa_rituali_indoceane_body", "value" => $spa_rituali_indoceane_body, "className" => "spaRitualiIndoceaneBody"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "spa_rituali_atlantik_body", "value" => $spa_rituali_atlantik_body, "className" => "spaRitualiAtlantikBody"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "spa_rituali_arktik_body", "value" => $spa_rituali_arktik_body, "className" => "spaRitualiArktikBody"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "promo_body", "value" => $promo_body, "className" => "promoBody"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "poklon_setovi_body", "value" => $poklon_setovi_body, "className" => "poklonSetoviBody"));

        //man
        //by action

        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "brijanje_man", "value" => $brijanje_man, "className" => "brijanjeMan"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "nega_man", "value" => $nega_man, "className" => "negaMan"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "poklon_setovi_man", "value" => $poklon_setovi_man, "className" => "poklonSetoviMan"));

        //SUN
        //by action

        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "lice_sun", "value" => $lice_sun, "className" => "liceSun"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "telo_sun", "value" => $telo_sun, "className" => "teloSun"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "posle_suncanja_sun", "value" => $posle_suncanja_sun, "className" => "posleSuncanjaSun"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "samopotamnjivanje_sun", "value" => $samopotamnjivanje_sun, "className" => "samopotamnjivanjeSun"));

        //SUPLEMENTI
        //by action

        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "mrsavljenje_suplement", "value" => $mrsavljenje_suplement, "className" => "mrsavljenjeSuplement"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "suncanje_suplement", "value" => $suncanje_suplement, "className" => "suncanjeSuplement"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "anti_age_suplement", "value" => $anti_age_suplement, "className" => "antiAgeSuplement"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "blagostanje_suplement", "value" => $blagostanje_suplement, "className" => "blagostanjeSuplement"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "organska_infuzija_suplement", "value" => $organska_infuzija_suplement, "className" => "organskaInfuzijaSuplement"));

        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "published", "value" => $published, "className" => "published"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "allow_comments", "value" => $allowComments, "className" => "allow-comments"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "rang", "value" => $rang, "className" => "rang"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "intro_image", "value" => $introImage, "className" => "intro-image-field$langSuffix"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "image", "value" => $image, "className" => "image-field$langSuffix"));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "lang_id", "value" => $langId));
        $this->displayFormField(array("type" => "hidden", "resource" => $this->resource, "langId" => $langId, "name" => "lang_group_id", "value" => $this->langGroupId));
        echo '<input type="hidden" name="gallery_json" id="'.$this->resource.'-gallery_json' . $langSuffix . '" value="' . $galleryJson . '" class="gallery-json-field' . $langSuffix . '" />';
        $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "title", "value" => $title, "label" => Trans::get("Title")));
        $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "subtitle", "value" => $subtitle, "label" => Trans::get("Subtitle")));
        $this->displayFormTextarea(array("name" => "intro_text", "resource" => $this->resource, "langId" => $langId, "value" => $introText, "label" => Trans::get("Intro text"), "rows" => 4, "info" => Trans::get("Short description when products are listed, not on the products page itself")));

        //opisi
        $this->displayFormTextarea(array("name" => "opis_1", "resource" => $this->resource, "langId" => $langId, "value" => $opis_1, "label" => Trans::get("Opis 1"), "rows" => 4, "info" => Trans::get("Product description")));
        $this->displayFormTextarea(array("name" => "opis_2", "resource" => $this->resource, "langId" => $langId, "value" => $opis_2, "label" => Trans::get("Opis 2"), "rows" => 4, "info" => Trans::get("Product description")));
        $this->displayFormTextarea(array("name" => "opis_3", "resource" => $this->resource, "langId" => $langId, "value" => $opis_3, "label" => Trans::get("Opis 3"), "rows" => 4, "info" => Trans::get("Product description")));
        $this->displayFormTextarea(array("name" => "opis_4", "resource" => $this->resource, "langId" => $langId, "value" => $opis_4, "label" => Trans::get("Opis 4"), "rows" => 4, "info" => Trans::get("Product description")));

        $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "alias", "value" => $alias, "label" => Trans::get("Alias"), "disabled" => true));
        $this->displayFormTextEditor(array("name" => "content", "resource" => $this->resource, "langId" => $langId, "value" => $content, "label" => Trans::get("Content")));
        echo '<div class="row">';
        echo '<div class="col-md-6">';
        $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "code", "value" => $code, "label" => Trans::get("Code")));
        echo '</div>';
        echo '<div class="col-md-6">';
        $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "price", "value" => $price, "label" => Trans::get("Price")));
        echo '</div>';
        echo '<div class="col-md-6">';
        $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "products_weight", "value" => $products_weight, "label" => Trans::get("Zapremina")));
        echo '</div>';
        echo '</div>';
        echo '<div class="row">';
        echo '<div class="col-md-6">';
        $this->renderDiscountTypeSelectBox($item, $langSuffix);
        echo '</div>';
        echo '<div class="col-md-6">';
        $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "discount_value", "value" => $discountValue, "label" => Trans::get("Discount Value")));
        echo '</div>';
        echo '</div>';
        echo '<div class="row">';
        echo '<div class="col-md-6">';
        $this->renderVendorsSelectBox($item, $langSuffix);
        echo '</div>';
        echo '<div class="col-md-6">';
        $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "youtube_video_code", "value" => $youtubeVideoLink, "label" => Trans::get("Youtube video link"), "info" => Trans::get("Copy Youtube video link and paste it in the field")));
        echo '</div>';
        echo '<div class="col-md-3 side-panels">';




        echo '</div>';

        echo '</div>';
        echo '</div>';
        $this->displayFormField(array("type" => "text", "resource" => $this->resource, "langId" => $langId, "name" => "meta_title", "value" => $metaTitle, "label" => Trans::get("Meta Title"), "info" => Trans::get('For better browser ranking (not visible on page)')));
        echo '<div class="row">';
        echo '<div class="col-md-6">';
        $this->displayFormTextarea(array("name" => "meta_description", "resource" => $this->resource, "langId" => $langId, "value" => $metaDescription, "label" => Trans::get("Meta Description"), "rows" => 4, "info" => Trans::get('For better browser ranking (not visible on page)')));
        echo '</div>';
        echo '<div class="col-md-6">';
        $this->displayFormTextarea(array("name" => "meta_keywords", "resource" => $this->resource, "langId" => $langId, "value" => $metaKeywords, "label" => Trans::get("Meta Keywords"), "rows" => 4, "info" => Trans::get('For better browser ranking (not visible on page)')));
        echo '</div>';
        echo '</div>';

        $this->renderFormGallery($item, $langId);


        echo '</form>';
    }


    public function renderDiscountTypeSelectBox($item, $langSuffix) {

        $percentSelected  = @exists($item->discount_type) && (int)$item->discount_type === (int)Conf::get('discount_type')['percent']  ? ' selected="selected"' : '';
        $absoluteSelected = @exists($item->discount_type) && (int)$item->discount_type === (int)Conf::get('discount_type')['absolute'] ? ' selected="selected"' : '';

        echo '<div class="form-group">';
        echo '<label>' . Trans::get("Discount Type") . '</label>';
        echo '<select class="form-control" id="' . $this->resource . '-discount_type' . $langSuffix . '" name="discount_type">';
        echo '<option value="">' . Trans::get("Choose") . '</option>';
        echo '<option value="' . Conf::get('discount_type')['percent'] . '"' . $percentSelected . '>' . Trans::get("percent") . '</option>';
        echo '<option value="' . Conf::get('discount_type')['absolute'] . '"' . $absoluteSelected . '>' . Trans::get("absolute") . '</option>';
        echo '</select>';
        echo '</div>';
    }


    public function renderVendorsSelectBox($item, $langSuffix) {

        if(@exists($this->vendors)) {

            echo '<div class="form-group">';
            echo '<label>' . Trans::get("Vendors") . '</label>';
            echo '<select class="form-control" id="' . $this->resource . '-vendor_id'. $langSuffix .'" name="vendor_id">';
            echo '<option value="">' . Trans::get("Choose") . '</option>';
            foreach ($this->vendors as $vendor) {

                if((int)$vendor->lang_id === (int)$item->lang_id) {

                    $selected = '';
                    if(@exists($item->vendor_id)) {
                        $selected = (int)$vendor->id === (int)$item->vendor_id ? ' selected="selected"' : '';
                    }

                    echo '<option value="' . $vendor->id . '"' . $selected .'>' . $vendor->name . '</option>';
                }
            }
            echo '</select>';
            echo '</div>';
        }
    }


    /********************************** SORT PAGE **********************************/


    public function displaySortPageHiddenFields() {

        echo '<input type="hidden" id="parentId" value="' . $this->parentId . '"/>';
    }
}
?>