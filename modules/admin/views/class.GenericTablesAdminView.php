<?php


class GenericTablesAdminView extends AdminView {

   public $activeNavigation;
   public $tableName;
   public $tableDescription;
   public $tableContent;

   public function __construct($controller) {

      parent::__construct($controller);

      $this->activeNavigation = "tables";
   }

   public function init($params) {

      if(!is_array($params)) $params = (array) $params;

      if(@exists($params['tableName']))        $this->tableName        = $params['tableName'];
      if(@exists($params['tableDescription'])) $this->tableDescription = $params['tableDescription'];
      if(@exists($params['tableContent']))     $this->tableContent     = $params['tableContent'];
   }


   public function displayTable() {

      echo '<div class="panel-header panel-header-gray">';
         echo '<button type="button" class="btn btn-primary new-entry" data-toggle="modal" data-target="#modal-entry"><i class="icon-plus"></i>' . Trans::get("Add new") . '</button>';
      echo '</div>';

      $this->renderTable();
   }


   public function renderTable() {

      echo '<div class="table-wrapper">';

         echo '<table class="table table-dynamic table-tools filter-select">';

            echo '<thead>';
               echo '<tr>';
                  foreach ($this->tableDescription as $tableRow) {
                     echo '<th>' . $tableRow['name'] . '</th>';
                  }
                  echo '<th class="text-right">' . Trans::get("Actions") . '</th>';
               echo '</tr>';
            echo '</thead>';

            echo '<tbody>';

            foreach ($this->tableContent as $row) {
               echo '<tr data-id="' . $row->id . '">';
                  foreach ($this->tableDescription as $tableRow) {
                     echo $this->displayCell($tableRow, $row);
                  }
                  echo "<td class='text-right'>";
                     echo '<button class="edit btn btn-sm btn-default" data-id="' . $row->id . '" data-toggle="modal" data-target="#modal-entry"><i class="icon-note"></i></button>';
                     echo '<a class="delete btn btn-sm btn-danger" data-id="' . $row->id . '"><i class="icons-office-52"></i></a>';
                  echo '</td>';
               echo '</tr>';
            }

            echo '</tbody>';

            echo '<tfoot>';
            echo '<tr>';
            foreach ($this->tableDescription as $tableRow) {
               if (strtolower($tableRow['name']) !== "id") {
                  echo '<th>' . $tableRow['name'] . '</th>';
               }
               else {
                  echo '<th style="visibility: hidden;"></th>';
               }
            }
            echo '<th style="visibility: hidden;">Actions</th>';
            echo '</tr>';
            echo '</tfoot>';

         echo '</table>';

      echo '</div>';
   }


   public function displayGenericTableModal() {

      echo '<div class="modal fade" id="modal-entry" tabindex="-1" role="dialog" aria-hidden="true">';
         echo '<div class="modal-dialog">';
            echo '<div class="modal-content">';

               echo '<div class="modal-header">';
                  echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>';
                  echo '<h4 class="modal-title"><strong>' . $this->tableName . '</strong>' . Trans::get("data") . '</h4>';
               echo '</div>';

               echo '<div class="modal-body">';
                  echo '<div class="row">';
                     echo '<div class="col-md-12">';
                        echo '<form id="data-form" role="form">';
                           echo '<div class="row">';
                           foreach ($this->tableDescription as $tableRow) {
                              $this->displayInput($tableRow);
                           }
                           echo '</div>';
                        echo '</form>';
                     echo '</div>';
                  echo '</div>';
               echo '</div>';

               echo '<div class="modal-footer bg-gray-light">';
                  echo '<button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">' . Trans::get("Close") . '</button>';
                  echo '<button id="save-changes" type="button" class="btn btn-primary btn-embossed" data-dismiss="modal">' . Trans::get("Save") . '</button>';
               echo '</div>';

            echo '</div>';
         echo '</div>';
      echo '</div>';
   }
}
?>