<?php


class Sessions extends Model {
    
    public function __construct() {  
        
        parent::__construct();            
        $this->setTable("sessions");
        sessionStart();
    }
    
        
    public function open ($user) {                
        
        $session['ip'] = clientIP();
        $session['user_id'] = $user->id;
        $session['auth_token'] = Encryption::generateStamp();
        $session['status'] = 1; 
        $this->insert($session); 
        
        if (Conf::get('session_php')) $_SESSION[Conf::get('session_prefix') . 'token'] = $session['auth_token']; 
    }
    
    public function close () {
        
        if (Conf::get('session_php')) $auth_token = $_SESSION[Conf::get('session_prefix') . 'token'];                 

        $session['status'] = 0;         
        $session['auth_token'] = $auth_token; 
        $this->update($session, array("auth_token")); 
        
        unset($_SESSION[Conf::get('session_prefix') . 'language']);
        unset($_SESSION[Conf::get('session_prefix') . 'token']);
    }
    
    public function active () {
       
        if (Conf::get('session_php')) {
            if (!isset($_SESSION[Conf::get('session_prefix') . 'token'])) return false; 
            $auth_token = $_SESSION[Conf::get('session_prefix') . 'token']; 
        }

        $sql = "select `s`.*, `u`.*
                from `sessions` as `s`
                left join `users` as `u` on `s`.`user_id` = `u`.`id`
                where `s`.`auth_token` = :auth_token";

        $active = $this->exafe($sql, array("auth_token" => $auth_token));
        
        if ($active) {
            if ($active->status) return $active; 
        }
        
        return false; 
    }
}
?>