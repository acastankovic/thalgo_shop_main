<?php


class Users extends Model {        
    
    private $selectQuery;

    public function __construct () {
        parent::__construct();
        $this->setTable("users");
        $this->setSelectQuery();
    }  
    

    /************************************ FETCH ************************************/  


    public function getOne($id) {

        $sql  = $this->selectQuery;
        $sql .= " where `u`.`id` = :id";

        $result = $this->exafe($sql, array("id" => $id));

        return $result; 
    }


    public function getAll() {

        $sql = $this->selectQuery;
        $results = $this->exafeAll($sql);

        foreach ($results as $result) {
            unset($result->password);
        }
        return $results; 
    }


    public function getByUsernameAndPassword($username, $password) {

        $password = Encryption::encode($password);

        $sql = "select * from `users` where `username` = :username and `password` = :password";
        $result = $this->exafe($sql, array("username" => $username, "password" => $password));
        return $result;
    }


    public function getByIdAndPassword($id, $password) {

        $password = Encryption::encode($password);

        $sql = "select * from `users` where `id` = :id and `password` = :password";
        $result = $this->exafe($sql, array("id" => $id, "password" => $password));
        return $result;
    }
    

    public function getByUsername($username) {
        
        $sql = "select * from `users` where `username` = :username";
        $result = $this->exafe($sql, array("username" => $username));
        return $result; 
    }


    /************************************ ACTIONS ************************************/


    public function insertUser($params) {

        $params['password'] = Encryption::encode($params['password']);

        $this->insert($params);

        $result = $this->lastInsertId();
        return $result;         
    }


    public function changePassword($id, $password) {

        $password = Encryption::encode($password);

        $sql = "update `users` set `password` = :password where `id` = :id";
        $this->execute($sql, array("id" => $id, "password" => $password));
    }


    /************************************ OTHER ************************************/


    private function setSelectQuery() {
        $this->selectQuery = "select `u`.*, 
                              `r`.`name` as `role`
                              from `users` as `u` 
                              left join `roles` as `r` on `u`.`role_id` = `r`.`id`";
    }    
    

    public function userExist($username) {

        $user = $this->getByUsername($username);
        
        return exists($user) && (bool)$user !== false;
    }


    public function passwordExist($id, $password) {

        $user = $this->getByIdAndPassword($id, $password);

        return exists($user) && (bool)$user !== false;
    }
}
?>