<?php


class AuthenticationController extends Controller {

    protected $user;
    
    public function __construct() {  
        
        parent::__construct();
    }


    /*
     * @param   string username
     * @param   string  password
     *
     * @return  object
    */
    public function login () {       
        
        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $username = $params['username'];
        $password = $params['password'];

        $users = new Users();
        $user = $users->getByUsernameAndPassword($username, $password);

        if($user) {
            //set session parameters. 
            $this->user = $user; 
            $session = new Sessions();
            $session->open($user);
            $this->view->respond($user, null);
        }
        else $this->view->respondError($users->getError(), null);
        
        return $user; 
    }
    
    
    public function logout () {       
                
        $session = new Sessions();
        $session->close();
    }    
    
    
    public function active () {
        
        $session = new Sessions();
        $session->active();
        
        $this->view->respond(null, null);
    }
    

    public function getActiveSession () {
        $session = new Sessions();
        $activeSession = $session->active();
        //todo: check if session is valid, expired and close it if needed
        return $activeSession; 
    }
}
?>