<?php


class UsersController extends Controller {

    private $usersModel;
        
    public function __construct() {
        parent::__construct();
        $this->model->setTable("users");

        $model = Users::Instance();

        if($model instanceof Users) {
            $this->usersModel = $model;
        }

        Trans::initTranslations();
    }
    

    /************************************ FETCH ************************************/


    /*
     * @param   int  id
     *
     * @return  object
    */
    public function fetchOne() {    
        
        $id = trim(Security::Instance()->purifier()->purify($this->params('id')));

        $data = $this->usersModel->getOne($id);

        $this->view->respond($data, null);
        return $data; 
    }


    /*
     * @param   NULL
     *
     * @return  array of objects
    */
    public function fetchAll() {

        $data = $this->usersModel->getAll();

        $this->view->respond($data, null);      
        return $data; 
    }


    /************************************ ACTIONS ************************************/


    /*
     * @param   string  username
     * @param   string  password
     *
     * @return  object:
     * 1) bool   success
     * 2) string message
     * 3) int    id - last inserted
    */
    public function insertUser() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = new stdClass();

        if(!@exists($params['username'])) {
            $data->success = false;
            $data->message = Trans::get('Username required');
        }
        else if(!@exists($params['password'])) {
            $data->success = false;
            $data->message = Trans::get('Password required');
        }
        else if($this->usersModel->userExist($params['username'])) {
            $data->success = false;
            $data->message = Trans::get('User with username') . ': "' . $params['username'] . '" ' . Trans::get('already exists');
        }
        else{

            $data->id      = $this->usersModel->insertUser($params);
            $data->success = true;
            $data->message = Trans::get("User created");
        }

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * @param   array of form params
     *
     * @return  object:
     * 1) bool   success
     * 2) string message
    */
    public function updateUser() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $data = new stdClass();

        if(!@exists($params['username'])) {
            $data->success = false;
            $data->message = Trans::get('Username required');
        }
        else{

            $this->usersModel->update($params);

            $data->success = true;
            $data->message = Trans::get("User updated");
        }

        $this->view->respond($data, null);
        return $data;
    }


    /*
     * @param   int  id
     *
     * @return  NULL
    */
    public function delete() {
        
        $id = trim(Security::Instance()->purifier()->purify($this->params('id')));

        $this->usersModel->delete($id);

        $this->view->respond(null, null);
        return null;
    }


    /*
     * @param   int     id
     * @param   string  password_current
     * @param   string  password_new
     * @param   string  password_repeat_new
     *
     * @return  object:
     * 1) bool   success
     * 2) string message
    */
    public function changePassword() {

        $params = trimFields(Security::Instance()->purifyAll($this->params()));

        $id                  = $params['id'];
        $password_current    = $params['password_current'];
        $password_new        = $params['password_new'];
        $password_repeat_new = $params['password_repeat_new'];

        if(!$this->passwordsMatch($password_new, $password_repeat_new)) {
            $success = false;
            $message = Trans::get("Passwords don't match");
        }
        else{

            if(!$this->usersModel->passwordExist($id, $password_current)) {
                $success = false;
                $message = Trans::get("Current password is incorrect");
            }
            else{

                $this->usersModel->changePassword($id, $password_new);

                $success = true;
                $message = Trans::get("Password changed");
            }           
        }


        $data = new stdClass();
        $data->success = $success;
        $data->message = $message;

        $this->view->respond($data, null);
        return $data;
    }


    /************************************** OTHER **************************************/
    

    private function passwordsMatch($password1, $password2) {
        return (string)$password1 === (string)$password2;
    }
}
?>