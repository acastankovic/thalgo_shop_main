<?php


class UsersApplication extends Application {    
    
    protected $name = "users";
    
    protected $resources = array ("users", "roles");
    
    protected $routes = array (
        
        //get user
        array("route"       => ":id", 
              "method"      => "get", 
              "controller"  => "users", 
              "action"      => "fetchOne"),

        //get users
        array("route"       => "", 
              "method"      => "get", 
              "controller"  => "users", 
              "action"      => "fetchAll"),
                  
        //delete user
        array("route"       => ":id", 
              "method"      => "delete", 
              "controller"  => "users", 
              "action"      => "delete"),
        
        //insert user
        array("route"       => "insert", 
              "method"      => "post", 
              "controller"  => "users", 
              "action"      => "insertUser"),
        
        //update user
        array("route"       => "update", 
              "method"      => "put",
              "controller"  => "users", 
              "action"      => "updateUser"),
        
        //change password
        array("route"       => "change-password", 
              "method"      => "put",
              "controller"  => "users", 
              "action"      => "changePassword"),
        
        //authentication login
        array("route"       => "login", 
              "method"      => "post", 
              "controller"  => "authentication", 
              "action"      => "login"),
        
        //authentication login
        array("route"       => "logout", 
              "method"      => "post", 
              "controller"  => "authentication", 
              "action"      => "logout"),
        
        //authentication active
        array("route"       => "active", 
              "method"      => "get", 
              "controller"  => "authentication", 
              "action"      => "active")
    );
}
?>