<?php


class Encryption {


    public static function encode($string) {

        return hash(Conf::get('enc_type'), $string . Conf::get('enc_hash'));
    }
    
    
    public static function generateStamp() {

        return sha1(rand (1000,9999) . time() . rand (1000,9999));
    }
}
?>