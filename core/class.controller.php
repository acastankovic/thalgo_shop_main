<?php
//////////////////////////////////////////////////
// 
// Project: Norma Core
// Company: Normasoft
// Author: Milos Pavlovic
// Email: milos.pavlovic@normasoft.net 
// Date: Spt 21, 2015
// Controller
//
//////////////////////////////////////////////////

class Controller {
    
    protected $urlParams;
    protected $request;
    protected $view; 
    protected $module; 
    protected $model; 
    protected $user;
    protected $langAlias;
    protected $routes;
    
    public function __construct() {

        $view = View::Instance();
        $this->view = $view instanceof View ? $view : new View();
        $this->view->setController($this);

        $model = Model::Instance();
        $this->model = $model instanceof Model ? $model : new Model();
    }
    
    public function setUrlParams($params) {
        $this->urlParams = $params; 
    }
    
    public function setRequest($request) {

        $this->request = $request; 
        $this->view->setRespondType($this->request->getRespondType());
    }
    
    public function getRequest() {
        return $this->request;
    }      
    
    public function setModule($module) {
        $this->module = $module;
        $this->view->setModuleName($this->module->alias);
        $this->view->setIsRootModule($this->module->root);
    }
    
    public function getModuleName() {
        return $this->module->alias; 
    }
    
    public function isRootModule() {
        return $this->module->root; 
    }

    public function setLangAlias($langAlias) {
        $this->langAlias = $langAlias;
    }

    public function langAlias() {
        return $this->langAlias;
    }

    public function setRoutes($routes) {
        $this->routes = $routes;
    }

    public function params($key = null) {        

        $result = array ();
        if (is_array($this->request->get)) $result = $this->request->get; 
        if (is_array($this->request->post)) $result = array_merge($result, $this->request->post); 
        if (is_array($this->request->put)) $result = array_merge($this->request->put, $result); 
        if (is_array($this->request->delete)) $result = array_merge($result, $this->request->delete);
        if (is_array($this->urlParams)) $result = array_merge($result, $this->urlParams);

        if (isset($key)) return $result[$key];

        return $result;
    }
    
    public function post() {
        return $this->request->post; 
    }
    
    public function run($action) {
        $result = $this->$action();
        return $result; 
    }
    
    //basic crud
    public function fetch() {
        
        $data = $this->model->load();
        
        $this->view->respond($data, null);
        return $data; 
    }
    
    
    public function fetchOne() {
        
        $data = $this->model->load($this->params("id"));
        $this->view->respond($data, null);
        return $data; 
    }
    
    public function insert() {
        
        $this->model->insert($this->params());
        $data = $this->model->loadLastInsert();
        
        if ($data) $this->view->respond($data, null);       
        else $this->view->respondError($this->model->getError(), null);
        
        return $data; 
    }
    
    public function update() {
                
        $this->model->update($this->params());
        $data = $this->model->load($this->params('id'));
        
        if ($data) $this->view->respond($data, null);       
        else $this->view->respondError($this->model->getError(), null);
        
        return $data; 
    }
    
    public function delete() {

        $result = $this->model->delete($this->params('id'));
        
        if ($result) $this->view->respond($result, null);       
        else $this->view->respondError($this->model->getError(), null);
        
        return $result; 
    }

    public function required($data, $requirements) {
        
        return $this->model->required($data, $requirements);
    }


    public function aliasDecoding($alias = null) {

        $params = $this->params();

        $aliasString = "";
        if(isset($alias)) {
            $aliasString = $alias;
        }
        else if(isset($params["alias"])) {
            $aliasString = $params["alias"];
        }

        $aliases = explode("/", $aliasString);
        
        $breadcrumb = array();
        $subCategoriesTree = array();

        $categories = Categories::getCategories(Trans::getLanguageId());
        foreach ($categories as $category) {
            $category->type = "category";
        }
        
        $pageType = ""; 
        $parentId = 0;
        $articles       = null;
        $article        = null;
        $shopCategories = null;
        $productsCount  = null;
        $products       = null;
        $product        = null;

        foreach($aliases as $alias) {

            $breadcrumbItem = new stdClass();
            $category = $this->findCategory($categories, $parentId, $alias);
 
            if ($category) {  

                $parentId = $category->id;
                $breadcrumbItem->category = $category;     
                
                if ($alias === end($aliases)) {
                    $subCategoriesTreeArray = formTree($categories, $category->id);
                    $subCategoriesTree = $subCategoriesTreeArray[0];


                    if ($this->isProductCategory($categories, $category)) {

                        if(isset($subCategoriesTree['children'])) {

                            $shopCategories = $subCategoriesTree['children'];
                            $pageType = "shop-category";
                        }
                        else{

//                            $products = Dispatcher::instance()->dispatch("shop", "products", "fetchByParentId", array("parent_id" => $category->id));

                            $p = array();
                            $p['parent_id'] = $category->id;
                            if(@exists($params['items_per_page'])) $p['items_per_page'] = $params['items_per_page'];
                            if(@exists($params['page'])) $p['page'] = $params['page'];
                            if(@exists($params['vendor_id'])) $p['vendor_id'] = $params['vendor_id'];

                            $results = Dispatcher::instance()->dispatch("shop", "products", "fetchWithFilters", $p);

                            $productsCount = $results->total;
                            $products = $results->items;
                            $pageType = "shop-last-child-category";
                        }
                    }
                    else{

                        $articles = Dispatcher::instance()->dispatch("content", "articles", "fetchByParentId", array("parent_id" => $category->id));
                        $pageType = "content-category";
                    }
                }
            }
            else {
                if ($alias === end($aliases)) {

                    $article = Dispatcher::instance()->dispatch("content", "articles", "fetchByAlias", array("parent_id" => $parentId, "alias" => $alias, "shortcodes" => true));
                    if (isset($article->id)) {
                        $breadcrumbItem->article = $article;
                        $pageType = "content-article";
                    }

                    $product = Dispatcher::instance()->dispatch("shop", "products", "fetchByAlias", array("parent_id" => $parentId, "alias" => $alias));

                    if (isset($product->id)) {
                        $breadcrumbItem->product = $product;
                        $pageType = "shop-product";
                    }
                }
            }
            array_push($breadcrumb, $breadcrumbItem);
        }

        $data = new stdClass();

        $data->categories        = $categories;
        $data->category          = $category;
        $data->shopCategories    = $shopCategories;
        $data->subCategoriesTree = $subCategoriesTree;
        $data->articles          = $articles;
        $data->products          = $products;
        $data->productsCount     = $productsCount;
        $data->product           = $product;
        $data->article           = $article;
        $data->breadcrumbs       = $breadcrumb;
        $data->pageType          = $pageType;

        return $data; 
    }    
    
    
    private function findCategory($categories, $parentId, $alias) {
        
        foreach ($categories as $category) {

            if ((int)$category->parent_id === (int)$parentId && $category->alias == $alias) return $category;
        }
        return false; 
    }


    private function isProductCategory($categories, $category) {

        if ( ((int)$category->id === (int)Conf::get('shop_category_id')) || (isset($category->lang_group_id) && (int)$category->lang_group_id === (int)Conf::get('shop_category_id')) ) return true;

        while ($category->parent_id) {

            foreach ($categories as $cat) {

                if ((int)$cat->id === (int)$category->parent_id) {
                    if ( ((int)$cat->id === (int)Conf::get('shop_category_id')) || (isset($cat->lang_group_id) && (int)$cat->lang_group_id === (int)Conf::get('shop_category_id')) ) return true;
                    $category = $cat;
                }
            }
        }

        return false;
    }


    protected function setLanguageByAlias($langAlias = null) {

        if(isset($langAlias)) {
            Trans::setLanguageByAlias($langAlias);
        }
    }
}
?>