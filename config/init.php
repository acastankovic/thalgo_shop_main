<?php

   require_once(str_replace("config", "", __DIR__) . "core/class.config.php");

   // config file
   require_once("params/params.php");

	 // system config
	 require_once("params/sys/params.php");

	 // init core autoloaders
	 require_once Conf::get('root').'/config/autoloaders.php';

	 // modules autoloaders
	 require_once Conf::get('root').'/modules/users/config/autoloaders.php';
	 require_once Conf::get('root').'/modules/content/config/autoloaders.php';
    require_once Conf::get('root').'/modules/shop/config/autoloaders.php';
	 require_once Conf::get('root').'/modules/admin/config/autoloaders.php';


    /********* libraries *********/


    // composers
    require Conf::get('root').'/vendor/autoload.php';

    // aditional functions
    require Conf::get('root').'/core/libs/functions.php';

?>