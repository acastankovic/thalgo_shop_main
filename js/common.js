if (Number.parseFloat === undefined) Number.parseFloat = parseFloat;
if (Number.parseInt === undefined) Number.parseInt = parseInt;

function ajaxCall(url, type, data, successHandler, errorHandler) {

   if (typeof errorHandler === "undefined") errorHandler = defaultErrorHandler; 

   $.ajax({
      url: BASE_URL + url,
      type: type,
      data: data,
      beforeSend: function(xhr) { 
         xhr.setRequestHeader("Accept","application/json");
      },
      success: successHandler, 
      error: errorHandler
   });
};


function defaultErrorHandler(error) {
    alert("Server error");
    console.log(error);
};


function ajaxBusy() {
    
   $(document).ajaxStart(function(){ 
      $('#ajaxOverlay').show();
      $('#loader').show();            
   }).ajaxStop(function(){ 
      $('#ajaxOverlay').hide();
      $('#loader').hide();              
   });
};


function ajaxSuccess(response) {
   return typeof response != "undefined" && response.success;
};


function formatNumber(value) {
   return parseFloat(value).toFixed(2);
};


// function formatPrice(price) {
//
//    newPrice = parseFloat(price).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1.');
//    newPrice = newPrice.replace(/.([^.]*)$/, ",$1");
//
//    return newPrice;
// };

function formatPrice(n, c, d, t) {

    var defaultD = ".";
    var defaultT = ",";
    if(exists(SITE_LANG)) {
        if(SITE_LANG == "rs" || SITE_LANG == "ср") {
            defaultD = ",";
            defaultT = ".";
        }
    }

    var c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? defaultD : d,
        t = t == undefined ? defaultT : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};


// check if required fields are empty
function validateRequiredFields(form) {

   var passed = true;

   $(form).find('.required').each(
      function() {

         var value = $(this).val();
         if(value == '') passed = false;
      }
   );

   return passed;
};


function validateRequiredFieldsWithWarningMessage(form, message) {

    var passed = true;

    $(form).find('.required').each(
        function() {

            var value = $(this).val();
            var originalPlaceholder = $(this).attr('data-placeholder');

            if(value == '') {
              passed = false;

              if(typeof message == 'undefined' || message == null || message == '') {
                if(typeof SITE_LANG != 'undefined' && SITE_LANG != null && SITE_LANG != '' && SITE_LANG == 'sr'){
                  message = 'obavezno';
                }
                else {
                  message = 'required';
                }
              }

              var warningMessage = originalPlaceholder + ' - ' + message;
              $(this).addClass('warning');
              $(this).attr('placeholder', warningMessage);
            }
            else{

                $(this).removeClass('warning');
                $(this).attr('placeholder', originalPlaceholder);
            }

        }
    );

    return passed;
};


function setFormFieldsPlaceholderAttribute(form) {

   $(form).find('.form-field').each(
      function() {

         var placeholder = $(this).attr('placeholder');

         var placeholderValue = typeof placeholder != 'undefined' ? placeholder : '';

         $(this).attr('data-placeholder', placeholderValue);
      }
   );
};


function clearFormFields(btnElem) {

   $(btnElem).parents('form').find('.form-field').each(
      function() {

         var originalPlaceholder = $(this).attr('data-placeholder');
         $(this).val('');
         $(this).removeClass('warning');
         $(this).attr('placeholder', originalPlaceholder);         
      }
   );
};


function setCaptchaRequiredClass() {

   if( $('#captcha_code').length > 0 ) $('#captcha_code').addClass('required');
};


function printContent(div) {
   var newWin= window.open('', 'win');
   newWin.document.write(div.innerHTML);
   newWin.location.reload();
   newWin.focus();
   newWin.print();
   newWin.close();
};


function backToHome(timeDelay) {
   setTimeout(function() {
      location.href = BASE_URL;
   }, timeDelay);
};


function pageReload(timeDelay) {
   setTimeout(function() {
      location.reload();
   }, timeDelay);
};


function validateEmail(data) {
  var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return emailRegex.test(data);
};


function validatePhoneNumber(data) {
  var phoneRegex = /^[+]?([0-9]|[\(]|[\)]|-|[\s]|[\/])*$/g;
  return phoneRegex.test(data);
};


function validateNumber(data) {
  return $.isNumeric(data);
};


// da li je broj deljiv
function isNumberDivisible(a, b) {
   return a % b == 0;
};


// filter youtube url
function getYouTubeVideoCode(url) {

   var ID = '';

   url = url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);

   if (url[2] !== undefined) {
      ID = url[2].split(/[^0-9a-z_\-]/i);
      ID = ID[0];
   } else {
      ID = url;
   }

   return ID;
};


function getQueryVariable(variable) {

   var query = window.location.search.substring(1);
   var vars = query.split("&");
   
   for ( var i = 0; i < vars.length; i++) {
      var pair = vars[i].split("=");
      if(pair[0] == variable){
         return pair[1];
      }
   }
   return(false);
};


function exists(data) {
   return typeof data != "undefined" && data != null && data != '';
};


function domElemExists(elem) {
   return $(elem).length > 0;
};


function valueZero(value) {
   return parseFloat(value) === 0;
};


function displayMediaImage(image) {
   return exists(image) ? MEDIA_URL + '/' + image : CSS_IMG_URL + '/no-image.png';
};


function stripBeginningAndEndingSlashes(string) {
    return string.replace(/^\/|\/$/g, '');
};


function getUrlRoute() {
    var currentUrl = window.location.href;
    var route = currentUrl.replace(BASE_URL, '');
    return stripBeginningAndEndingSlashes(route);
};


function setUrlVariables() {

   var variables = getUrlVariables();

   if(!variables) {
      variables = [];
   }

   return variables;
};


function getUrlWithoutVariables() {

   var currentUrl = window.location.href;
   var currentUrlParts = currentUrl.split("?");
   var pageBaseUrl = stripBeginningAndEndingSlashes(currentUrlParts[0]);

   return pageBaseUrl;
};


function getUrlVariables() {

   var query = window.location.search.substring(1);
   if(query == "") return false;
   var variables = query.split("&");

   if (typeof variables == "undefined" || variables.length == 0) return false;

   var varsArray = [];
   for (var i = 0; i < variables.length; i++) {

      var pair = variables[i].split("=");

      varsArray[pair[0]] = pair[1];
   }

   return varsArray;
};


function eventPreventDefault(event) {
   event.preventDefault ? event.preventDefault() : (event.returnValue = false);
};


function findElementByScroll(elem) {

    if($(elem).length == 0) {
        return;
    }

    var windowTop      = $(window).scrollTop();
    var windowHeight   = $(window).height();
    var divHeight      = $(elem).height();
    var divTop         = $(elem).offset().top;

    var windowBottom = windowTop + windowHeight;
    var divBottom    = divTop + divHeight;

    var windowBottomEnteredDiv = windowBottom >= divTop && windowBottom <= divBottom;
    var windowTopEnteredDiv    = windowTop >= divTop && windowTop <= divBottom;
    var divVisibleInWindow		= windowTop <= divTop && windowBottom >= divBottom;

    // console.log('windowTop = ' + windowTop);
    // console.log('windowBottom = ' + windowBottom);
    // console.log('windowHeight = ' + windowHeight);
    // console.log('divTop = ' + divTop);
    // console.log('divBottom = ' + divBottom);
    // console.log('divHeight = ' + divHeight);
    // console.log("----------------");

    return divVisibleInWindow || windowTopEnteredDiv || windowBottomEnteredDiv;
};
