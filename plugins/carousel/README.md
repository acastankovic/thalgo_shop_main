<h1>Carousel</h1>

<h2>Include</h2>

<p>Javascript and CSS files need to be included into script</p>

```html
<link rel="stylesheet" type="text/css" href="/plugins/carousel/carousel.min.css" />
<script src="/plugins/carousel/carousel.min.js"></script>
```

<h2>HTML</h2>

<p>For HTML unordered list should be given id or class wich is used for carousel initialization</p>
<p>LI items should contain content needed for display</p>


<h3>example:</h3>

```html
<ul id="carousel">
    <li><div>Item 1</div></li>
    <li><div>Item 2</div></li>
    <li><div>Item 3</div></li>
</ul>
```


<h2>JQUERY</h2>

<p>ALl params passed to carousel plugin are optional</p>


<h3>example:</h3>

```javascript
$('#carousel').carousel(
    {
        carouselSpeed: "",         // optional, int (default 4000)
        stopOnHover: "",           // optional, boolean (default false)
        responsiveBreakPoint: "",  // optional, int (default 992) - if stop on hover is "on" it will not work under break point resolutions 
        showNavigation: "",        // optional, boolean (default true)
        showBullets: "",           // optional, boolean (default true)
        spinAutomatically: ""      // optional, boolean (default true)
    }
);
```