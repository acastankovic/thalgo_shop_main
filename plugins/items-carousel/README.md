<h1>Items Carousel</h1>


<h2>Include</h2>

<p>Javascript and CSS files need to be included into script</p>

```html
<link rel="stylesheet" type="text/css" href="/plugins/items-carousel/items-carousel.min.css" />
<script src="/plugins/items-carousel/items-carousel.min.js"></script>
```

<h2>HTML</h2>

<p>For HTML unordered list should be given id or class wich is used for carousel initialization</p>
<p>LI items should contain content needed for display</p>


<h3>example:</h3>

```html
<ul id="itemCarousel">
    <li><div>Item 1</div></li>
    <li><div>Item 2</div></li>
    <li><div>Item 3</div></li>
</ul>
```


<h2>JQUERY</h2>

<p>ALl params passed to carousel plugin are optional</p>


<h3>example:</h3>

```javascript
$('#itemCarousel').itemsCarousel(
    {
        carouselSpeed: "",      // optional, int (default 5000 ms)
        itemsChangingSpeed: "", // optional, int (default 380 ms)
        showNavigation: "",     // optional, boolean (default true)
        itemsPerRow: "",        // optional, int (default 5)
        spinAutomatically: ""   // optional, boolean (default false)
    }
);
```