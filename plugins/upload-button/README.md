<h1>Custom Upload Button</h1>

<h2>Include</h2>

<p>Javascript and CSS files need to be included into script</p>

```html
<link rel="stylesheet" type="text/css" href="/plugins/upload-button/upload-button.min.css" />
<script src="/plugins/upload-button/upload-button.min.css"></script>
```

<h2>HTML</h2>

<p>For HTML input file tag should be given id or class wich is used for plugin initialization</p>


<h3>example:</h3>

```html
<input type="file" id="uploadButton" />
```


<h2>JQUERY</h2>

<p>ALl params passed to upload button plugin are optional</p>


<h3>example:</h3>

```javascript
$('#uploadButton').uploadButton(
    {
        buttonTitle: ""  // optional, string (default "Browse")
    }
);
```