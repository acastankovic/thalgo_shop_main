<header>
    <div class="top-bar">
        <div class="container">
            <div class="top-bar-left">
                <a href="#">thalgo</a>
                <span class="top-bar-divider">|</span>
                <a href="#">sea cleaners</a>
            </div>
            <div class="top-bar-right">
                <a href="#">profesional</a>
                <span class="top-bar-divider">|</span>
                <a href="#">kontakt</a>
            </div>
        </div>
    </div>
    <div class="middle-bar">
        <div class="container clearfix">
            <div class="middle-bar-wrapper">
                <div class="empty">
                </div>
                <div class="site-logo">
                    <a href="<?php echo Conf::get('url'); ?>"><img src="<?php echo Conf::get('css_img_url'); ?>/logo.svg" alt="logo"/></a>
                </div>
                <div class="widget-wrapper">
                    <?php Dispatcher::instance()->dispatch("normacore", "layout", "cartWidget", null, Request::HTML_REQUEST); ?>
                    <div class="search-wrapper">
                        <form>
                            <input class="search-input" type="search">
                            <button type="submit" ><img src="<?php echo Conf::get('css_img_url'); ?>/ico-search 1.png" alt="search"/></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container clearfix">
        <?php Dispatcher::instance()->dispatch('normacore', 'layout', 'mainMenu', null, Request::HTML_REQUEST); ?>
    </div>
</header>

<?php // Dispatcher::instance()->dispatch('normacore', 'layout', 'languages', null, Request::HTML_REQUEST); ?>