<div id="siteNewsletter">

  <div class="container">

    <h3>Prijavite se na naš Newsletter</h3>

    <div class="field-wrapper">
      <input type="text" class="field" placeholder="E-mail">
      <button type="button" class="site-btn">Prijava</button>
    </div>

  </div>

</div>

<footer>

  <div class="container">

    <div class="col menu">
      <h5>Brzi linkovi</h5>
      <a href="#">Početna</a>
      <a href="#">O nama</a>
      <a href="#">Novosti</a>
      <a href="#">Kontakt</a>
    </div>

    <div class="col links">
      <h5>Korisni linkovi</h5>
      <a href="www.skf.com" target="_blank">www.skf.com</a>
      <a href="www.skf.rs" target="_blank">www.skf.rs</a>
      <a href="www.skfptp.com" target="_blank">www.skfptp.com</a>
      <a href="www.loctite.rs" target="_blank">www.loctite.rs</a>
    </div>

    <div class="col reg-data">
      <h5>Registarski podaci</h5>
      <p>TP STANDARD TRADE D.O.O</p>
      <p>DIREKCIJA/MAGACIN</p>
      <p>Ul. Cvijićeva bb</p>
      <p>11080 Zemun</p>
      <p>tel/fax: 011/2618-771</p>
    </div>


    <div class="col business-data">
      <h5>Poslovne informacije</h5>
      <p>Tekući račun: 205-81087-06</p>
      <p>Komercijalna banka</p>
      <p>PIB: 103541570</p>
      <p>Mat. br. 17570749</p>
      <p>Šif. del. 4690</p>
      <p>Reg. br. 01801750749</p>
    </div>

  </div>


</footer>

<div class="site-rights"><?php echo Conf::get('site_name'); ?> © <?php echo date('Y') . ', ' . Trans::get('All rights reserved') . ' | ' . Trans::get('Powered by'); ?> <a href="http://normasoft.net/" target="_blank">Normasoft</a></div>

<div id="overlay" class="page-overlay"></div>
<div id="loader">
  <div class="loader-dots">
    <span></span><span></span><span></span>
  </div>
</div>

<a id="backTop" class="site-btn"><i></i></a>

<?php $this->displaySearchForm(); ?>