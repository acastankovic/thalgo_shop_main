<link rel="stylesheet" type="text/css" href="<?php echo Conf::get('url'); ?>/css/common.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Conf::get('url'); ?>/css/main.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Conf::get('url'); ?>/css/cart-widget.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Conf::get('url'); ?>/css/menu.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Conf::get('url'); ?>/css/sidebar.css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">

