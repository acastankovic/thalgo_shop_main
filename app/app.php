<?php


class NormacoreApplication extends Application {

    protected $name = "normacore";

    protected $resources = array ();

    protected $default = array(
        "route"       => ":alias",
        "method"      => "get",
        "controller"  => "pages",
        "action"      => "aliasDecoding"
    );

    protected $routes = array (

        /************* STATIC *************/

        array("route"       => "",
              "method"      => "get",
              "controller"  => "pages",
              "action"      => "indexPage"),

        array("route"       => "kontakt",
              "method"      => "get",
              "controller"  => "pages",
              "action"      => "contactPage",
              "langAlias"   => "sr"),

        array("route"       => "контакт",
              "method"      => "get",
              "controller"  => "pages",
              "action"      => "contactPage",
              "langAlias"   => "ср"),

        array("route"       => "contact",
              "method"      => "get",
              "controller"  => "pages",
              "action"      => "contactPage",
              "langAlias"   => "en"),

        array("route"       => "rezultati-pretrage",
              "method"      => "post",
              "controller"  => "pages",
              "action"      => "searchResultsPage",
              "langAlias"   => "sr"),

        array("route"       => "резултати-претраге",
              "method"      => "get",
              "controller"  => "pages",
              "action"      => "searchResultsPage",
              "langAlias"   => "ср"),

        array("route"       => "search-results",
              "method"      => "post",
              "controller"  => "pages",
              "action"      => "searchResultsPage",
              "langAlias"   => "en"),


        /************* SHOP *************/

        array("route"       => "cart",
              "method"      => "get",
              "controller"  => "pages",
              "action"      => "cartPage",
              "langAlias"   => "en"),


        array("route"       => "korpa",
              "method"      => "get",
              "controller"  => "pages",
              "action"      => "cartPage",
              "langAlias"   => "sr"),

        array("route"       => "checkout",
              "method"      => "get",
              "controller"  => "pages",
              "action"      => "orderPage",
              "langAlias"   => "en"),

        array("route"       => "poruci",
              "method"      => "get",
              "controller"  => "pages",
              "action"      => "orderPage",
              "langAlias"   => "sr"),

        array("route"       => "katalog-pretraga",
              "method"      => "get",
              "controller"  => "pages",
              "action"      => "shopSearchResultsPage",
              "langAlias"   => "sr"),

        array("route"       => "каталог-претрага",
              "method"      => "get",
              "controller"  => "pages",
              "action"      => "shopSearchResultsPage",
              "langAlias"   => "ср"),

        array("route"       => "catalog-search",
              "method"      => "get",
              "controller"  => "pages",
              "action"      => "shopSearchResultsPage",
              "langAlias"   => "en"),

        /************* ASYNC *************/

        array("route"       => "send-email",
              "method"      => "post",
              "controller"  => "async",
              "action"      => "sendEmail"),

        array("route"       => "languages-set",
              "method"      => "post",
              "controller"  => "async",
              "action"      => "languagesSet"),

        array("route"       => "add-comment",
              "method"      => "post",
              "controller"  => "async",
              "action"      => "addComment"),

        array("route"       => "newsletter-signup",
              "method"      => "post",
              "controller"  => "async",
              "action"      => "newsletterSignup"),


        /************* TEST *************/

        array("route"       => "тест",
              "method"      => "get",
              "controller"  => "pages",
              "action"      => "testPage",
              "langAlias"   => "ср"),

        array("route"       => "test",
              "method"      => "get",
              "controller"  => "pages",
              "action"      => "testPage",
              "langAlias"   => "en"),
    );
}
?>
