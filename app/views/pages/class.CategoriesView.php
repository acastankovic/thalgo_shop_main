<?php


class CategoriesView extends MainView implements PagesViewInterface {

   public $pageName;
   private $breadcrumbs;
   private $category;
   private $articles;
   private $subCategories;


   public function __construct($data) {
      parent::__construct();

      if(@exists($data)) {

         if(@exists($data->breadcrumbs)) {
            $this->breadcrumbs = $data->breadcrumbs;
         }

         if(@exists($data->category)) {
            $this->category = $data->category;
            $this->pageName = ucfirst($this->category->name);
         }

         if(@exists($data->articles)) {
            $this->articles = $data->articles;
         }

         if(@exists($data->subCategories)) {
            if(@exists($data->subCategories['children'])) {
               $this->subCategories = $data->subCategories['children'];
            }
         }
      }
   }


   // meta title tag
   public function displayMetaTitle() {
     $this->renderMetaTitle($this->pageName, $this->category);
   }


   // meta description, keywords and og tags
   public function displayAdditionalMetaTags() {
     $this->displayGenericAdditionalMetaTags($this->category);
   }


   public function displayBreadcrumbs() {
      $this->renderBreadcrumbs($this->breadcrumbs);
   }


   public function displayPage() {

      $this->renderLangGroupIdHiddenField($this->category);

      echo '<div class="container clearfix">';
         $this->renderBreadcrumbs($this->breadcrumbs);
         $this->renderImage();
         $this->renderPageTitle($this->category);
         $this->renderPageSubtitle($this->category);
         $this->renderPageContent($this->category);
         $this->renderSubCategories();
         $this->renderArticles();
      echo '</div>';
   }


   public function renderImage() {

      if(@exists($this->category->image)) {

         $image = $this->setMediaImageUrl(array("image" => $this->category->image));

         echo '<div class="article-image">';
            echo '<img src="' . $image . '" alt="' . $this->category->image . '" />';
         echo '</div>';
      }
   }


   public function renderSubCategories() {

      if(@exists($this->subCategories)) {

         echo '<div class="articles clearfix">';

            foreach ($this->subCategories as $category) {

               if(@exists($category)) {

                  $category = (object) $category;

                  $image = $this->setMediaImageUrl(array("image" => $category->intro_image));

                  echo '<div class="article clearfix">';
                     echo '<h4 class="title">' . $category->name .  '</h4>';
                     // echo '<div class="bg-image-wrapper" style="background-image: url(' .$image . ')"></div>';
                     echo '<div>';
                        echo '<img src="' . $image . '" alt="' . $category->intro_image . '" />';
                     echo '</div>';
                     echo '<div class="content-wrapper">';
                        echo '<div class="content">' . truncateString($category->intro_text) . '</div>';
                        echo '<a href="' . $category->url . '" class="read-more">' . ucfirst(Trans::get('Read more')) . '...</a>';
                     echo '</div>';
                  echo '</div>';
               }
            }

         echo '</div>';
      }
   }


   public function renderArticles() {

      if(@exists($this->articles)) {

         echo '<div class="articles">';

         foreach ($this->articles as $article) {

           $this->renderArticle($article);
         }

         echo '</div>';
      }
   }


   private function ifCategory($id) {
      return (int)$this->category->id === (int)$id || (int)$this->category->lang_group_id === (int)$id;
   }


   private function ifParent($id) {
      return (int)$this->category->parent_id === (int)$id || (int)$this->category->parent_lang_group_id === (int)$id;
   }
}
?>