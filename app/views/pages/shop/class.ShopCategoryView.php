<?php

class ShopCategoryView extends MainView implements PagesViewInterface {

    public $pageName;
    private $breadcrumbs;
    private $category;
    private $subCategories = array();
    private $products = array();
    private $productsCount = 0;
    private $params;

    public function __construct($data, $params) {
        parent::__construct();

        if(@exists($data)) {

            if(@exists($data->breadcrumbs)) {
                $this->breadcrumbs = $data->breadcrumbs;
            }

            if(@exists($data->category)) {
                $this->category = $data->category;
                $this->pageName = $this->category->name;
            }

            if(@exists($data->subCategoriesTree) && @exists($data->subCategoriesTree['children'])) {
                $this->subCategories = $data->subCategoriesTree['children'];
            }

            if(@exists($data->products)) {
                $this->products = $data->products;
            }

            if(@exists($data->productsCount)) {
                $this->productsCount = $data->productsCount;
            }
        }

        if(@exists($params)) {

            $this->params = $params;

            if(!@exists($params['items_per_page'])) {
                $this->params['items_per_page'] = Conf::get("items_per_page")["site_products"];
            }

            if(@exists($data->category)) {
                $this->params['url'] = $data->category->url;
            }

            if(@exists($data->productsCount)) {
                $this->params['total'] = $data->productsCount;
            }
        }
    }


    // meta title tag
    public function displayMetaTitle() {
        $this->renderMetaTitle($this->pageName, $this->category);
    }


    // meta description, keywords and og tags
    public function displayAdditionalMetaTags() {
        $this->displayGenericAdditionalMetaTags($this->category);
    }


    public function displayPage() {

        $this->dispalyHiddenFields();

        echo '<div class="container clearfix">';

            $this->renderBreadcrumbs($this->breadcrumbs);

            Dispatcher::instance()->dispatch("normacore", "layout", "sidebarNavigation", null, Request::HTML_REQUEST);

            echo '<div class="inner-wrapper">';

                $this->renderCategory();
                $this->renderCategories();
                $this->renderProducts($this->products, $this->params);

            echo '</div>';

        echo '</div>';
    }


    private function dispalyHiddenFields() {

        $this->renderLangGroupIdHiddenField($this->category);
        echo '<input type="hidden" id="categoryId" value="' . $this->category->id . '" />';
    }


    private function renderCategory() {

        $this->renderPageTitle($this->category);
        $this->renderPageSubtitle($this->category);
        echo '<div class="category-desc clearfix">';

            $contentClass = '';
            if($this->mediaImageExists($this->category->image)) {
                $this->renderImage();
                $contentClass = ' with-image';
            }

            if(@exists($this->category->content)) {

                echo '<div class="page-content' . $contentClass . '">' . $this->category->content . '</div>';
            }

        echo '</div>';
    }


    private function renderImage() {


        $image = $this->setMediaImageUrl(array("image" => $this->category->image, "thumb" => true));

        echo '<div class="category-image" style="background-image: url(' . $image . ')"></div>';
    }


    private function renderCategories() {

        if(@exists($this->subCategories)) {

            echo '<h2 class="categories-title">' . Trans::get('Refine Search') . '</h2>';

            echo '<div class="categories">';

            foreach ($this->subCategories as $category) {

                if(!is_object($category)) $category = (object) $category;

                $image = $this->setMediaImageUrl(array("image" => $category->image, "thumb" => true));

                echo '<div class="category">';

                    echo '<div class="bg-image-wrapper">';
                        echo '<a href="' . $category->url . '">';
                            echo '<div class="bg-image" style="background-image: url(' . $image . ')"></div>';
                        echo '</a>';
                    echo '</div>';
                    echo '<h3 class="title"><a href="' . $category->url . '">' . $category->name . '</a><h3>';

                echo '</div>';
            }

            echo '</div>';
        }
    }


    private function ifCategory($id) {
        return (int)$this->category->id === (int)$id || (int)$this->category->lang_group_id === (int)$id;
    }


    private function ifParent($id) {
        return (int)$this->category->parent_id === (int)$id || (int)$this->category->parent_lang_group_id === (int)$id;
    }
}
?>
