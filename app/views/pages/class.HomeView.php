<?php


class HomeView extends MainView implements PagesViewInterface {

  public $pageName;
  public $categories;
  public $products;
  public $news;

  public function __construct($data) {
    parent::__construct();

    $this->pageName = Conf::get('site_name');

    if(@exists($data)) {

      if(@exists($data->products)) {
        $this->products = $data->products;
      }

      if(@exists($data->categories)) {
        $this->categories = $data->categories;
      }

      if(@exists($data->news)) {
        $this->news = $data->news;
      }
    }
  }


  // meta title tag
  public function displayMetaTitle() {
    $title = $this->pageName;
    $this->renderMetaTitle($title);
  }


  // meta description, keywords and og tags
  public function displayAdditionalMetaTags() {
    $this->displayStaticAdditionalMetaTags(array('title' => $this->pageName));
  }


  public function displayPage() {
      Dispatcher::instance()->dispatch("normacore", "layout", "homeSlider", null, Request::HTML_REQUEST);
    echo '<div class="container clearfix">';
      Dispatcher::instance()->dispatch("normacore", "layout", "sidebarNavigation", null, Request::HTML_REQUEST);
      echo '<div class="inner-wrapper">';

        $this->renderCategories();
        $this->displayProducts();
      echo '</div>';
    echo '</div>';
    $this->renderAboutSection();
    $this->renderNewsSection();
  }


  public function displayProducts() {

    if (@exists($this->products)) {

      echo '<div class="products-list">';

      foreach ($this->products as $product) {

        $this->renderProductInList($product);
      }

      echo '</div>';
    }
  }


  public function renderCategories() {

    if (@exists($this->categories)) {

      echo '<div class="categories">';

      foreach ($this->categories as $category) {

        echo '<div class="category">';
          echo '<a href="' . $category->url . '">';
            echo '<img src="' . Conf::get('media_url') . '/'. $category->image . '" />';
          echo '</a>';
        echo '</div>';
      }

      echo '</div>';
    }
  }


  public function renderAboutSection() {

    echo '<div class="about">';

      echo '<div class="container">';

      echo '<h2>Donec rhoncus ut sapien ac pretium?</h2>';
      echo '<p><span>LOREM IPSUM</span> dolor sit amet, consectetur adipiscing elit. Integer cursus ut eros eget venenatis.<span>SKF  proizvodnih grupa</span> (zaptivke; masti i sistemi podmazivanja; linearna tehnika - vodjice, vretena, stolovi za  pozicioniranje; proizvodi za prenos snage - lanci, lancanici, remenje, remenice, spojnice i servis -alati za  montažu i demontažu ležajeva, alati za praćenje stanja obrtne opreme) koji su svi dostupni iz jednog izvora.</p>';
      echo '<p>Vestibulum sodales, risus id tincidunt porttitor, libero quam tincidunt ex.</p>';

      echo '<div class="bottom">';

        echo '<div class="left">';
          echo '<div class="image">';
            echo '<img src="' . Conf::get('css_img_url') . '/banner.png" alt="o nama" />';
          echo '</div>';
        echo '</div>';

          echo '<div class="right">';
            echo '<p><span>LOREM IPSUM</span> dolor sit amet, consectetur adipiscing elit <span>INTEGER</span> cursus ut eros eget venenatis.</p>';
            echo '<ul>';
              echo '<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>';
              echo '<li>Suspendisse volutpat posuere metus quis mollis.</li>';
              echo '<li>Proin vel elementum nisi.</li>';
              echo '<li>Vestibulum sodales, risus id tincidunt porttitor, libero quam tincidunt ex.</li>';
              echo '<li>Ut convallis nec tortor eget molestie.</li>';
              echo '<li>Donec malesuada sed mauris vel commodo.</li>';
              echo '<li>Pellentesque eleifend scelerisque enim a pulvinar.</li>';
              echo '<li>Phasellus euismod lacus quam, sit amet fermentum ligula vehicula ut.</li>';
              echo '<li>Donec in efficitur felis, id imperdiet velit.</li>';
            echo '</ul>';
          echo '</div>';
      echo '</div>';

      echo '</div>';

    echo '</div>';
  }


  public function renderNewsSection() {

    echo '<div class="news">';

      echo '<div class="container">';

        echo '<h2>' . Trans::get('Latest news') . '</h2>';

        echo '<div class="articles">';

          foreach ($this->news as $news) {

            $this->renderArticle($news);
          }
        echo '</div>';

      echo '</div>';

    echo '</div>';
  }
}

?>